//
//  ForgetPwController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "ForgetPwController.h"

@interface ForgetPwController ()

@property (nonatomic, weak) IBOutlet UITextField *userName;

@property (nonatomic, weak) IBOutlet UITextField *passw;

@property (nonatomic, weak) IBOutlet UITextField *aginPw;

@property (nonatomic, weak) IBOutlet UITextField *phone;

@property (nonatomic, weak) IBOutlet UITextField *verifyCode;

//@property (nonatomic, strong) NSString *reusltCode;

@property (nonatomic, strong) NSTimer *time;

@property (nonatomic, weak) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *confirmBtn;

@end

@implementation ForgetPwController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"忘记密码";
    
    [STUtils setupView:self.confirmBtn cornerRadius:18 bgColor:AppTintColor borderW:0 borderColor:nil];
    [self.userName becomeFirstResponder];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendSMS)];
    
    self.label.userInteractionEnabled = YES;
    [STUtils setupView:self.label cornerRadius:10 bgColor:AppTintColor borderW:0 borderColor:nil];
    
    [self.label addGestureRecognizer:tap];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
//    [self prepareLayout];
}

- (void)prepareLayout {
    
    self.view.backgroundColor = [UIColor colorWithRed:0.941 green:0.9365 blue:0.9456 alpha:1.0];
    
    self.userName = [self customWithFrame:CGRectMake(20, 50, ScreenWidth - 40, 40) andLeftTitle:@"  账号" andPlaceH:@"请输入登录账号"];
    [self.userName becomeFirstResponder];
    [self.view addSubview:self.userName];
    
    self.passw = [self customWithFrame:CGRectMake(20, self.userName.bottom + 10, ScreenWidth - 40, 40) andLeftTitle:@"  新密码" andPlaceH:@"请输入密码"];
    self.passw.secureTextEntry = YES;
    [self.view addSubview:self.passw];
    
    self.aginPw = [self customWithFrame:CGRectMake(20, self.passw.bottom + 10, ScreenWidth - 40, 40) andLeftTitle:@"  确认密码" andPlaceH:@"请再次输入密码"];
    self.aginPw.secureTextEntry = YES;
    [self.view addSubview:self.aginPw];
    
    self.phone = [self customWithFrame:CGRectMake(20, self.aginPw.bottom + 10, ScreenWidth - 40, 40) andLeftTitle:@"  手机号" andPlaceH:@"请输入您的手机号"];
    
    [self.view addSubview:self.phone];
    
    self.verifyCode = [self customWithFrame:CGRectMake(20, self.phone.bottom + 10, ScreenWidth - 40 - 100, 40) andLeftTitle:@"  验证码" andPlaceH:@"请输入验证码"];
    
    [self.view addSubview:self.verifyCode];
    
    UILabel *lable = [[UILabel alloc] initWithFrame:CGRectMake(self.verifyCode.right, self.verifyCode.top, 100, 40)];
    
    self.label = lable;
    
    lable.backgroundColor = AppTintColor;
    
    lable.font = FontSystem(14);
    
    lable.textAlignment = NSTextAlignmentCenter;
    
    lable.text = @"获取验证码";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sendSMS)];
    
    lable.userInteractionEnabled = YES;
    
    [lable addGestureRecognizer:tap];
    
    [self.view addSubview:lable];
    
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeSystem)];
    
    btn.frame = CGRectMake(20, self.verifyCode.bottom + 20, ScreenWidth - 40, 40);
    
    btn.backgroundColor = AppTintColor;
    
    btn.layer.cornerRadius = 3;
    
    [btn setTintColor:[UIColor whiteColor]];
    
    [btn setTitle:@"确  认  修  改" forState:(UIControlStateNormal)];
    
    [btn addTarget:self action:@selector(resetBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    
    [self.view addSubview:btn];
}


- (void)sendSMS {
    
    if (![self checkTel:self.phone.text]) {
        return;
    }
    
    self.label.userInteractionEnabled = NO;
    
    self.label.text = @"60";
    
    
    self.time = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timeStart) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:self.time forMode:NSDefaultRunLoopMode];
    [self.time fire];
    
    [DataRequest  PostWithURL:SendSMS parameter:@{@"vcLinkTel": self.phone.text, @"vcType":@"1", @"vcIMEI" : [[UIDevice currentDevice].identifierForVendor UUIDString]} :^(id reponserObject) {
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:1 ToView:self.view userInteractionEnabled:YES];
//            _reusltCode = reponserObject[@"smsCode"];
            
        }else {
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
        }

        DLog(@"%@",reponserObject);
    }];
  
}
- (void)timeStart {
    if ([self.label.text intValue] - 1 > 0) {
        self.label.text = [NSString stringWithFormat:@"%d",[self.label.text intValue] - 1];
    }else {
        self.label.text = @"重新获取";
        [self.time invalidate];
        self.label.userInteractionEnabled = YES;
    }
}
- (IBAction)resetBtnClick2:(UIButton *)sender {
    [self resetBtnClick];
}

- (void)resetBtnClick {
    if (![self.passw.text isEqualToString:self.aginPw.text]) {
        
        [MBProgressHUD showMessage:@"两次密码不一致" RemainTime:2 ToView:self.view userInteractionEnabled:YES];

        return;
        
    }else if(self.passw.text == nil || [self.passw.text isEqualToString:@""] || self.aginPw.text == nil || [self.aginPw.text isEqualToString:@""] ||  self.phone.text == nil || [self.phone.text isEqualToString:@""] || self.verifyCode.text == nil || [self.verifyCode.text isEqualToString:@""]) {
        
        [MBProgressHUD showMessage:@"请填写全部信息" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        
        return;
    }else{
        
//        if (![self.reusltCode isEqualToString:self.verifyCode.text]) {
//        [MBHelper addHUDInView:self.view text:@"验证码错误" hideAfterDelay:2];
//        return;
//    }
    
        [DataRequest PostWithURL:FogetPw parameter:@{@"vcTel": self.phone.text, @"newPassword" : self.aginPw.text , @"smsCode" : self.verifyCode.text} :^(id reponserObject) {
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });
            
        }else {
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:1 ToView:self.view userInteractionEnabled:YES];
        }
        
    }];
    }
}

- (BOOL)checkTel:(NSString *)str
{
    if ([str length] == 0)
    {
        
        [MBProgressHUD showMessage:@"请输入手机号码" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
        
        
        return NO;
    }
    NSString *regex1 = @"^((13[0-9])|(15[^4,\\D])|(14[57])|(17[0-9])|(18[0-9]))\\d{8}$";
    NSPredicate *pred1 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex1];
    BOOL isMatch = [pred1 evaluateWithObject:str];
    if (!isMatch) {
        [MBProgressHUD showMessage:@"请输入正确的手机号码" RemainTime:2 ToView:self.view userInteractionEnabled:YES];

        return NO;
        
    }
    return YES;
}


- (UITextField *)customWithFrame:(CGRect)frame andLeftTitle:(NSString *)leftT andPlaceH:(NSString *)placeho {
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 100, frame.size.height - 20)];
    label.text = leftT;
    label.textColor = [UIColor colorWithRed:0.4353 green:0.4353 blue:0.4353 alpha:1.0];
    label.font = [UIFont systemFontOfSize:15];
    UITextField *textF = [[UITextField alloc] initWithFrame:frame];
    textF.leftViewMode = UITextFieldViewModeAlways;
    textF.layer.borderWidth = 0;
    textF.leftView = label;
    textF.placeholder = placeho;
    textF.clearButtonMode = UITextFieldViewModeWhileEditing;
    textF.font = [UIFont systemFontOfSize:13];
//    [self addLineWithUI:textF andFrame:CGRectMake(0, frame.size.height - 1, frame.size.width, 1) andColor:[UIColor colorWithRed:0.7019 green:0.7019 blue:0.7019 alpha:1.0]];
    textF.backgroundColor = [UIColor whiteColor];
    textF.delegate = self;
    textF.borderStyle = UITextBorderStyleRoundedRect;
    
    return textF;
}


- (void)addLineWithUI:(UIView *)UIv andFrame:(CGRect)frame andColor:(UIColor *)color {
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = color;
    view.frame = frame;
    [UIv addSubview:view];
}



@end
