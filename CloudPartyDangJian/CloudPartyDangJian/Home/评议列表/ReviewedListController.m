//
//  ReviewedListController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ReviewedListController.h"
#import "VoteCell.h"
#import "ReviewedController.h"

@interface ReviewedListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@end

@implementation ReviewedListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"评议列表";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.isFirst) {
        
        [self requestData];
    }
}

- (void)requestData {
    
    [DataRequest GetWithURL:ReviewedList :^(id reponserObject) {
        DLog(@"reponserObject%@",reponserObject);
        self.reviewedListModel = [ReviewedListModel mj_objectWithKeyValues:reponserObject];
        
        [self.tableV reloadData];
        
    }];

}

- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableV registerClass:[VoteCell class] forCellReuseIdentifier:@"VoteCell"];
//    self.tableV.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 50)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.reviewedListModel.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VoteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VoteCell" forIndexPath:indexPath];
    
    [cell setTitle:self.reviewedListModel.objects[indexPath.row].vcTitle andBeginTime:self.reviewedListModel.objects[indexPath.row].dtBegin andEndTime:self.reviewedListModel.objects[indexPath.row].dtEnd andState:self.reviewedListModel.objects[indexPath.row].nStatus];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];

    
    ReviewedController *reviewed = [ReviewedController new];
    reviewed.reviewedModel = self.reviewedListModel.objects[indexPath.row];
//reviewed.cell.CellreviewedModel = self.reviewedListModel.objects[indexPath.row];
    self.isFirst = NO;

    [self.navigationController pushViewController:reviewed animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}







@end
