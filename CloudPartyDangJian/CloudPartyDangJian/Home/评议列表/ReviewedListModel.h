//
//  ReviewedListModel.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ReviewedModel,ReviewedChoices,Begreadperson,Optionscore;
@interface ReviewedListModel : NSObject

@property (nonatomic, strong) NSArray<ReviewedModel *> *objects;

@end
@interface ReviewedModel : NSObject

@property (nonatomic, assign) NSInteger nTotalScore;

@property (nonatomic, copy) NSString *vcExamGroupName;

@property (nonatomic, strong) NSArray<ReviewedChoices *> *choices;

@property (nonatomic, assign) NSInteger nStatus;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *vcRegister;

@property (nonatomic, copy) NSString *vcTitle;

@property (nonatomic, strong) NSArray<Begreadperson *> *beGreadPerson;

@property (nonatomic, copy) NSString *dtBegin;

@property (nonatomic, copy) NSString *dtEnd;

@property (nonatomic, copy) NSString *dtReg;

@property (nonatomic, copy) NSString *vcExamGroupId;

@end

@interface ReviewedChoices : NSObject

@property (nonatomic, copy) NSString *vcOption;

@property (nonatomic, assign) NSInteger nTopicsId;

@property (nonatomic, assign) NSInteger nScore;

@property (nonatomic, assign) NSInteger nId;

@end

@interface Begreadperson : NSObject

@property (nonatomic, copy) NSString *vcTel;

@property (nonatomic, assign) NSInteger nStatus;

@property (nonatomic, copy) NSString *beGreadPerson;

@property (nonatomic, strong) NSArray<Optionscore *> *optionScore;

@end

@interface Optionscore : NSObject

@property (nonatomic, assign) NSInteger nScore;

@property (nonatomic, copy) NSString *vcOption;

@property (nonatomic, assign) NSInteger nOptionId;

@end

