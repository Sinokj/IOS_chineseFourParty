
//
//  ExamCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ExamCell.h"
#import "DyfTool.h"
#import "AnswerCell.h"
#import <NSString+YYAdd.h>
@interface ExamCell ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UILabel *titleL;
@property (nonatomic, strong) UITableView *examTableV;
@property (nonatomic, strong) NSMutableDictionary *dic;
@property (nonatomic, strong) NSMutableString *selectStr;
@property (nonatomic, strong) UILabel *answerLabel;
@property (nonatomic, strong) UILabel *answerDetailL;
@property (nonatomic, strong) UIScrollView *bgScroll;

@end

@implementation ExamCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.dic = [NSMutableDictionary dictionary];
    self.contentView.backgroundColor = AppTintColor;
    
//    UIImageView *imageView = [[UIImageView alloc] init];
//    imageView.image = [UIImage imageNamed:@"bg_anwser1"];
//    [self.contentView addSubview:imageView];
//    imageView.frame = CGRectMake(0, -40, ScreenWidth, 150);

    
    UIScrollView *bgScrollerV = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.width, self.contentView.height - 2)];
    bgScrollerV.backgroundColor = [UIColor whiteColor];
//    bgScrollerV.layer.cornerRadius = 10;
//    bgScrollerV.layer.masksToBounds = true;
    [self.contentView addSubview:bgScrollerV];
    
    self.bgScroll = bgScrollerV;
    
    self.titleL = [UILabel customLablWithFrame:CGRectMake(27, 25, ScreenWidth - 54, 40) andTitle:@"" andFontNumber:16];
    self.titleL.numberOfLines = 0;
    [bgScrollerV addSubview:self.titleL];
    
    self.examTableV = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    self.examTableV.delegate = self;
    self.examTableV.dataSource = self;
    self.examTableV.scrollEnabled = YES;
    self.examTableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.examTableV.tableFooterView = [UIView new];
    [self.examTableV registerClass:[AnswerCell class] forCellReuseIdentifier:@"AnswerCell"];
    [bgScrollerV addSubview:self.examTableV];
    
    self.examTableV.sd_layout
    .leftSpaceToView(bgScrollerV, 0)
    .rightSpaceToView(bgScrollerV, 0)
    .bottomSpaceToView(bgScrollerV, 100);
    
    self.answerLabel = [UILabel customLablWithFrame:CGRectMake(20, self.examTableV.bottom + 10, ScreenWidth - 40 - 12 * 2, 30) andTitle:@"答案 :  " andFontNumber:15];
    self.answerLabel.textAlignment = NSTextAlignmentCenter;
    [bgScrollerV addSubview:self.answerLabel];
//    self.answerDetailL = [UILabel customLablWithFrame:CGRectMake(20, self.answerLabel.bottom, ScreenWidth - 40, 70) andTitle:@"解析 :  " andFontNumber:14];
    self.answerDetailL.numberOfLines = 0;
    [bgScrollerV addSubview:self.answerDetailL];
    
    
    bgScrollerV.contentSize = CGSizeMake(ScreenWidth - 24, ScreenHeight);
    
    
    self.indexBtn = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_SIZE.width/2 - CGRectGetWidth(self.examTableV.frame)/6/2, -CGRectGetWidth(self.examTableV.frame)/6/2 + 35 , CGRectGetWidth(self.examTableV.frame)/6, CGRectGetWidth(self.examTableV.frame)/6)];
    [self.indexBtn setBackgroundImage:ImageName(@"Oval") forState:UIControlStateNormal];
    [self.indexBtn setTitleColor:AppTintColor forState:UIControlStateNormal];
    self.indexBtn.titleLabel.font = [UIFont systemFontOfSize:15.0];
//    [self.contentView addSubview:self.indexBtn];
    


}

- (void)setExamModel:(Exam *)examModel {
    
    _examModel = nil;
    _examModel = examModel;
    
    [self.dic removeAllObjects];
    
    for (NSString *choi in  self.answer) {
        
        if (![choi isEqualToString:@""]) {
            
            int num = [choi characterAtIndex:0] - 65;
            [self.dic setObject: [NSString stringWithFormat:@"%d", 1] forKey:[NSString stringWithFormat:@"%d",num]];
        }
        
    }

    self.titleL.text = [NSString stringWithFormat:@"%ld (%@): %@",examModel.nCodeId, examModel.vcType,examModel.vcTitle];

    CGRect rect = [self.titleL.text boundingRectWithSize:CGSizeMake(ScreenWidth - 54, MAXFLOAT) options:NSStringDrawingUsesFontLeading | NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.titleL.font} context:nil];
    CGFloat height = rect.size.height;

    self.titleL.height = height;

    self.examTableV.top = self.titleL.bottom + 150;

    self.examTableV.height = ScreenHeight - 64 - height  - 70 - 120;
//    self.examTableV.sd_layout.heightIs(ScreenHeight - 64 - height  - 70 - 120);

    if ((self.examModel.choices.count * 44) > self.examTableV.height) {

       self.examTableV.height = self.examModel.choices.count * 44;
//        self.examTableV.sd_layout.heightIs(self.examModel.choices.count * 44);
    }

    if (self.isOver) {

    self.answerLabel.text = [NSString stringWithFormat:@"正确答案 :  %@",[examModel.vcAnswer substringToIndex:examModel.vcAnswer.length - 1]];
    self.answerLabel.textColor = RGB(115, 115, 115);
    self.answerLabel.font = FontSystem(15);
//    self.answerDetailL.text = [NSString stringWithFormat:@"解析 :  %@",examModel.vcDescribe.length == 0? @"暂无":examModel.vcDescribe];

    CGFloat answerH = [self stringHeightWithStr:examModel.vcDescribe andSize:14 maxWidth:ScreenWidth - 40];

    self.answerLabel.top = self.examTableV.bottom + 10;
    self.answerDetailL.top = self.answerLabel.bottom;
    self.answerDetailL.height = answerH;
    }

    self.bgScroll.contentSize = CGSizeMake(ScreenWidth - 24, self.titleL.height + self.examTableV.height + self.answerLabel.height + self.answerDetailL.height + 50);

    [self.examTableV reloadData];

}

- (float)stringHeightWithStr:(NSString *)text andSize:(CGFloat)fontSize maxWidth:(CGFloat)maxWidth {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:fontSize],NSFontAttributeName, nil];
    
    float height = [text boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    return ceilf(height);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.examModel.choices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Choices *choicesM = self.examModel.choices[indexPath.row];
    AnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AnswerCell" forIndexPath:indexPath];
    if (!self.type) {
        for (NSString *anse in self.answer) {
            if ([anse isEqualToString:[choicesM.vcAnswer substringToIndex:1]]) {
                
                choicesM.isSelect = YES;
            }
        }
    }
    cell.choicesM = choicesM;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return [AnswerCell CellheightWithStr:self.examModel.choices[indexPath.row].vcAnswer];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
    {
        __block ExamCell *strongSelf = self;
    [self.examTableV deselectRowAtIndexPath:indexPath animated:YES];
    
    AnswerCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if ([self.examModel.vcType isEqualToString:@"单选"]) {
        
        [cell selectBtnClickWithRow:indexPath.row andModel:self.examModel andType:NO andFirst:NO block:^() {
            
            [strongSelf.examTableV reloadData];

        }];
//        [self.examTableV reloadData];
        
        [DataRequest PostWithURL:UpdateAnswer parameter:@{@"nTopicsId":@(self.examModel.nTopicsId), @"nTitleId" : @(self.examModel.nId), @"nTitleScore" : @(self.examModel.nScore), @"vcAnswer":[[self.examModel.choices[indexPath.row].vcAnswer substringToIndex:1] stringByAppendingString:@","],} :^(id reponserObject) {
            
            strongSelf.ExamBlock(@{@"nTitleId" : @(self.examModel.nId), @"vcAnswer": [[self.examModel.choices[indexPath.row].vcAnswer substringToIndex:1] stringByAppendingString:@","]}, [NSString stringWithFormat:@"%ld",self.examModel.nId]);
            

            if (![[NSString stringWithFormat:@"%@", reponserObject[@"nRes"]] isEqualToString:@"1"]) {

                DLog(@"出错了%d",__LINE__);
            }
        }];
    }else {
        
        self.selectStr = [NSMutableString string];

        
        [cell selectBtnClickWithRow:indexPath.row andModel:self.examModel andType:YES andFirst:NO block:^() {
            
            [strongSelf.dic setObject: [NSString stringWithFormat:@"%d", strongSelf.examModel.choices[indexPath.row].isSelect] forKey:[NSString stringWithFormat:@"%ld",indexPath.row]];
            
            for (NSString *str in strongSelf.dic.allKeys) {
                
                if ([strongSelf.dic[str] isEqualToString:@"1"]) {
                    
                    int num = [str intValue];
                
                    NSString *sele = [NSString stringWithFormat:@"%c",num + 65];
                    
                    if (sele) {
                        
                        [self.selectStr appendFormat:@"%@,",sele];
                    }
                }
            }
            
  
        }];
        
        DLog(@"%@",self.selectStr);
        [DataRequest PostWithURL:UpdateAnswer parameter:@{@"nTopicsId":@(self.examModel.nTopicsId), @"nTitleId" : @(self.examModel.nId), @"nTitleScore" : @(self.examModel.nScore), @"vcAnswer":self.selectStr,} :^(id reponserObject) {
            
            
            strongSelf.ExamBlock(@{@"nTitleId" : @(self.examModel.nId), @"vcAnswer": self.selectStr}, [NSString stringWithFormat:@"%ld",self.examModel.nId]);
            
            
            if (![[NSString stringWithFormat:@"%@", reponserObject[@"nRes"]] isEqualToString:@"1"]) {
    
                DLog(@"出错了%d",__LINE__);
            }
        }];
        
    }
    self.type = YES;

    [self.examTableV reloadData];
}

- (void)setIsOver:(BOOL)isOver {
    
    _isOver = isOver;
    
    if (isOver) {
        self.examTableV.allowsSelection = NO;
        
        self.answerLabel.hidden = NO;
        self.answerDetailL.hidden = NO;
        
    }else {
        self.examTableV.allowsSelection = YES;

        self.answerLabel.hidden = YES;
        self.answerDetailL.hidden = YES;
    }
}
@end
