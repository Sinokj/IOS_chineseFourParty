//
//  AnswerCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "AnswerCell.h"
#import "DyfTool.h"
#import <Masonry.h>

@interface AnswerCell ()
@property (nonatomic, strong) UIButton *checkBtn; //勾选按钮
@end
@implementation AnswerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.checkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"icon_dorpbox_nor"] forState:UIControlStateNormal];
    [self.checkBtn setBackgroundImage:[UIImage imageNamed:@"icon_dorpbox_pre"] forState:UIControlStateSelected];
    [self.contentView addSubview:self.checkBtn];
    
    self.checkBtn.sd_layout
    .rightSpaceToView(self.contentView, 25)
    .widthIs(25)
    .heightIs(25)
    .centerYEqualToView(self.contentView);
    
    self.choseBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    [self.choseBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [self.choseBtn setTitleColor:AppTintColor forState:(UIControlStateSelected)];
    self.choseBtn.titleLabel.numberOfLines = 0;
    self.choseBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.choseBtn.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [self.contentView addSubview:self.choseBtn];
    
    self.choseBtn.sd_layout
    .leftSpaceToView(self.contentView, 25)
    .rightSpaceToView(self.checkBtn, 10)
    .heightIs(40)
    .topSpaceToView(self.contentView, 10);
    
    UIView *lineV = [UIView new];
    lineV.backgroundColor = UIColorHex(949494);
    [self.contentView addSubview:lineV];
    
    lineV.sd_layout
    .leftSpaceToView(self.contentView, 25)
    .rightSpaceToView(self.contentView, 25)
    .heightIs(1)
    .bottomSpaceToView(self.contentView, 0);
    
    [self.choseBtn resignFirstResponder];
    self.choseBtn.userInteractionEnabled = NO;
    
}

- (void)setChoicesM:(Choices *)choicesM {
    if (_choicesM != choicesM) {
        _choicesM = nil;
        _choicesM = choicesM;
        
//        self.contentL.text = choicesM.vcAnswer;
        [self.choseBtn setTitle:choicesM.vcAnswer forState:UIControlStateNormal];
        
//        CGFloat height = [self stringHeightWithStr:choicesM.vcAnswer andSize:15 maxWidth:ScreenWidth - 60];
        
//        self.contentL.height = height;
        
//        self.choseBtn.centerY = self.contentL.centerY;
        
    }
    
    self.choseBtn.selected = choicesM.isSelect;
    self.checkBtn.selected = self.choseBtn.selected;
    [self setNeedsDisplay];
}

- (float)stringHeightWithStr:(NSString *)text andSize:(CGFloat)fontSize maxWidth:(CGFloat)maxWidth {
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:fontSize],NSFontAttributeName, nil];
    
    float height = [text boundingRectWithSize:CGSizeMake(maxWidth, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    return ceilf(height);
}

- (void)selectBtnClickWithRow:(NSInteger)row andModel:(Exam *)exam andType:(BOOL)type  andFirst:(BOOL)first block:(void(^)(void))block {
    if (!type) {
        for (Choices *choice in exam.choices) {
            choice.isSelect = NO;
        }
    }
    if (first) {
        for (Choices *choice in exam.choices) {
            choice.isSelect = NO;
        }

    }
    
    exam.choices[row].isSelect = !exam.choices[row].isSelect;
    
    block();
    
}

+ (CGFloat)CellheightWithStr:(NSString *)content {
    
    NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[UIFont systemFontOfSize:15],NSFontAttributeName, nil];
    
    float height = [content boundingRectWithSize:CGSizeMake(ScreenWidth - 60, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:dic context:nil].size.height;
    if (ceilf(height) > 50) {
        return ceilf(height) + 10;
    }else {
        return 50;
    }
    

}

- (void)drawRect:(CGRect)rect{
    [super drawRect:rect];
    
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    //设置虚线颜色
    CGContextSetStrokeColorWithColor(currentContext, [UIColor colorWithWhite:1 alpha:1].CGColor);
    //设置虚线宽度
    CGContextSetLineWidth(currentContext, 1);
    //设置虚线绘制起点
    CGContextMoveToPoint(currentContext, 10, [AnswerCell CellheightWithStr:self.choicesM.vcAnswer] - 1);
    //设置虚线绘制终点
    CGContextAddLineToPoint(currentContext, self.frame.size.width, [AnswerCell CellheightWithStr:self.choicesM.vcAnswer] - 1);
    //设置虚线排列的宽度间隔:下面的arr中的数字表示先绘制3个点再绘制1个点
    CGFloat arr[] = {3,2};
    //下面最后一个参数“2”代表排列的个数。
    CGContextSetLineDash(currentContext, 0, arr, 2);
    CGContextDrawPath(currentContext, kCGPathStroke);
}

@end
