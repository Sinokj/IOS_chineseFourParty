//
//  ExamController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ExamController.h"
#import "ExamCell.h"
#import "AnswerSheetController.h"
#import <Masonry.h>
#import "STNavigationController.h"

@interface ExamController ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionV;
/**
 *  当前的位置
 */
@property (nonatomic, strong) NSIndexPath *indexPathNow;

@property (nonatomic, strong) UICollectionViewFlowLayout *layout;
@property (nonatomic, strong) UILabel *currentPageLabel; //当前页面
//@property (nonatomic, weak) UILabel *indexLab;
//@property (nonatomic, weak) UIButton *indexBtn;


@end

@implementation ExamController

- (void)setIndexPathNow:(NSIndexPath *)indexPathNow {
    _indexPathNow = indexPathNow;
    
//    [self.indexBtn setTitle:[NSString stringWithFormat:@"%zd / %zd", indexPathNow.item + 1, self.examsM.objects.count] forState:UIControlStateNormal];
//    self.indexLab.text = [NSString stringWithFormat:@"%zd / %zd", indexPathNow.item + 1, self.examsM.objects.count];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.9952 green:0.9904 blue:1.0 alpha:1.0];
    self.title = @"查看试题";
    
    //返回
    UIBarButtonItem *leftItem = [UIBarButtonItem barButtonItemWithImageName:@"back" block:^(id sender) {
        [DECommenAlert showAlertWithContent:@"您正在答题过程中\n请问是否确定退出答题过程?" leftBtn:@"确定" rightBtn:@"取消" tapEvent:^(NSInteger index) {
            [DECommenAlert dismiss];
            if (index == 0) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    if (self.isOver) {
        
        UIButton *btnItem = [[UIButton alloc] init];
        [btnItem setBackgroundImage:[UIImage imageNamed:@"小角标"] forState:(UIControlStateNormal)];
        [btnItem setTitle:@"答题卡" forState:(UIControlStateNormal)];
        btnItem.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnItem setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [btnItem addTarget:self action:@selector(commitClick) forControlEvents:UIControlEventTouchUpInside];
        
        [btnItem sizeToFit];
        btnItem.titleLabel.font = [UIFont systemFontOfSize:15.0];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnItem];
    }else {
        
        UIButton *btnItem = [[UIButton alloc] init];
        [btnItem setBackgroundImage:[UIImage imageNamed:@"小角标"] forState:(UIControlStateNormal)];
        [btnItem setTitle:@"答题卡" forState:(UIControlStateNormal)];
        btnItem.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnItem setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [btnItem addTarget:self action:@selector(commitClick) forControlEvents:UIControlEventTouchUpInside];
        
        [btnItem sizeToFit];
        btnItem.titleLabel.font = [UIFont systemFontOfSize:15.0];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnItem];
        
    }
    
    [self prepareLayout];
    
}
// 根据题号 获取到答案
- (NSArray *)getAnswerWithId:(NSInteger)str {
    if (self.answerArr != nil) {
        NSString *answerStr;
        for (NSDictionary *dic in self.answerArr) {
            if ([[NSString stringWithFormat:@"%@",dic[@"nTitleId"]] isEqualToString:[NSString stringWithFormat:@"%ld",str]]) {
                answerStr = dic[@"vcAnswer"];
            }
        }
        return [answerStr componentsSeparatedByString:@","];
    }
    return nil;
}

- (void)prepareLayout {
    self.view.backgroundColor = AppTintColor;
    
    self.layout = [[UICollectionViewFlowLayout alloc] init];
    self.layout .scrollDirection = UICollectionViewScrollDirectionHorizontal;
//    self.collectionLayout.minimumLineSpacing = 10;
//    self.collectionLayout.minimumInteritemSpacing = 10;
//    self.layout .sectionInset = UIEdgeInsetsMake(0, 0, 80, 0);
    CGFloat xHeight = ScreenHeight == 812 ? 34 : 0;
    self.layout.itemSize = CGSizeMake(ScreenWidth, ScreenHeight - 64 - 50 - xHeight);
    
    
    if (@available(iOS 11.0, *)) {
        self.collectionV.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    self.collectionV = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64 - 50 - xHeight) collectionViewLayout:self.layout];
    self.collectionV.backgroundColor = [UIColor whiteColor];
    self.collectionV.showsVerticalScrollIndicator = NO;
    self.collectionV.showsHorizontalScrollIndicator = NO;
    self.collectionV.delegate = self;
    self.collectionV.dataSource = self;
    self.collectionV.pagingEnabled = YES;
    self.collectionV.bounces = YES;
    
    [self.collectionV registerClass:[ExamCell class] forCellWithReuseIdentifier:@"ExamCell"];
    [self.view addSubview:self.collectionV];
    
    
    UIView *bottomBgView = [[UIView alloc] initWithFrame:CGRectMake(0, self.collectionV.bottom, ScreenWidth, 50)];
    bottomBgView.backgroundColor = AppTintColor;
    [self.view addSubview:bottomBgView];
    
    
//    UIButton *indexBtn = [[UIButton alloc] init];
//    [indexBtn setBackgroundImage:ImageName(@"Oval") forState:UIControlStateNormal];
//    [indexBtn setTitle:[NSString stringWithFormat:@"1 / %zd", self.examsM.objects.count] forState:UIControlStateNormal];
//    [indexBtn setTitleColor:AppTintColor forState:UIControlStateNormal];
//    self.indexBtn = indexBtn;
//    [indexBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//
//
//
//    }];
//    [[UIApplication sharedApplication].keyWindow addSubview:indexBtn];
    
    
//    UILabel *indexLab = [[UILabel alloc] init];
//    indexLab.text = [NSString stringWithFormat:@"1 / %zd", self.examsM.objects.count];
//    indexLab.textColor = [UIColor whiteColor];
//    indexLab.textAlignment = NSTextAlignmentCenter;
//    [bottomBgView addSubview:indexLab];
//    self.indexLab = indexLab;
//    indexLab.textAlignment = NSTextAlignmentCenter;
//    [indexLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerX.offset(0);
//        make.centerY.offset(0);
//    }];

    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 5, 20, 15)];
//    imageView.image = [UIImage imageNamed:@"icon_last"];
    [bottomBgView addSubview:imageView];
    imageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *up = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(upSelectClick)];
    [imageView addGestureRecognizer:up];
    
    
    UIImageView *rimageView = [[UIImageView alloc] initWithFrame:CGRectMake(ScreenWidth - 40, 5, 20, 15)];
//    rimageView.image = [UIImage imageNamed:@"icon_next"];
    [bottomBgView addSubview:rimageView];
    rimageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *down = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(downSelectClick)];
    [rimageView addGestureRecognizer:down];
    

    UIButton *left = [UIButton buttonWithType:(UIButtonTypeSystem)];
    left.frame = CGRectMake(10, imageView.bottom, 100, bottomBgView.height - 25);
    left.centerX = imageView.centerX;
    [left setTintColor:[UIColor whiteColor]];
    [left setTitle:@"上一题" forState:(UIControlStateNormal)];
    [bottomBgView addSubview:left];
    [left addTarget:self action:@selector(upSelectClick) forControlEvents:(UIControlEventTouchUpInside)];
    left.tag = 998;
    
    
    UIButton *right = [UIButton buttonWithType:(UIButtonTypeSystem)];
    right.frame = CGRectMake(ScreenWidth - 60, imageView.bottom, 100, bottomBgView.height - 25);
    right.centerX = rimageView.centerX;
    [right setTintColor:[UIColor whiteColor]];
    [right setTitle:@"下一题" forState:(UIControlStateNormal)];
    [bottomBgView addSubview:right];
    [right addTarget:self action:@selector(downSelectClick) forControlEvents:(UIControlEventTouchUpInside)];
    right.tag = 999;
    
    UILabel *currentPageLabel = [UILabel new];
    [bottomBgView addSubview:currentPageLabel];
    [currentPageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(bottomBgView.mas_centerX);
        make.bottom.offset(-10);
    }];
    currentPageLabel.textColor = [UIColor whiteColor];
    self.currentPageLabel = currentPageLabel;
    self.currentPageLabel.text = [NSString stringWithFormat:@"1/%zd", self.examsM.objects.count];
}

#pragma mark - delegate 

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.examsM.objects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ExamCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ExamCell" forIndexPath:indexPath];
    
    cell.isOver = self.isOver;
    
    cell.answer = [self getAnswerWithId:self.examsM.objects[indexPath.item].nId];
    
    cell.examModel = self.examsM.objects[indexPath.item];
    [cell.indexBtn setTitle:[NSString stringWithFormat:@"%zd/%zd", indexPath.item +1, self.examsM.objects.count] forState:UIControlStateNormal];

    @weakify(self);
    
    cell.ExamBlock = ^(NSDictionary *dic, NSString *key) {
      
        BOOL isReplace = NO;
        
        for (int i = 0; i < weak_self.answerArr.count; i++) {
            
            NSDictionary *answerDic = weak_self.answerArr[i];
            
            if ([answerDic[@"nTitleId"] integerValue] == [key integerValue]) {
                
                [weak_self.answerArr replaceObjectAtIndex:i withObject:dic];
                
                isReplace = YES;
            }

        }
        
        if (!isReplace) {
            
            [weak_self.answerArr addObject:dic];
        }
        
    };
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0;
    
}

#pragma mark - button-click

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    CGPoint pInView = [self.view convertPoint:self.collectionV.center toView:self.collectionV];

    self.indexPathNow = [self.collectionV indexPathForItemAtPoint:pInView];
    self.currentPageLabel.text = [NSString stringWithFormat:@"%zd/%zd", self.indexPathNow.item +1, self.examsM.objects.count];
}
 //上一题
- (void)upSelectClick {
    
//    if (sender.tag == 998) {
        //上一题
        
        if (self.indexPathNow.row > 0) {
            
            [self.collectionV scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.indexPathNow.item - 1 inSection:self.indexPathNow.section] atScrollPosition:(UICollectionViewScrollPositionNone) animated:YES];
            
            self.indexPathNow = [NSIndexPath indexPathForItem:self.indexPathNow.item - 1 inSection:self.indexPathNow.section];
            
            self.currentPageLabel.text = [NSString stringWithFormat:@"%zd/%zd", self.indexPathNow.item +1, self.examsM.objects.count];
            
            [self.collectionV reloadData];
            
        }else {
            
            [MBProgressHUD showMessage:@"已经是第一题了" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            self.currentPageLabel.text = [NSString stringWithFormat:@"%zd/%zd", self.indexPathNow.item +1, self.examsM.objects.count];
        }
    
//    }else if (sender.tag == 999) {
//        //下一题
//          }
}
//下一题
- (void)downSelectClick {
    
    if (self.indexPathNow.row < self.examsM.objects.count - 1) {
        
        [self.collectionV scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:self.indexPathNow.item + 1 inSection:self.indexPathNow.section] atScrollPosition:(UICollectionViewScrollPositionNone) animated:YES];
        
        self.indexPathNow = [NSIndexPath indexPathForItem:self.indexPathNow.item + 1 inSection:self.indexPathNow.section];
        self.currentPageLabel.text = [NSString stringWithFormat:@"%zd/%zd", self.indexPathNow.item +1, self.examsM.objects.count];
        [self.collectionV reloadData];
    }else {
            [MBProgressHUD showMessage:@"已经是最后一题了" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            self.currentPageLabel.text = [NSString stringWithFormat:@"%zd/%zd", self.indexPathNow.item +1, self.examsM.objects.count];
    }

}
- (void)commitClick {
//单选
    NSMutableDictionary *singleDic = [NSMutableDictionary dictionary];
//多选
    NSMutableDictionary *doubleDic = [NSMutableDictionary dictionary];
    

    for (Exam *exam in self.examsM.objects) {
        if ([exam.vcType isEqualToString:@"单选"]) {
            
            
            [singleDic setObject:exam forKey:@([self.examsM.objects indexOfObject:exam])];
            
        }else {//多选
            
            [doubleDic setObject:exam forKey:@([self.examsM.objects indexOfObject:exam])];
    
        }
    }
    
    @weakify(self);
    
    [DataRequest PostWithURL:ContinueExam parameter:@{@"nTopicsId": @(self.examList.nId)} :^(id reponserObject) {
        @strongify(self);
        AnswerSheetController *answerSheet = [AnswerSheetController new];
        
        answerSheet.examList = self.examList;
        
        answerSheet.answerArr = reponserObject[@"objects"];
        
        answerSheet.singleDic = singleDic;
        
        answerSheet.doubleDic = doubleDic;
        
        answerSheet.isOver = self.isOver;
        
        answerSheet.AnswerSheetBlock = ^(NSInteger row) {
            
            [self.collectionV scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:row inSection:0] atScrollPosition:(UICollectionViewScrollPositionNone) animated:YES];
    
            self.indexPathNow = [NSIndexPath indexPathForItem:row inSection:0];
            
        };
        
        
        [self.navigationController pushViewController:answerSheet animated:YES];
    }];
}

@end
