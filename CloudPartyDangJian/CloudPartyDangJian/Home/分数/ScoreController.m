//
//  ScoreController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ScoreController.h"
#import <Masonry.h>

#define TRUEW(v) (v*([UIScreen mainScreen].bounds.size.width)/320)

@interface ScoreController ()

@property (nonatomic, strong) UIButton *suBtn;

@end

@implementation ScoreController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"分数";
    
     self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.suBtn];
    
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor blackColor];

    CALayer *redLayer = [CALayer layer];
    redLayer.backgroundColor = UIColorFromHex(0xe65454).CGColor;
    redLayer.frame = CGRectMake(0, 0, ScreenWidth, 256);
    
    [self.view.layer addSublayer:redLayer];
    

    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(12, 20, ScreenWidth - 2 * 12, 347)];
    
    bgView.image = [UIImage imageNamed:@"bg_question-1"];    
    
    [self.view addSubview:bgView];
    
    
    UIImageView *iconImage = [[UIImageView alloc] init];
    iconImage.image = [UIImage imageNamed:[self.isPass isEqualToString:@"不及格！"] ? @"pic_lose" : @"pic_success"];
    
    [bgView addSubview:iconImage];
    iconImage.contentMode = UIViewContentModeScaleAspectFit;
    [iconImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(40);
        make.right.offset(-40);
        make.top.offset(30);
        make.height.mas_equalTo(170);
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
    
    [bgView addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(iconImage.mas_bottom).offset(13);
        make.left.offset(50);
        make.right.offset(-50);
        make.height.mas_equalTo(1 / ScreenScale);
    }];
    
    UILabel *resultLab = [[UILabel alloc] init];
    resultLab.text = @"考试结果";
    resultLab.textAlignment = NSTextAlignmentCenter;
    resultLab.font = [UIFont systemFontOfSize:13];
    resultLab.textColor = UIColorFromHex(0x676165);
    resultLab.backgroundColor = [UIColor whiteColor];
    [bgView addSubview:resultLab];
    [resultLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.centerY.equalTo(line.mas_centerY);
        make.width.mas_equalTo(93);
        
    }];
    
    
    
    
    UILabel *scoreL = [[UILabel alloc] init];
    
    scoreL.font = FontSystem(30);
    
    [bgView addSubview:scoreL];
    
    scoreL.textAlignment = NSTextAlignmentCenter;
    
    scoreL.textColor = UIColorFromHex(0x9a9698);
    
    scoreL.text = [NSString stringWithFormat:@"得分 :%ld",(long)self.nScore];
    
    [scoreL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(resultLab.mas_bottom).offset(20);
    }];
    
//    UILabel *stateL = [[UILabel alloc] initWithFrame:CGRectMake(10, scoreL.bottom + 10, ScreenWidth - 20, 40)];
//
//    stateL.font = FontSystem(16);
//
//    [bgView addSubview:stateL];
//
//    stateL.textAlignment = NSTextAlignmentCenter;
//
//    stateL.text = self.isPass;
    
    UIButton *btnItem = [[UIButton alloc] init];
    [btnItem setBackgroundImage:[UIImage imageNamed:@"btn_login_nor"] forState:(UIControlStateNormal)];
    [btnItem setTitle:@"回到主页" forState:(UIControlStateNormal)];
    btnItem.titleLabel.textAlignment = NSTextAlignmentCenter;
    [btnItem setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [btnItem addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:btnItem];
    
    [btnItem mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(bgView.mas_bottom).offset(44);
        make.width.mas_equalTo(ScreenWidth - 24);
        make.height.mas_equalTo(48);
    }];
    
    
}

-(UIButton*)suBtn
{
    if (_suBtn==nil) {
        
        _suBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        _suBtn.frame = CGRectMake(0, TRUEW(2), TRUEW(15), TRUEW(20));
        
        [_suBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        
        _suBtn.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        [_suBtn addTarget:self action:@selector(pop) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    return _suBtn;
}

- (void)pop {
    
    //    [self.navigationController popToRootViewControllerAnimated:YES];
    
    [self.navigationController popViewControllerAnimated:true];
}

- (void)backToHome {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
    
//    [self.navigationController popViewControllerAnimated:true];
}





@end
