//
//  ViwepagerModel.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Objects;
@interface ViwepagerModel : NSObject

@property (nonatomic, assign) BOOL result;

@property (nonatomic, strong) NSArray<Objects *> *objects;

@end
@interface Objects : NSObject

@property (nonatomic, copy) NSString *vcRegister;
@property (nonatomic, assign) NSInteger nOrder;
@property (nonatomic, copy) NSString *dtReg;
@property (nonatomic, assign) NSInteger nclick;
@property (nonatomic, assign) NSInteger nLogin;
@property (nonatomic, copy) NSString *vcType;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *vcMemo;
@property (nonatomic, copy) NSString *vcPlatform;
@property (nonatomic, assign) NSInteger nShow;
@property (nonatomic, copy) NSString *vcTitle;
@property (nonatomic, copy) NSString *vcDescribe;
@property (nonatomic, assign) BOOL bCustom;
@property (nonatomic, copy) NSString *vcUrl;
@property (nonatomic, copy) NSString *vcPath;
@property (nonatomic, assign) NSInteger nId;
@property (nonatomic, copy) NSString *vcGroupId;

@end

@interface ArticleListModel: NSObject
@property (nonatomic, copy) NSString *vcPath;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *vcTitle;
@property (nonatomic, copy) NSString *vcDescribe;
@end
