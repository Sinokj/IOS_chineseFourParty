//
//  ListCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ListCell.h"
#import "DyfTool.h"
#import <UIImageView+AFNetworking.h>
@interface ListCell ()

//@property (nonatomic, strong) UIImageView *icoImageV;

@property (weak, nonatomic) IBOutlet UIImageView *icoImageV;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//@property (nonatomic, strong) UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

//@property (nonatomic, strong) UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

//@property (nonatomic, strong) UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *seeLabel;

//@property (nonatomic, strong) UILabel *seeLabel;
@property (weak, nonatomic) IBOutlet UILabel *alertL;

//@property (nonatomic, strong) UILabel *alertL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthContant;

//@property (nonatomic, strong) UIImageView *seeImgv;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertLWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertLHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *lineView;


@end
@implementation ListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        [self.timeLabel sizeToFit];
//        [self.seeLabel sizeToFit];
//        self.selectionStyle = UITableViewCellSeparatorStyleNone;
//        [self prepareLayout];
    }
    return self;
}

- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSeparatorStyleNone;

    self.icoImageV.sd_layout
    .leftSpaceToView(self.contentView, 12)
    .widthIs(230 * kWScale)
    .heightIs(150 * kWScale)
    .centerYEqualToView(self.contentView);
    
    self.titleLabel.sd_layout
    .leftSpaceToView(self.icoImageV, 10)
    .rightSpaceToView(self.contentView, 12)
    .topEqualToView(self.icoImageV)
    .autoHeightRatio(0);
    [self.titleLabel setMaxNumberOfLinesToShow:2];
    
    self.timeLabel.sd_layout
    .leftEqualToView(self.titleLabel)
    .bottomEqualToView(self.icoImageV)
    .heightIs(20);
    [self.timeLabel setSingleLineAutoResizeWithMaxWidth:200];
    
    self.lineView.sd_layout
    .leftSpaceToView(self.timeLabel, 5)
    .centerYEqualToView(self.timeLabel)
    .widthIs(1)
    .heightIs(14);
    
    self.seeLabel.sd_layout
    .leftSpaceToView(self.lineView, 5)
    .centerYEqualToView(self.lineView)
    .heightIs(20);
    [self.seeLabel setSingleLineAutoResizeWithMaxWidth:100];
    
    self.alertL.font = FontSystem(13);
    self.alertL.textAlignment = NSTextAlignmentCenter;
    self.alertL.sd_layout
    .rightSpaceToView(self.contentView, 12)
    .bottomEqualToView(self.icoImageV)
    .widthIs(100 * kWScale)
    .heightIs(40 * kWScale);
    self.alertL.sd_cornerRadiusFromHeightRatio = @(0.5);
    
//    self.alertL.layer.cornerRadius = MYDIMESCALEW(8);
//    self.alertL.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
//    self.alertL.clipsToBounds = YES;
//    self.imageWidthContant.constant = MYDIMESCALEW(107);
//    self.alertLWidthConstraint.constant = MYDIMESCALEW(45);
//    self.alertLHeightConstraint.constant = MYDIMESCALEW(15);
//    [STUtils setupView:self.icoImageV cornerRadius:5 bgColor:nil borderW:0 borderColor:nil];
}


- (void)setModel:(NewList *)model {
    if (_model != model) {
        _model = nil;
        _model = model;
        [self.icoImageV setImageWithURL:[NSURL URLWithString:model.vcPath] placeholderImage:[UIImage imageNamed:@"placeholder"]];
        
        self.titleLabel.text = model.vcTitle;
        self.detailLabel.text = model.vcDescribe;
        self.timeLabel.text = [NSString stringWithFormat:@"%@",[model.dtReg substringToIndex:model.dtReg.length - 3]];
        self.seeLabel.text = [NSString stringWithFormat:@"阅读 %ld",(long)model.nclick];
        
        if (model.nStatus > 0) {
            self.alertL.text = @"已读";
            
            self.alertL.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            self.alertL.layer.borderWidth = 1;
            self.alertL.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1].CGColor;
            self.alertL.backgroundColor = [UIColor whiteColor];
        }else if (model.nStatus == 0) {
            self.alertL.text = @"未读";
            
            self.alertL.textColor = [UIColor whiteColor];
            self.alertL.backgroundColor = AppTintColor;
            self.alertL.layer.borderWidth = 1;
            self.alertL.layer.borderColor = AppTintColor.CGColor;

        }
    }
    
    if (self.isLearn) {
        self.lineView.hidden = YES;
    }
    
    // 没有登录也隐藏
    if ([STUtils isLogin]) {
        self.alertL.hidden = NO;
        self.timeLeftMarginConstraint.priority = 999;
        self.timeLeftToSuperViewConstraint.priority = 750;
    }else {
        self.alertL.hidden = YES;
        self.timeLeftMarginConstraint.priority = 750;
        self.timeLeftToSuperViewConstraint.priority = 999;
    }
}

- (void)setIsShow:(BOOL)isShow {
    _isShow = isShow;
    
}


@end
