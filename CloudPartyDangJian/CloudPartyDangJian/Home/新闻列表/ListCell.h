//
//  ListCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListModel.h"

@interface ListCell : UITableViewCell

@property (nonatomic, strong) NewList *model;

@property (nonatomic, assign) BOOL isShow;

@property (nonatomic, assign) BOOL isLearn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLeftMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *timeLeftToSuperViewConstraint;
// 学习列表隐藏这个
@property (weak, nonatomic,readonly) IBOutlet UILabel *alertL;
@property (weak, nonatomic,readonly) IBOutlet UILabel *seeLabel;
@end
