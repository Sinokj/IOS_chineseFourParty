//
//  ALNewListSubVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2019/1/15.
//  Copyright © 2019 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ALnewsTypeModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ALNewListSubVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
/** 根视图 */
@property (nonatomic, strong)UITableView *tableView;
//页数
@property (nonatomic, assign)NSInteger page;
//models
@property (nonatomic, strong)NSMutableArray *modelArray;
//是否展示tab
@property (nonatomic, assign)BOOL isShowTab;

@property (nonatomic, strong) NSString *type;     //模块名称
@property (nonatomic, assign) NSInteger nModuleId;//模块Id
@property (nonatomic, strong) ALnewsTypeModel *typeModel;//分类tab
@end

NS_ASSUME_NONNULL_END
