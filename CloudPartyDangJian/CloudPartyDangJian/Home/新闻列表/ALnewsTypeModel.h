//
//  ALnewsTypeModel.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2019/1/15.
//  Copyright © 2019 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ALnewsTypeModel : NSObject
@property (nonatomic , copy) NSString              * nOrder;
@property (nonatomic , copy) NSString              * vcTagName;
@property (nonatomic , copy) NSString              * nId;
@property (nonatomic , copy) NSString              * nModuleId;
@end

@interface ALNewsModel : NSObject
@property (nonatomic, assign) NSInteger rn;

@property (nonatomic, copy) NSString *vcDescribe;

@property (nonatomic, copy) NSString *vcUrl;

@property (nonatomic, copy) NSString *vcMemo;

@property (nonatomic, copy) NSString *vcType;

@property (nonatomic, copy) NSString *vcRegister;

@property (nonatomic, assign) NSInteger nOrder;

@property (nonatomic, assign) NSInteger nclick;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, assign) BOOL bCustom;

@property (nonatomic, copy) NSString *vcPath;

@property (nonatomic, copy) NSString *vcTitle;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *dtReg;

@property (nonatomic, copy) NSString *vcContent;

@property (nonatomic, copy) NSString *vcPlatform;
/**
 *  1 已读
 */
@property (nonatomic, assign) NSInteger nStatus;
@property (nonatomic, assign) NSInteger nShared;
@end

NS_ASSUME_NONNULL_END
