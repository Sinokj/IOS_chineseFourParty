//
//  ALNewListSubVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2019/1/15.
//  Copyright © 2019 TWO. All rights reserved.
//

#import "ALNewListSubVC.h"
#import "WebViewController.h"
#import "ListModel.h"
#import "ListCell.h"

@interface ALNewListSubVC ()

@end

@implementation ALNewListSubVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //绘制页面
    [self drawSubView];
    
    [self.tableView.mj_header beginRefreshing];
}

- (void)drawSubView{
    [self.view addSubview:self.tableView];
    self.tableView.sd_layout.spaceToSuperView(UIEdgeInsetsZero);
}

#pragma mark - net
- (void)requestForSaleGongdanList{
    
    NSString *url = [NSString stringWithFormat:@"%@?vcType=%@&nPageNo=%ld&nPageSize=10&nTagId=%@&nCommitteeId=%@",ArticleURL,self.type, self.page, self.typeModel.nId, [STUtils objectForKey:KDangJianType]?[STUtils objectForKey:KDangJianType]:@2];
    
    [DataRequest GetWithURL:url :^(id reponserObject) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
        
        NSArray *models = [NewList mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]];
        
        if (self.page == 1) {
            [self.modelArray removeAllObjects];
        }
        if(models.count == 0 && self.page > 1) {
            [SMGlobalMethod showMiddleMessage:@"没有更多了"];
            return ;
        }
        [self.modelArray addObjectsFromArray:models];
        [self.tableView reloadData];
    }];

}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.model = self.modelArray[indexPath.row];
    if ([STUtils objectForKey:KDangJianType] && [[STUtils objectForKey:KDangJianType] integerValue] == 2) {
        cell.alertL.hidden = YES;
        cell.timeLeftMarginConstraint.priority = 750;
        cell.timeLeftToSuperViewConstraint.priority = 999;
    }else {
        cell.alertL.hidden = NO;
        cell.timeLeftMarginConstraint.priority = 999;
        cell.timeLeftToSuperViewConstraint.priority = 750;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 110;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NewList *neList = self.modelArray[indexPath.row];
    
    WebViewController *webVC = [WebViewController new];
    webVC.type = @"1";
    webVC.str = neList.url;
    webVC.canShare = neList.nShared;
    webVC.tit = self.type;
    webVC.shareImage = @[neList.vcPath];
    webVC.shareTitle = neList.vcTitle;
    webVC.shareDescribe = neList.vcDescribe;
    webVC.articleId = neList.nId;
    webVC.vcType = neList.vcType;
    [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark - lazy
- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerNib:[UINib nibWithNibName:@"ListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        
        MJWeakSelf;
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
            self.page = 1;
            [weakSelf requestForSaleGongdanList];
        }];
        
        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            self.page ++;
            [weakSelf requestForSaleGongdanList];
        }];
    }
    
    return _tableView;
}

- (NSMutableArray *)modelArray{
    if (_modelArray == nil) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}



@end
