//
//  ALNewsListVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2019/1/15.
//  Copyright © 2019 TWO. All rights reserved.
//

#import "YPTabBarController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ALNewsListVC : YPTabBarController
@property (nonatomic, strong) NSString *type;     //模块名称
@property (nonatomic, assign) NSInteger nModuleId;//模块Id
@end

NS_ASSUME_NONNULL_END
