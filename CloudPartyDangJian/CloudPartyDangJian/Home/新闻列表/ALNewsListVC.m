//
//  ALNewsListVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2019/1/15.
//  Copyright © 2019 TWO. All rights reserved.
//

#import "ALNewsListVC.h"
#import "ALnewsTypeModel.h"
#import "ALNewListSubVC.h"
#import "LLSearchViewController.h"

@interface ALNewsListVC ()
@property (nonatomic, strong)NSArray *typeArray;
@end

@implementation ALNewsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = [self.type isEqualToString:@"主题教育"] ? @"专题教育":self.type;
    @weakify(self);
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"home_search2" block:^(id sender) {
        [weak_self pushToSearchVC];
    }];
    self.view.backgroundColor = [UIColor whiteColor];
    
    //加载tab数据
    [self requestForLoadTabData];
}

#pragma mark - action
- (void)pushToSearchVC {
    LLSearchViewController *vc = [[LLSearchViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - net
//tab数据
- (void)requestForLoadTabData{
    NSString *url = [NSString stringWithFormat:@"%@nModuleId=%ld", TopMarkURL, self.nModuleId];
    [DataRequest PostWithURL:url parameter:nil :^(id reponserObject) {
        //解析数据
        self.typeArray = [ALnewsTypeModel mj_objectArrayWithKeyValuesArray:reponserObject[@"result"]];
        
        //配置tab
        [self setupTab];
    }];
}

#pragma mark - private
//配置tab
- (void)setupTab{
    if (self.typeArray.count == 1) {//不显示导航条
        [self setTabBarFrame:CGRectMake(0, -44, ScreenWidth, 44)
            contentViewFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kTopHeight - kBottomSafeAreaHeight)];
    }else{//显示导航条
        [self setTabBarFrame:CGRectMake(0, 0, ScreenWidth, 44)
            contentViewFrame:CGRectMake(0, 44, ScreenWidth, ScreenHeight - kTopHeight  - 44 - kBottomSafeAreaHeight)];
    }
    
    self.tabBar.itemTitleColor = [UIColor lightGrayColor];
    self.tabBar.itemTitleSelectedColor = AppTintColor;
    self.tabBar.itemTitleFont = [UIFont systemFontOfSize:15];
    self.tabBar.itemTitleSelectedFont = [UIFont boldSystemFontOfSize:15];
    
    self.tabBar.indicatorScrollFollowContent = YES;
    self.tabBar.indicatorColor = AppTintColor;
    
    self.tabBar.leadingSpace = 15;
    self.tabBar.trailingSpace = 15;
    [self.tabBar setIndicatorWidth:40 marginTop:40 marginBottom:0 tapSwitchAnimated:NO];
    [self.tabBar setScrollEnabledAndItemFitTextWidthWithSpacing:30 * kWScale];
    
    
    [self.tabContentView setContentScrollEnabled:YES tapSwitchAnimated:NO];
    self.tabContentView.loadViewOfChildContollerWhileAppear = YES;
    
    [self initViewControllers];
}

- (void)initViewControllers{
    NSMutableArray *controllers = [NSMutableArray array];
    for (int i = 0; i < self.typeArray.count; i ++) {
        ALnewsTypeModel *model = self.typeArray[i];
        ALNewListSubVC *subVC = [ALNewListSubVC new];
        subVC.yp_tabItemTitle = model.vcTagName;
        subVC.nModuleId = self.nModuleId;
        subVC.type = self.type;
        subVC.typeModel = model;
        [controllers addObject:subVC];
    }
    self.viewControllers = controllers;
}



@end
