//
//  ExamListController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ExamListController.h"
#import "ExamListModel.h"
#import "PrepareExamController.h"
#import "ALExamlistCell.h"

@interface ExamListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) ExamListModel *examListM;

@end

@implementation ExamListController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self requestData];
    
    [self prepareLayout];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!self.isFirst) {
        [DataRequest GetWithURL:[NSString stringWithFormat:@"%@?vcClassify=%@",ExamList,@"在线考试"] :^(id reponserObject) {
            self.reponsObjects = reponserObject;

            [self requestData];
        }];
    }
}

- (void)requestData {
    self.examListM = [ExamListModel mj_objectWithKeyValues:self.reponsObjects];
    
    [self.tableV reloadData];
    
}

- (void)prepareLayout {
    
    CGFloat xSafeArea = kScreenHeight == 812 ? 34 : 0;
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64 - xSafeArea) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.backgroundColor = UIColorFromHex(0xE3E3E3);
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    [self.tableV registerClass:[ExamListCell class] forCellReuseIdentifier:@"ExamListCell"];
    [self.tableV registerNib:[UINib nibWithNibName:@"ALExamlistCell" bundle:nil] forCellReuseIdentifier:@"ALExamlistCell"];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.examListM.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ALExamlistCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ALExamlistCell" forIndexPath:indexPath];
    cell.examLists = self.examListM.objects[indexPath.row];
    cell.icoImageV.image = [UIImage imageNamed:@"zhangshangdati"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PrepareExamController *prepareExam = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PrepareExamController"];
    
    prepareExam.examList = self.examListM.objects[indexPath.row];

    [self.navigationController pushViewController:prepareExam animated:YES];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return MYDIMESCALEH(100);
}
@end
