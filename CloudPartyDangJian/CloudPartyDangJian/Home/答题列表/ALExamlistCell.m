//
//  ListCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ALExamlistCell.h"
#import "DyfTool.h"
#import <UIImageView+AFNetworking.h>
@interface ALExamlistCell ()

//@property (nonatomic, strong) UIImageView *icoImageV;


@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//@property (nonatomic, strong) UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;

//@property (nonatomic, strong) UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

//@property (nonatomic, strong) UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *seeLabel;

//@property (nonatomic, strong) UILabel *seeLabel;
@property (weak, nonatomic) IBOutlet UILabel *alertL;

//@property (nonatomic, strong) UILabel *alertL;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imageWidthContant;

//@property (nonatomic, strong) UIImageView *seeImgv;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertLWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertLHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *lineView;


@end
@implementation ALExamlistCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        [self.timeLabel sizeToFit];
//        [self.seeLabel sizeToFit];
//        self.selectionStyle = UITableViewCellSeparatorStyleNone;
//        [self prepareLayout];
    }
    return self;
}



- (void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.selectionStyle = UITableViewCellSeparatorStyleNone;

    self.icoImageV.sd_layout
    .leftSpaceToView(self.contentView, 20)
    .widthIs(120 * kWScale)
    .heightIs(120 * kWScale)
    .centerYEqualToView(self.contentView);
    self.icoImageV.sd_cornerRadiusFromHeightRatio = @(0.5);
    
    self.titleLabel.sd_layout
    .leftSpaceToView(self.icoImageV, 10)
    .rightSpaceToView(self.contentView, 12)
    .topSpaceToView(self.contentView, 15)
    .autoHeightRatio(0);
    [self.titleLabel setMaxNumberOfLinesToShow:2];
    
    self.timeLabel.sd_layout
    .leftEqualToView(self.titleLabel)
    .bottomSpaceToView(self.contentView, 15)
    .heightIs(20);
    [self.timeLabel setSingleLineAutoResizeWithMaxWidth:200];
    
    self.lineView.sd_layout
    .leftSpaceToView(self.timeLabel, 5)
    .centerYEqualToView(self.timeLabel)
    .widthIs(1)
    .heightIs(14);
    
    self.seeLabel.sd_layout
    .leftSpaceToView(self.lineView, 5)
    .centerYEqualToView(self.lineView)
    .heightIs(20);
    [self.seeLabel setSingleLineAutoResizeWithMaxWidth:100];
    
    self.alertL.font = FontSystem(13);
    self.alertL.textAlignment = NSTextAlignmentCenter;
    self.alertL.sd_layout
    .rightSpaceToView(self.contentView, 12)
    .bottomEqualToView(self.icoImageV)
    .widthIs(100 * kWScale)
    .heightIs(44 * kWScale);
    self.alertL.sd_cornerRadiusFromHeightRatio = @(0.5);
    
//    self.alertL.layer.cornerRadius = MYDIMESCALEW(8);
//    self.alertL.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
//    self.alertL.clipsToBounds = YES;
//    self.imageWidthContant.constant = MYDIMESCALEW(107);
//    self.alertLWidthConstraint.constant = MYDIMESCALEW(45);
//    self.alertLHeightConstraint.constant = MYDIMESCALEW(15);
//    [STUtils setupView:self.icoImageV cornerRadius:5 bgColor:nil borderW:0 borderColor:nil];
}


- (void)setExamLists:(ExamLists *)examLists {
    if (_examLists != examLists) {
        _examLists = nil;
        _examLists = examLists;
        
        self.titleLabel.text = examLists.vcTitle;
        self.timeLabel.text = [NSString stringWithFormat:@"%@",[examLists.dtReg substringToIndex:examLists.dtReg.length - 3]];
        self.seeLabel.text = [NSString stringWithFormat:@"%ld人作答",(long)examLists.examedNumber];
        
        if (examLists.isBegin == 2) {//已完成
            self.alertL.text = @"已完成";
            self.alertL.layer.borderColor = UIColorFromRGB(0x6A6A6A).CGColor;
            self.alertL.layer.borderWidth = 1;
            self.alertL.textColor = UIColorFromRGB(0x6A6A6A);
        }else{//未参加
            self.alertL.text = @"未参加";
            self.alertL.layer.borderColor = AppTintColor.CGColor;
            self.alertL.layer.borderWidth = 1;
            self.alertL.textColor = AppTintColor;
        }
    }
}


@end
