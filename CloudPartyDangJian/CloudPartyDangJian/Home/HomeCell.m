//
//  HomeCell.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "HomeCell.h"
#import "DyfTool.h"
#import <Masonry.h>

@interface HomeCell()

@property (nonatomic, weak) UILabel *titleLab;
@property (nonatomic, strong) UIView *cornerV;
@property (nonatomic, strong) UILabel *cornerL;

@end

@implementation HomeCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.backgroundColor = [UIColor whiteColor];
    // image
    self.img = [[UIImageView alloc] init];
    self.img.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:self.img];
    @weakify(self);
    [self.img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weak_self.contentView.mas_centerY).offset(-8);
        make.width.height.mas_equalTo(MYDIMESCALEW(40));
        make.centerX.equalTo(weak_self.contentView.mas_centerX);
    }];
    
    // title
    UILabel *titleLab = [UILabel customLablWithFrame:CGRectZero andTitle:@"民情民意" andFontNumber:14];
    [self addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.offset(0);
        make.top.equalTo(weak_self.img.mas_bottom).offset(5);
    }];
    
    titleLab.textColor = [UIColor darkGrayColor];
    _titleLab = titleLab;
    
    
    self.cornerV = [[UIView alloc] init];
    
    self.cornerV.layer.cornerRadius = 10;
    self.cornerV.backgroundColor = [UIColor redColor];
    
    [self.contentView addSubview:self.cornerV];
    
    [self.cornerV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(5);
        make.right.offset(-5);
        make.size.mas_equalTo(CGSizeMake(20, 20));
    }];
    
    
    self.cornerL = [UILabel customLablWithFrame:CGRectMake(0, 0, 20, 20) andTitle:@"" andFontNumber:9];
    [self.cornerV addSubview:self.cornerL];
    self.cornerL.textAlignment = NSTextAlignmentCenter;
    self.cornerL.textColor = [UIColor whiteColor];
    
    
    [self.cornerL mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(weak_self.cornerV);
    }];
    self.cornerV.hidden = YES;
}

- (void)setModel:(CollectViewModel *)collectModel andTitle:(NSString *)title {
    self.cornerL.text = @"";
    self.cornerV.hidden = YES;
    self.titleLab.text = title;
    if (collectModel == nil) {
        self.cornerV.hidden = YES;
        self.cornerL.text = @"";
    }else {
        for (ObjectsX *objectX in collectModel.objectX) {
            if ([objectX.vcModule isEqualToString:self.titleLab.text]) {
                if (objectX.unRead > 0) {
                    self.cornerV.hidden = NO;
                    if (objectX.unRead > 99) {
                        self.cornerL.text = @"99+";
                    }else {
                        self.cornerL.text = [NSString stringWithFormat:@"%ld",objectX.unRead];
                    }
                }else {
                    self.cornerV.hidden = YES;
                    self.cornerL.text = @"";
                    
                }
            }
        }
    }
    
}


@end

