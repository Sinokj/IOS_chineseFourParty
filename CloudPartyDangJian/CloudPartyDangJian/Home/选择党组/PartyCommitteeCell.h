//
//  PartyCommitteeCell.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

//Party committee 翻译：党委
#import <UIKit/UIKit.h>
#import "PartyCommitteeModel.h"

@interface PartyCommitteeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconLeftConstraint;


- (void)refreshCellWithModel:(PartyCommitteeModel *)model;

@end
