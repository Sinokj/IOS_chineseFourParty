//
//  ChooseDangZuController.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface ChooseDangZuController : ViewController

@property (nonatomic, copy) void(^PartyBlock)(id model);

@end
