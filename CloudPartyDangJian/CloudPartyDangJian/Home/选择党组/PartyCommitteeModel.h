//
//  PartyCommitteeModel.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PartyCommitteeModel : NSObject

@property (nonatomic, assign) NSInteger nId;
@property (nonatomic, assign) NSInteger nPid;
@property (nonatomic, assign) NSInteger nOrders;
@property (nonatomic, strong) NSString *vcName;
@property (nonatomic, strong) NSString *vcPartyAppName;
@property (nonatomic, assign) NSInteger nModuleType;
@property (nonatomic, assign) NSInteger nLevel;
@property (nonatomic, assign) NSInteger bLeaf;

+ (PartyCommitteeModel *)createModelWithDic:(NSDictionary *)dic;

@end
