//
//  ChooseDangZuController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "ChooseDangZuController.h"
#import "PartyCommitteeCell.h"
#import "PartyCommitteeModel.h"

@interface ChooseDangZuController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataArray;


@end

@implementation ChooseDangZuController

- (NSMutableArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (void)dealloc {
    DLog(@"--dealloc Choose");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"中国四维e党建";

    [self setupTableView];
    [self getTheData];
    
}

- (void)setupLeftItem {
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)back {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getTheData {
    [MBProgressHUD showMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:YES];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [DataRequest PostWithURL:DangWeiURL parameter:nil :^(id reponserObject) {
           
            NSArray *dangweiArr = reponserObject[@"objects"];
            for (NSDictionary *dict in dangweiArr) {
                [self.dataArray addObject:[PartyCommitteeModel createModelWithDic:dict]];
                
                [self.tableView reloadData];
            }
            
            [MBProgressHUD hideHUD];
        }];
    });
}

- (void)setupTableView {
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *cellID = @"DANGZUCELLID";
    PartyCommitteeCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"PartyCommitteeCell" owner:nil options:nil][0];
    }
    if (indexPath.row == 0) {
        PartyCommitteeModel *model = self.dataArray[indexPath.row];
        cell.titleLabel.text = model.vcName;
        cell.imgView.image = [UIImage imageNamed:@"dang3"];
        cell.iconLeftConstraint.constant = 15;
    }else {
        PartyCommitteeModel *model = self.dataArray[indexPath.row];
        [cell refreshCellWithModel:model];
        cell.imgView.image = [UIImage imageNamed:@"dang4"];
        cell.iconLeftConstraint.constant = 60;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
//    if (indexPath.row == 0) return;
    
    PartyCommitteeModel *model = self.dataArray[indexPath.row];
    
    if (self.PartyBlock) {
        self.PartyBlock(model);
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.navigationController popViewControllerAnimated:YES];
    });
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 65;
}
//
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    else if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//
//}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
