//
//  PartyArticleDetailVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "PartyArticleDetailVC.h"
#import "FuJianCell.h"
#import "MJDownload.h"
#import "ALReasonVC.h"
#import "DESaoMIaoController.h"
#import "UIImage+ImageSize.h"

#define kFuJianItemH 40

@interface PartyArticleDetailVC ()<UITableViewDelegate, UITableViewDataSource, UINavigationBarDelegate, UIWebViewDelegate, UIAlertViewDelegate>
/** web */
@property (nonatomic, strong)UIWebView *webV;
/** 附件 */
@property (nonatomic, strong)UITableView *fujianView;
@property (nonatomic, strong)NSArray *fujianList;

@property (nonatomic, strong) UIButton *bgBtnView;
@property (nonatomic, strong) UIImageView *imgView;
@property(nonatomic,strong)UIButton *closeBtn;

@property (nonatomic, strong)UIButton *btmFirBtn;
@property (nonatomic, strong)UIButton *btmSecBtn;
@end

@implementation PartyArticleDetailVC

- (instancetype)init{
    if (self = [super init]) {
        self.isDisPlayBtm = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    //导航条
    self.title = self.type;
    UIBarButtonItem *leftItem = [UIBarButtonItem barButtonItemWithImageName:@"back" block:^(id sender) {
        [self.navigationController popViewControllerAnimated:YES];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    self.navigationItem.leftBarButtonItem = leftItem;

    //加载附件
    [self requestForFuJian];
}

- (void)drawView{
    //webview
    self.webV = [[UIWebView alloc] init];
    // 避免WebView最下方出现黑线
    self.webV.backgroundColor = [UIColor clearColor];
    self.webV.opaque = NO;
    self.webV.delegate = self;
    [self.view addSubview:self.webV];
    
    //底部按钮
    //添加 请假
    UIButton *qingjiaBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btmSecBtn = qingjiaBtn;
    self.btmSecBtn.hidden = YES;
    qingjiaBtn.backgroundColor = [UIColor whiteColor];
    [qingjiaBtn setTitle:@"我要请假" forState:UIControlStateNormal];
    [qingjiaBtn setTitleColor:UIColorHex(484848) forState:UIControlStateNormal];
    qingjiaBtn.titleLabel.font = FontSystem(18);
    [qingjiaBtn addTarget:self action:@selector(actionForQingJiaBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:qingjiaBtn];
    
    //添加 参会
    UIButton *attenBtn = [UIButton buttonWithType: UIButtonTypeCustom];
    self.btmFirBtn = attenBtn;
    self.btmFirBtn.hidden = YES;
    attenBtn.backgroundColor = AppTintColor;
    [attenBtn setTitle:@"我要参会" forState:UIControlStateNormal];
    [attenBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    attenBtn.titleLabel.font = FontSystem(18);
    [attenBtn addTarget:self action:@selector(actionForAttenBtn) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:attenBtn];
    
    //列表(动态显示和大小)
    self.fujianView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.fujianView.delegate = self;
    self.fujianView.dataSource = self;
    self.fujianView.tableFooterView = [UIView new];
    [self.fujianView registerClass:[FuJianCell class] forCellReuseIdentifier:@"FuJianCell"];
    [self.view addSubview:self.fujianView];
    
    if (self.isDisPlayBtm) {
        qingjiaBtn.hidden = NO;
        attenBtn.hidden = NO;
        
        qingjiaBtn.sd_layout
        .rightSpaceToView(self.view, 0)
        .bottomSpaceToView(self.view, kBottomSafeAreaHeight)
        .widthIs(ScreenWidth * 0.5)
        .heightIs(50);
        
        attenBtn.sd_layout
        .leftSpaceToView(self.view, 0)
        .bottomSpaceToView(self.view, kBottomSafeAreaHeight)
        .widthIs(ScreenWidth * 0.5)
        .heightIs(50);
        
        self.fujianView.sd_layout
        .leftSpaceToView(self.view, 0)
        .rightSpaceToView(self.view, 0)
        .bottomSpaceToView(qingjiaBtn, 0);//高度根据返回的附件个数确定
    }else{
        qingjiaBtn.hidden = YES;
        attenBtn.hidden = YES;
        
        self.fujianView.sd_layout
        .leftSpaceToView(self.view, 0)
        .rightSpaceToView(self.view, 0)
        .bottomSpaceToView(self.view, kBottomSafeAreaHeight);//高度根据返回的附件个数确定
    }
    
    self.webV.sd_layout
    .leftSpaceToView(self.view, 0)
    .rightSpaceToView(self.view, 0)
    .bottomSpaceToView(self.fujianView, 0)
    .topSpaceToView(self.view, 0);
}


#pragma mark - datasorce
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.fujianList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    FuJianModel *model = self.fujianList[indexPath.row];
    FuJianCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FuJianCell" forIndexPath:indexPath];
    cell.model = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kFuJianItemH;
}

#pragma mark - action
//请假
- (void)actionForQingJiaBtn{
    
    ALReasonVC *reasonVC = [[ALReasonVC alloc] init];
    reasonVC.type = @"请假";
    reasonVC.nId = self.nId;
    [self.navigationController pushViewController:reasonVC animated:YES];
}

//签到
//- (void)actionForSignBtn{
//
//    DESaoMIaoController *saomaVC = [DESaoMIaoController new];
//    saomaVC.isVideoZoom = YES;
//    saomaVC.resultBlock = ^(NSString *scanResult) {
//        //字符串解密
//        NSData *dataFromBase64String = [[NSData alloc]
//                                        initWithBase64EncodedString:scanResult options:0];
//        NSString *base64Decoded = [[NSString alloc]
//                                   initWithData:dataFromBase64String encoding:NSUTF8StringEncoding];
//
//
//        PartyArticle *model = [PartyArticle  mj_objectWithKeyValues:base64Decoded];
//
//        if (self.nId != model.nId) {
//            [SMGlobalMethod showMiddleMessage:@"当前会议与二维码不匹配"];
//            return ;
//        }
//
//        NSString *url = [NSString stringWithFormat:@"%@nId=%ld",SanHuiYiKeSignIn, model.nId];
//        [DataRequest GetWithURL:url :^(id reponserObject) {
//            [SMGlobalMethod showMiddleMessage:reponserObject[@"vcResult"]];
//        }];
//    };
//    [self.navigationController pushViewController:saomaVC animated:YES];
//}

//参会
- (void)actionForAttenBtn{
    [DECommenAlert showAlertWithContent:@"确定参加会议吗?" leftBtn:@"取消" rightBtn:@"确定" tapEvent:^(NSInteger index) {
        [DECommenAlert dismiss];
        if (index == 1) {
            NSString *url = [NSString stringWithFormat:@"%@nId=%ld",SanHuiYiKeConfirmMeeting, self.nId];
            [DataRequest GetWithURL:url :^(id reponserObject) {
                [SMGlobalMethod showMiddleMessage:reponserObject[@"vcResult"]];
            }];
        }
    }];
}

#pragma mark - net
//附件
- (void)requestForFuJian{
    NSString *url = [NSString stringWithFormat:@"%@nId=%ld",SanHuiYiKeAttach, self.nId];
    [DataRequest GetWithURL:url :^(id reponserObject) {
        
        //绘制界面
        [self drawView];
        
        //加载web
        [self loadUrl];
        
        //处理数据
        self.fujianList = [FuJianModel mj_objectArrayWithKeyValuesArray:reponserObject[@"object"]];
        
        //设置最大同时下载数
        [MJDownloadManager defaultManager].maxDownloadingCount = 1;
       
        NSInteger count = self.fujianList.count;
        if (count == 0) {
            self.fujianView.sd_layout.heightIs(0.01);
        }else if(count > 0 && count <= 3){
            self.fujianView.sd_layout.heightIs(kFuJianItemH * count);
        }else{
            self.fujianView.sd_layout.heightIs(kFuJianItemH * 3);
        }
        [self.fujianView reloadData];
        
        
        if ([reponserObject[@"nStatus"] integerValue] == 0) {//0未处理
            self.btmFirBtn.hidden = NO;
            self.btmSecBtn.hidden = NO;
        }else {
            self.btmFirBtn.hidden = YES;
            self.btmSecBtn.hidden = YES;
        }
    }];
}

//加载web
- (void)loadUrl{
    NSString *url = [NSString stringWithFormat:@"%@/meet/getMeetingDetail.do?nId=%ld", BaseUrl, self.nId];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [self.webV loadRequest:request];
}

//***************************************************************************

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUD];

    //这里是js，主要目的实现对url的获取
    static  NSString * const jsGetImages =
    @"function getImages() {\
    var objs = document.getElementsByTagName(\"img\");\
    var imgScr = '';\
    \
    for(var i = 0; i<objs.length; i++) {\
    imgScr = imgScr + objs[i].src + '+';\
    \
    objs[i].onclick=function() {\
    document.location=\"myweb:imageClick:\"+this.src;\
    };\
    \
    };\
    return imgScr;\
    };";
    
    [webView stringByEvaluatingJavaScriptFromString:jsGetImages];//注入js方法
    NSString *urlResult = [webView stringByEvaluatingJavaScriptFromString:@"getImages()"];
    NSArray *imgUrlArr = [NSMutableArray arrayWithArray:[urlResult componentsSeparatedByString:@"+"]];
    //urlResurlt 就是获取到得所有图片的url的拼接；imgUrlArr就是所有图片Url的数组
    DLog(@"imgUrlArr--:%@",imgUrlArr);
    
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    //将url转换为string
    NSString *requestString = [[request URL] absoluteString];
    
    //    DLog(@"requestString is %@",requestString);
    
    //hasPrefix 判断创建的字符串内容是否以pic:字符开始
    if ([requestString hasPrefix:@"myweb:imageClick:"]) {
        //如果是购买图书不做放大处理
        if ([requestString hasPrefix:@"myweb:imageClick:http://djy.ewanyuan.cn/shopapp/"] ||
            [requestString hasPrefix:@"myweb:imageClick:https://djy.ewanyuan.cn/shopapp/"]) {
            return YES;
        }
        
        NSString *imageUrl = [requestString substringFromIndex:@"myweb:imageClick:".length];
        CGSize size = [UIImage getImageSizeWithURL:imageUrl];
        CGFloat height = (MAIN_SIZE.width - 10) *size.height /size.width;
        
        //创建视图并显示图片
        [self showBigImage:imageUrl andHeight:height];
        
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
}

#pragma mark 显示大图片
-(void)showBigImage:(NSString *)imageUrl andHeight:(CGFloat)height {
    //创建灰色透明背景，使其背后内容不可操作
    self.bgBtnView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, MAIN_SIZE.width, MAIN_SIZE.height)];
    [self.bgBtnView setBackgroundColor:[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.7]];
    [self.bgBtnView addTarget:self action:@selector(removeBigImage) forControlEvents:UIControlEventTouchUpInside];
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgBtnView];
    
    //创建边框视图
    UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_SIZE.width, height + 10)];
    //将图层的边框设置为圆脚
    borderView.layer.cornerRadius = 8;
    borderView.layer.masksToBounds = YES;
    //给图层添加一个有色边框
    borderView.layer.borderWidth = 5;
    borderView.layer.borderColor = [[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.7] CGColor];
    [borderView setCenter:self.bgBtnView.center];
    [self.bgBtnView addSubview:borderView];
    
    //创建显示图像视图
    self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, MAIN_SIZE.width - 10, height)];
    self.imgView.userInteractionEnabled = YES;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    [borderView addSubview:self.imgView];
    
    //添加关闭视图手势
    [self.imgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeBigImage)]];
    
    //添加捏合手势
    [self.imgView addGestureRecognizer:[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)]];
    
    //添加保存图片到相册
    UIButton *saveImgBtn = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_SIZE.width/2 - 20, CGRectGetMaxY(borderView.frame), 40, 40)];
    [saveImgBtn setTitle:@"保存" forState:UIControlStateNormal];
    [saveImgBtn addTarget:self action:@selector(saveImage) forControlEvents:UIControlEventTouchUpInside];
    [self.bgBtnView addSubview:saveImgBtn];
    
}

//关闭大图
-(void)removeBigImage {
    self.bgBtnView.hidden = YES;
}
//捏合手势
- (void) handlePinch:(UIPinchGestureRecognizer*) recognizer {
    //缩放:设置缩放比例
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}
//保存图片
- (void)saveImage {
    UIImageWriteToSavedPhotosAlbum(self.imgView.image,self,@selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:),NULL); // 写入相册
    
}
-(void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.bgBtnView animated:YES];
    hud.mode = MBProgressHUDModeText;
    
    if(!error){
        hud.label.text = @"保存成功";
    }
    else{
        hud.label.text = @"保存失败";
    }
    
    hud.margin = 10.f;
    [hud setOffset:CGPointMake(0, 150.0f)];
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:3.0];
}


@end
