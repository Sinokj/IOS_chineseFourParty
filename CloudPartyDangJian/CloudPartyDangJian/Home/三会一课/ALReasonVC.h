//
//  ALReasonVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "ViewController.h"
#import "PartyArticleListModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ALReasonVC : ViewController
@property (nonatomic, copy)NSString *type;//类型
@property (nonatomic, assign)NSInteger nId;
@end

NS_ASSUME_NONNULL_END
