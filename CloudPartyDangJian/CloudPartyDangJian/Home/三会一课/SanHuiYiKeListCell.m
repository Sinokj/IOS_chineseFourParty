//
//  SanHuiYiKeListCell.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/8.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "SanHuiYiKeListCell.h"
#import "DyfTool.h"

@implementation SanHuiYiKeListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.iconWidthConstraint.constant = MYDIMESCALEW(60);
    // 起名字起反了。
    self.stateWithConstraint.constant = MYDIMESCALEW(50);
    self.stateHeightConstraint.constant = MYDIMESCALEW(20);
    self.stateBtn.layer.cornerRadius = MYDIMESCALEW(10);
    self.stateBtn.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
    self.stateBtn.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



- (void)setModel:(PartyArticle *)model {
    if (_model != model) {
        _model = nil;
        _model = model;
        
        self.detailLab.text = model.vcName;
//        self.timeLab.text = model.vcType;
        
        self.timeLab.text = [NSString stringWithFormat:@"%@",[model.dtBegin substringToIndex:model.dtBegin.length - 3]];
        
//        if (model.nStatus == 1) {
//            [self.stateBtn setImage:ImageName(@"已读") forState:UIControlStateNormal];
////            self.stateBtn.text = @"已读";
////            self.stateBtn.textColor = [UIColor colorWithWhite:0.5 alpha:1];
//        }else {
//            [self.stateBtn setImage:ImageName(@"未读") forState:UIControlStateNormal];
////            self.stateBtn.text = @"未读";
////            self.stateBtn.textColor = [UIColor redColor];
//        }
        
        if (model.nStatus > 0) {
            
            self.stateBtn.text = @"已读";
            
            self.stateBtn.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            self.stateBtn.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1].CGColor;
            self.stateBtn.backgroundColor = [UIColor whiteColor];
            
        }else if (model.nStatus == 0) {
            
            self.stateBtn.text = @"未读";
            self.stateBtn.textColor = AppTintColor;
            self.stateBtn.layer.borderColor = AppTintColor.CGColor;
//            self.stateBtn.backgroundColor = AppTintColor;
            self.stateBtn.backgroundColor = [UIColor whiteColor];
            
            
        }
        
    }
}


@end
