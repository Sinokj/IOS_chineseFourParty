//
//  FuJianCell.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PartyArticleListModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface FuJianCell : UITableViewCell
@property (nonatomic, strong)FuJianModel *model;
@end

NS_ASSUME_NONNULL_END
