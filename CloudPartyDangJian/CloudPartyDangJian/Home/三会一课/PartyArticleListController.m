//
//  PartyArticleListController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/9/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "PartyArticleListController.h"
#import "WebViewController.h"
#import "PartyArticleDetailVC.h"
#import "SanHuiYiKeListCell.h"
#import "PartyPageVC.h"

@interface PartyArticleListController ()<UITableViewDelegate,UITableViewDataSource,WebViewDelegate,UIScrollViewDelegate>

@property (nonatomic, strong) PartyArticleListModel *partyModel;
@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSIndexPath *indexp;

@property (nonatomic, strong) SegmentControlPackage *segmentControl;
@property (nonatomic, strong) NSMutableArray *markTitleArr;
@property (nonatomic, strong) NSString *seletMark;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) NSMutableArray *tableArr;
@property (nonatomic, assign) NSInteger currentNum;
@property (nonatomic, strong) NSNumber *nArticleId;



@property (nonatomic, strong)PartyPageVC *pageVC;
@end

@implementation PartyArticleListController

- (NSMutableArray *)tableArr {
    if (_tableArr == nil) {
        _tableArr = [NSMutableArray array];
    }
    return _tableArr;
}

- (NSMutableArray *)markTitleArr {
    if (_markTitleArr == nil) {
        _markTitleArr = [NSMutableArray array];
        NSArray *arr = self.responsObject[@"result"];
        for (int i = 0; i < arr.count; i ++) {
            NSString *name = [arr[i] objectForKey:@"nTagName"];
            [_markTitleArr addObject:name];
        }
    }
    return _markTitleArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.seletMark = @"";
    self.currentNum = 0;
//    [self requestData];
//    if (self.nArticleId) {
//        [DataRequest PostWithURL:@"partyarticle/getPartyArticleStatus" parameter:@{@"nArticleId":self.nArticleId} :^(id reponserObject) {
//
//        }];
//    }
   
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"三会一课";
    
    self.pageVC = [PartyPageVC new];
    [self addChildViewController:self.pageVC];
    [self.view addSubview:self.pageVC.view];
    self.pageVC.view.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight - kTopHeight);
    
    //赋值频道
    self.pageVC.channels = self.markTitleArr;
    
//    [self requestData];
//    [self setupSegment];
//    [self setupMainView];
}

- (void)setupMainView {
    
    //底部ScrollView
    self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.segmentControl.frame), MAIN_SIZE.width, MAIN_SIZE.height - 64 - 45)];
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.delegate = self;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.contentSize = CGSizeMake(self.markTitleArr.count * MAIN_SIZE.width, 0);
    
    //TableView
    for (int i = 0; i < self.markTitleArr.count; i ++) {
        
        self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(i *MAIN_SIZE.width, 0, ScreenWidth, CGRectGetHeight(self.mainScrollView.frame)) style:(UITableViewStylePlain)];
        self.tableV.delegate = self;
        self.tableV.dataSource = self;
        self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.mainScrollView addSubview:self.tableV];
        
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        self.tableV.mj_header = header;
        [self.tableV.mj_header beginRefreshing];
//        @weakify(self);
//        self.tableV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            @strongify(self);
//            [self requestData];
//        }];
        self.tableV.tableFooterView = [UIView new];
        
        self.tableV.tag = i;
        [self.tableArr addObject:self.tableV];
    }
    
    
}
- (void)refreshData {
    [self requestData];
    [self.tableV.mj_header endRefreshing];
}

//标签栏
- (void)setupSegment {
    
    __weak typeof(self) weakSelf = self;
    self.segmentControl = [[SegmentControlPackage alloc] initwithTitleArr:self.markTitleArr iconArr:nil SCType:SCType_SelectChange];
    self.segmentControl.frame = CGRectMake(0, 0, MAIN_SIZE.width, 45);
    self.segmentControl.selectBtnWID = 60;
//    self.segmentControl.titleFont = FontSystem(17);
    self.segmentControl.selectType = ^(NSInteger selectIndex,NSString *selectIndexTitle){
        
        weakSelf.currentNum = selectIndex;
        //点击标签，设置scrollView的偏移量
        [weakSelf.mainScrollView setContentOffset:CGPointMake(selectIndex *MAIN_SIZE.width, 0) animated:YES];
        
        //点击标签回调
        weakSelf.seletMark = weakSelf.markTitleArr[selectIndex];
        [weakSelf requestData];
        
        
    };
    [self.view addSubview:self.segmentControl];
    
}

- (void)requestData {
    NSUserDefaults *userDe = [NSUserDefaults standardUserDefaults];
    NSString *partyID = [userDe objectForKey:@"partyID"];
    
    NSString *url = [NSString stringWithFormat:@"%@nCommitteeId=%@&vcType=%@",SanHuiYiKeList, partyID?partyID:@2, self.seletMark];
    
    [DataRequest GetWithURL:url :^(id reponserObject) {

        self.partyModel = [PartyArticleListModel mj_objectWithKeyValues:reponserObject];
        self.tableV = self.tableArr[self.currentNum];
        [self.tableV reloadData];
        [self.tableV.mj_header endRefreshing];
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.partyModel.objects.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SanHuiYiKeListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[NSBundle mainBundle] loadNibNamed:@"SanHuiYiKeListCell" owner:nil options:nil][0];
    }
    
    cell.model = self.partyModel.objects[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SanHuiYiKeListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            cell.stateBtn.text = @"已读";
            cell.stateBtn.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            cell.stateBtn.layer.borderColor = [UIColor colorWithWhite:0.5 alpha:1].CGColor;
            cell.stateBtn.backgroundColor = [UIColor whiteColor];
        });
    }
    
    PartyArticleDetailVC *detailVC = [[PartyArticleDetailVC alloc] init];
    detailVC.nId = [self.partyModel.objects[indexPath.row] nId];
    detailVC.type = [self.partyModel.objects[indexPath.row] vcType];
    [self.navigationController pushViewController:detailVC animated:YES];
    
    
//    WebViewController *webVC = [WebViewController new];
//
//    webVC.type = @"1";
//
//    webVC.str = [NSString stringWithFormat:@"%@/partyarticle/getPartyArticleContent.do?nId=%ld", BaseUrl, (long)self.partyModel.objects[indexPath.row].nId];
//
//    self.nArticleId = @(self.partyModel.objects[indexPath.row].nId);
//
//    PartyArticle *neList = self.partyModel.objects[indexPath.row];
//
//    webVC.tit = self.partyModel.objects[indexPath.row].vcType;
//    webVC.delegate = self;
//    self.indexp = indexPath;
//
//    webVC.vcType = @"三会一课";
//
//    webVC.articleId = neList.nId;
//
//    webVC.shareImage = nil;
//    webVC.shareTitle = neList.vcTitle;
//    webVC.shareDescribe = neList.vcDeptName;
//
//    [self.navigationController pushViewController:webVC animated:YES];
    
}

- (void)getNewState:(NSInteger)state {
    
    self.partyModel.objects[self.indexp.row].nStatus = state;
    self.tableV = self.tableArr[self.currentNum];
//    [self.tableV reloadRowsAtIndexPaths:@[self.indexp] withRowAnimation:(UITableViewRowAnimationNone)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return MYDIMESCALEH(100);
}
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//
//}


//scrollView代理方法
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView isEqual:self.mainScrollView]) {
        self.currentNum = self.mainScrollView.contentOffset.x / MAIN_SIZE.width;
        self.segmentControl.selectIndex = self.currentNum;
        
        return;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
}



@end
