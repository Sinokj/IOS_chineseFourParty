//
//  PartySubVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PartySubVC : UIViewController<UITableViewDelegate, UITableViewDataSource>
/** 根视图 */
@property (nonatomic, strong)UITableView *tableView;
//页数
@property (nonatomic, assign)NSInteger page;
//models
@property (nonatomic, strong)NSMutableArray *modelArray;
@end

NS_ASSUME_NONNULL_END
