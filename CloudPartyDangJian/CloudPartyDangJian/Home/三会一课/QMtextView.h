//
//  QMtextView.h
//  Q_M_S
//
//  Created by hao on 16/5/11.
//  Copyright © 2016年 sanmi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QMtextView : UITextView
/** 占位文字 */
@property (nonatomic, copy) NSString *placeholder;
/** 占位文字的颜色 */
@property (nonatomic, strong) UIColor *placeholderColor;
/** 输入字数限制 */
@property (nonatomic, assign)NSInteger limitedNum;
@end
