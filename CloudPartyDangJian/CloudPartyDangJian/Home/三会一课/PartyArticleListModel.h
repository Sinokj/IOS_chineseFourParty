//
//  PartyArticleListModel.h
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/9/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PartyArticle;
@interface PartyArticleListModel : NSObject

@property (nonatomic, strong) NSArray<PartyArticle *> *objects;

@end

@interface PartyArticle : NSObject

@property (nonatomic, copy) NSString *dtBegin;
@property (nonatomic, copy) NSString *vcTitle;
@property (nonatomic, assign) NSInteger nStatus;
@property (nonatomic, copy) NSString *vcType;
@property (nonatomic, assign) NSInteger nId;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *vcDeptName;
@property (nonatomic, copy) NSString *vcName;


@end

//文档附件
@interface FuJianModel: NSObject
@property (nonatomic, copy)NSString *mId;
@property (nonatomic, copy)NSString *nId;
@property (nonatomic, copy)NSString *vcName;
@property (nonatomic, copy)NSString *vcUrl;
@property (nonatomic, copy)NSURL *fileUrl;//文件本地路劲

@property (nonatomic, assign) NSInteger              nStatus; //0未处理 1 审核通过 2审核未通过
@end

/*
 "dtReg": "2018-05-04 11:40:47",
 "vcTitle": "通信事业部党支部第一次例会——支委换届选举大会",
 "nStatus": 1,
 "vcType": "支部党员大会",
 "nId": 1,
 "url": "h
 */


