//
//  PartyPageVC.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/6.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "YPTabBarController.h"
#import "PartySubVC.h"
NS_ASSUME_NONNULL_BEGIN

@interface PartyPageVC : YPTabBarController
@property (nonatomic, strong)NSArray *channels;
@end

NS_ASSUME_NONNULL_END
