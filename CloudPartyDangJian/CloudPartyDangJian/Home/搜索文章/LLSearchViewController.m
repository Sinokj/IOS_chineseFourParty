//
//  LLSearchViewController.m
//  LLSearchView
//
//  Created by 王龙龙 on 2017/7/25.
//  Copyright © 2017年 王龙龙. All rights reserved.
//

#import "LLSearchViewController.h"

#import "LLSearchSuggestionVC.h"
#import "LLSearchView.h"
#import "LLSearchViewConst.h"
#import "DataRequest.h"
#import "ListModel.h"
#import "SearchResVC.h"
#import "WebViewController.h"

@interface LLSearchViewController ()<UISearchBarDelegate>

@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) LLSearchView *searchView;
@property (nonatomic, strong) NSMutableArray *hotArray;
@property (nonatomic, strong) NSMutableArray *historyArray;
@property (nonatomic, strong) LLSearchSuggestionVC *searchSuggestVC;

@end

@implementation LLSearchViewController

- (NSMutableArray *)hotArray
{
    if (!_hotArray) {
        self.hotArray = [[NSMutableArray alloc] init];
    }
    return _hotArray;
}

- (NSMutableArray *)historyArray
{
    if (!_historyArray) {
        _historyArray = [NSKeyedUnarchiver unarchiveObjectWithFile:KHistorySearchPath];
        if (!_historyArray) {
            self.historyArray = [NSMutableArray array];
        }
    }
    return _historyArray;
}


- (LLSearchView *)searchView
{
    if (!_searchView) {
        self.searchView = [[LLSearchView alloc] initWithFrame:CGRectMake(0, 0, KScreenWidth, KScreenHeight - 0) hotArray:self.hotArray historyArray:self.historyArray];
        __weak LLSearchViewController *weakSelf = self;
        _searchView.tapAction = ^(NSString *str) {
            [weakSelf pushToSearchResultWithSearchStr:str];
        };
    }
    return _searchView;
}


- (LLSearchSuggestionVC *)searchSuggestVC
{
    
    if (!_searchSuggestVC) {
        self.searchSuggestVC = [[LLSearchSuggestionVC alloc] init];
        _searchSuggestVC.view.frame = CGRectMake(0, 0, KScreenWidth, KScreenHeight - 0);
        _searchSuggestVC.view.hidden = YES;
        __weak LLSearchViewController *weakSelf = self;
        _searchSuggestVC.searchBlock = ^(NewList *model) {
            // 猜测
            WebViewController *webVC = [WebViewController new];
            
            webVC.type = @"1";
            webVC.str = model.url;
            
            NewList *neList = model;
            
            
            webVC.canShare = neList.nShared;
            
            webVC.tit = neList.vcType;
            
            webVC.shareImage = neList.vcPath;
            webVC.shareTitle = neList.vcTitle;
            webVC.shareDescribe = neList.vcDescribe;
            
            webVC.articleId = neList.nId;
            webVC.vcType = neList.vcType;
            
            [weakSelf.navigationController pushViewController:webVC animated:YES];
            
        };
    }
    return _searchSuggestVC;
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!_searchBar.isFirstResponder) {
        [self.searchBar becomeFirstResponder];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
       [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:AppTintColor] forBarMetrics:UIBarMetricsDefault];
    // 回收键盘
    [self.searchBar resignFirstResponder];
    _searchSuggestVC.view.hidden = YES;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setBarButtonItem];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.searchView];
    [self.view addSubview:self.searchSuggestVC.view];
    [self addChildViewController:_searchSuggestVC];
}


- (void)setBarButtonItem
{
    self.navigationItem.leftBarButtonItem = nil;
    
    [self.navigationItem setHidesBackButton:YES];
    // 创建搜索框
    [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"search_square"] forState:UIControlStateNormal];
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(5, 7, self.view.frame.size.width, 30)];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(titleView.frame) - 15, 30)];
    searchBar.delegate = self;
    searchBar.showsCancelButton = YES;
    searchBar.alpha = 0.0f;
    searchBar.backgroundImage = [UIImage new];
    searchBar.barTintColor = [UIColor whiteColor];
    

    
    UITextField *searchTextField;
    if (@available(iOS 13.0, *)) {
        searchTextField = searchBar.searchTextField;
    }
    else {
        searchTextField = [searchBar valueForKey:@"_searchField"];
    }
    UIView *leftV = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 25, 8)];
    searchTextField.leftView = leftV;
    searchTextField.leftViewMode = UITextFieldViewModeAlways;
    if (searchTextField) {
        searchTextField.font = [UIFont systemFontOfSize:15.0];
        [searchTextField setBackgroundColor:RGB(224, 224, 224)];
        searchTextField.layer.cornerRadius = 14;
        
//        searchTextField.layer.borderColor = [UIColor colorWithRed:0/255.0 green:255/255.0 blue:0/255.0 alpha:1].CGColor;
//        searchTextField.layer.borderWidth = 2;
        searchTextField.borderStyle = UITextBorderStyleNone;
        searchTextField.layer.masksToBounds = YES;
        [searchTextField setTintColor: AppTintColor];
    }
    


    
    [searchBar setImage:[UIImage imageNamed:@""] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    UIButton *cancleBtn = [searchBar valueForKey:@"cancelButton"];
    //修改标题和标题颜色
    [cancleBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancleBtn setTitleColor:AppTintColor forState:UIControlStateNormal];
    cancleBtn.titleLabel.font = FontSystem(17);
    [titleView addSubview:searchBar];
    self.searchBar = searchBar;
//    [self.searchBar becomeFirstResponder];
    self.navigationItem.titleView = titleView;
    
//    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:self action:nil];
}

//调用方法
- (UIImage*)searchFieldBackgroundImage {
    UIColor*color = [UIColor whiteColor];
    CGFloat cornerRadius = 5;
    CGRect rect =CGRectMake(0,0,28,28);
    
    UIBezierPath*roundedRect = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:cornerRadius];
    roundedRect.lineWidth=0;
    
    UIGraphicsBeginImageContextWithOptions(rect.size,NO,0.0f);
    [color setFill];
    [roundedRect fill];
    [roundedRect stroke];
    [roundedRect addClip];
    
    UIImage *image =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}



- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0.15 animations:^{
            self.searchBar.alpha = 1.0f;
        }];
    });
     [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];

}

- (void)presentVCFirstBackClick:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}



/** 点击取消 */
- (void)cancelDidClick
{
    [self.searchBar resignFirstResponder];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (void)pushToSearchResultWithSearchStr:(NSString *)str
{
    [self setHistoryArrWithStr:str];
    
    [DataRequest GetWithURL:[NSString stringWithFormat:@"%@vcTitle=%@&nPageNo=%zd&nPageSize=15&nCommitteeId=%@", searchNews, str,1,[STUtils objectForKey:KDangJianType]] :^(id reponserObject) {
        
        SearchResVC *list = [SearchResVC new];
        
//        list.type = type;
        list.text = str;
        list.listM = [ListModel mj_objectWithKeyValues:reponserObject];
        
        list.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:list animated:YES];
        
    }];
}

- (void)setHistoryArrWithStr:(NSString *)str
{
    for (int i = 0; i < _historyArray.count; i++) {
        if ([_historyArray[i] isEqualToString:str]) {
            [_historyArray removeObjectAtIndex:i];
            break;
        }
    }
    [_historyArray insertObject:str atIndex:0];
    [NSKeyedArchiver archiveRootObject:_historyArray toFile:KHistorySearchPath];
}


#pragma mark - UISearchBarDelegate -


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self pushToSearchResultWithSearchStr:searchBar.text];
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchBar.text == nil || [searchBar.text length] <= 0) {
        _searchSuggestVC.view.hidden = YES;
        [self.view bringSubviewToFront:_searchView];
    } else {
        _searchSuggestVC.view.hidden = NO;
        [self.view bringSubviewToFront:_searchSuggestVC.view];
        [_searchSuggestVC searchTestChangeWithTest:searchBar.text];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
