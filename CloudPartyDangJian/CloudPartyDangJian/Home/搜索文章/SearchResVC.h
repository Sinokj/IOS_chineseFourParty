//
//  SearchResVC.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/10/30.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "ListModel.h"

@interface SearchResVC : ViewController

//@property (nonatomic, strong) NSString *type;

@property (nonatomic, strong) NSString *text;

@property (nonatomic, strong) ListModel *listM;

@end
