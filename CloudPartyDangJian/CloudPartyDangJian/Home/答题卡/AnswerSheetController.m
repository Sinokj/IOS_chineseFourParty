//
//  AnswerSheetController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/26.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "AnswerSheetController.h"
#import "ExamsModel.h"
#import "ScoreController.h"
#import <Masonry.h>

#define KGreenColor  RGB(67, 185, 100)

#define KFontAnswerSheetColor  RGB(74, 74, 74)
@interface AnswerSheetController ()

@end

@implementation AnswerSheetController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = AppTintColor;
    
    self.title = @"答题卡";

    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];

    [self prepareLayout];
}

- (void)prepareLayout {
    
    // 方块scrollView
    UIScrollView *bgScroll = [[UIScrollView alloc] initWithFrame:CGRectZero];
    
    bgScroll.backgroundColor = [UIColor whiteColor];
    
//    bgScroll.layer.cornerRadius = 10;
//    bgScroll.layer.masksToBounds = true;
    
    [self.view addSubview:bgScroll];
    
    [bgScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.bottom.offset(0);
    }];
    
    // 交卷按钮
    if (!self.isOver) {
        
        UIButton *btnItem = [[UIButton alloc] init];
        [btnItem setTitle:@"确认交卷" forState:(UIControlStateNormal)];
        btnItem.backgroundColor = AppTintColor;
        btnItem.titleLabel.textAlignment = NSTextAlignmentCenter;
        [btnItem setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [btnItem addTarget:self action:@selector(sureClick) forControlEvents:UIControlEventTouchUpInside];
        
        [self.view addSubview:btnItem];
        
        CGFloat height = 48.0;
        btnItem.layer.cornerRadius = height/2;
        btnItem.layer.borderColor = [UIColor whiteColor].CGColor;
        btnItem.layer.borderWidth = 2.0;
        
        btnItem.sd_layout
        .centerXEqualToView(self.view)
        .bottomSpaceToView(self.view, 60 + kBottomSafeAreaHeight)
        .widthIs(ScreenWidth - 60)
        .heightIs(48);
        btnItem.sd_cornerRadiusFromHeightRatio = @(0.5);
        
        
//        self.view.backgroundColor = UIColorFromHex(0xeeeeec);
        //        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnItem];
    }else {
//        self.view.backgroundColor = UIColorFromHex(0xe85454);
    }
    
//    UIImageView *titleImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 17, 15, 35)];
//    titleImage.image = [UIImage imageNamed:@"icon_test"];
//    titleImage.contentMode = UIViewContentModeScaleAspectFit;
//    [bgScroll addSubview:titleImage];
    
    
//    UIImageView *testIcon = [UIImageView new];
//    testIcon.frame = CGRectMake(15, 17, 22, 22);
//    testIcon.image = [UIImage imageNamed:@"icon_test222"];
//    [bgScroll addSubview:testIcon];
    
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, ScreenWidth - 54, 30)];
    titleLabel.center = CGPointMake(ScreenWidth * 0.5, 15);
    [bgScroll addSubview:titleLabel];
    titleLabel.font = FontSystem(16);
    titleLabel.textColor = KFontAnswerSheetColor;
    titleLabel.numberOfLines = 0;
    titleLabel.text = [NSString stringWithFormat:@"本试卷共有%ld道题，已答%ld道",self.examList.nTopicsTotal,self.answerArr.count];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    CGFloat lastBottom = titleLabel.bottom + 10;
    
    CALayer *line1 = [CALayer layer];
    line1.backgroundColor = UIColorFromHex(0xbbbbbb).CGColor;
    
    [bgScroll.layer addSublayer:line1];
    line1.frame = CGRectMake(15, lastBottom - 3, ScreenWidth - 55, 1 / ScreenScale);
    
    
//    单选
    if (self.singleDic.allKeys.count != 0 && self.singleDic != nil) {
        
        UILabel *singleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, titleLabel.bottom + 15, ScreenWidth - 24, 25)];
        
        [bgScroll addSubview:singleLabel];
        
        singleLabel.text = @"单选题";
        singleLabel.textColor = KFontAnswerSheetColor;
        singleLabel.font = FontSystem(17);
        
        UIView *singleBgView = [[UIView alloc] init];
        [bgScroll addSubview:singleBgView];
        
        NSInteger number = 1;
        
        UIButton *lastBtn = nil;
        CGFloat margin = 18;
        
        NSInteger rowCount = 5;
        CGFloat itemWidth = (ScreenWidth - 24 - 28 - (rowCount - 1) * margin) / rowCount;
        for (int i = 0; i < self.singleDic.allKeys.count; i++) {
            
            UIButton *btn = [UIButton buttonWithType:(UIButtonTypeSystem)];
            
            [singleBgView addSubview:btn];
            
            CGFloat top;
   
            NSInteger row = (i +  0.9999999) / rowCount;
            NSInteger col = i % rowCount;
            
            btn.frame = CGRectMake(14 + (margin + itemWidth) * col, 10 + row * (itemWidth + 12), itemWidth, itemWidth);
            
            
//            BOOL needLineFeed = lastBtn.right + 50 + 10 > ScreenWidth - 54;
//            if(lastBtn == nil) {
//                top = 5;
//            } else if (needLineFeed) {
//                top = lastBtn.bottom + 12;
//                number++;
//            } else{
//
//                top = lastBtn.top;
//            }
//
//            btn.frame = CGRectMake(lastBtn == nil ? 14 : needLineFeed ? 14 : lastBtn.right + 18, top, 50, 50);
            
//            btn.layer.cornerRadius = 15;
            
//            [btn setBackgroundColor:[UIColor whiteColor]];
//            [btn setBackgroundImage:[UIImage imageNamed:@"bg_suject_2"] forState:(UIControlStateNormal)];
            btn.layer.borderWidth = 1;
            btn.layer.borderColor = UIColorHex(0xbbbbbb).CGColor;
            btn.layer.cornerRadius = itemWidth/2;
            btn.layer.masksToBounds = YES;
            [btn setTitleColor:UIColorFromHex(0xbbbbbb) forState:(UIControlStateNormal)];
            
            [btn setTitle:[NSString stringWithFormat:@"%ld",[[self getSingleArray][i] integerValue] + 1] forState:(UIControlStateNormal)];
            
            Exam *exam = [self.singleDic objectForKey: [self getSingleArray][i]];
            for (NSDictionary *dic in self.answerArr) {
                
                if ([[NSString stringWithFormat:@"%ld",exam.nId] isEqualToString: [NSString stringWithFormat:@"%@",dic[@"nTitleId"]]]) {
                    if (self.isOver) {// 已考完
                        if ([exam.vcAnswer isEqualToString:dic[@"vcAnswer"]]) {
                            // 正确
                            btn.backgroundColor = AppTintColor;
                            [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                            btn.layer.borderColor = AppTintColor.CGColor;
                        }else {
                            // 错误
                            [btn setTitleColor:KGreenColor forState:(UIControlStateNormal)];
                            btn.layer.borderColor = KGreenColor.CGColor;
                        }
                        
                    }else {// 待提交

//                        btn.backgroundColor = AppTintColor;
//                        [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
//                        btn.layer.borderColor = AppTintColor.CGColor;
                        [btn setTitleColor:AppTintColor forState:(UIControlStateNormal)];
                        btn.layer.borderColor = AppTintColor.CGColor;
                    }
                    
                    break;
                }
            }
            
            btn.tag = 1000 + [[self getSingleArray][i] integerValue];
            
            [btn addTarget:self action:@selector(listSelect:) forControlEvents:(UIControlEventTouchUpInside)];
            
            lastBtn = btn;
        }
        
        singleBgView.frame = CGRectMake(0, singleLabel.bottom + 5, ScreenWidth - 24, lastBtn.bottom + 25);

        lastBottom = singleBgView.bottom + 5;
        
    }
//    多选
    CALayer *line2 = [CALayer layer];
    line2.backgroundColor = UIColorFromHex(0xbbbbbb).CGColor;
    
    [bgScroll.layer addSublayer:line2];
    line2.frame = CGRectMake(15, lastBottom - 3, ScreenWidth - 55, 1 / ScreenScale);
    
    if (self.doubleDic.allKeys.count != 0 && self.doubleDic != nil) {
        
        UILabel *doubleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, lastBottom, ScreenWidth, 25)];
        [bgScroll addSubview:doubleLabel];
        
        doubleLabel.backgroundColor = [UIColor whiteColor];
        doubleLabel.text = @"多选题";
        doubleLabel.font = FontSystem(17);
        doubleLabel.textColor = KFontAnswerSheetColor;
        UIView *doubleBgView = [[UIView alloc] init];
        doubleBgView.backgroundColor = [UIColor whiteColor];
        [bgScroll addSubview:doubleBgView];
        
        NSInteger number = 1;
        UIButton *lastBtn = nil;
        for (int i = 0; i < self.doubleDic.allKeys.count; i++) {
            
            UIButton *btn = [UIButton buttonWithType:(UIButtonTypeSystem)];
            
            [doubleBgView addSubview:btn];
            
            CGFloat top;
            CGFloat margin = 18;
            
            NSInteger rowCount = 5;
            CGFloat itemWidth = (ScreenWidth - 24 - 28 - (rowCount - 1) * margin) / rowCount;
            NSInteger row = (i +  0.9999999) / rowCount;
            NSInteger col = i % rowCount;
            
            btn.frame = CGRectMake(14 + (margin + itemWidth) * col, 10 + row * (itemWidth + 12), itemWidth, itemWidth);
            
            btn.layer.borderWidth = 1;
            btn.layer.cornerRadius = itemWidth/2;
            btn.layer.masksToBounds = YES;
            btn.layer.borderColor = UIColorHex(0xbbbbbb).CGColor;
            
//            BOOL needLineFeed = lastBtn.right + 50 + 18 > ScreenWidth - 54;
//            if(lastBtn == nil) {
//                top = 5;
//            } else if (needLineFeed) {
//                top = lastBtn.bottom + 12;
//                number++;
//            } else{
//
//                top = lastBtn.top;
//            }
//
//            btn.frame = CGRectMake(lastBtn == nil ? 14 : needLineFeed ? 14 : lastBtn.right + 18, top, 50, 50);
//
//            btn.layer.cornerRadius = 15;
            
//            [btn setBackgroundImage:[UIImage imageNamed:@"bg_suject_2"] forState:(UIControlStateNormal)];
            [btn setTitleColor:UIColorFromHex(0xbbbbbb) forState:(UIControlStateNormal)];
            
            [btn setTitle:[NSString stringWithFormat:@"%ld",[[self getDoubleArray][i] integerValue] + 1] forState:(UIControlStateNormal)];
            
            Exam *exam = [self.doubleDic objectForKey: [self getDoubleArray][i]];
            for (NSDictionary *dic in self.answerArr) {
                
                if ([[NSString stringWithFormat:@"%ld",exam.nId] isEqualToString: [NSString stringWithFormat:@"%@",dic[@"nTitleId"]]]) {
                    
                    if (self.isOver) {
                        
                        if ([[self sortStr:exam.vcAnswer ] isEqualToString:[self sortStr:dic[@"vcAnswer"]]]) {
                            //正确
                            btn.backgroundColor = AppTintColor;
                            [btn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
                            btn.layer.borderColor = AppTintColor.CGColor;
                            
                        }else {
                            //错误
                            [btn setTitleColor:KGreenColor forState:(UIControlStateNormal)];
                            btn.layer.borderColor = KGreenColor.CGColor;
                        }
                    }else {
                        [btn setTitleColor:AppTintColor forState:(UIControlStateNormal)];
                        btn.layer.borderColor = AppTintColor.CGColor;
                    }
                    

                    break;
                }
            }
            
            btn.tag = 1000 + [[self getDoubleArray][i] integerValue];
            
            [btn addTarget:self action:@selector(listSelect:) forControlEvents:(UIControlEventTouchUpInside)];
            
            lastBtn = btn;
        }
//        doubleBgView.frame = CGRectMake(0, doubleLabel.bottom + 5, ScreenWidth - 24, number * 40);
        doubleBgView.frame = CGRectMake(0, doubleLabel.bottom + 5, ScreenWidth - 24, lastBtn.bottom + 25);
        
        lastBottom = doubleBgView.bottom + 5;


    }
    
    bgScroll.contentSize = CGSizeMake(ScreenWidth - 24, lastBottom + 5);
    bgScroll.bounces = false;

}

- (NSString *)sortStr:(NSString *)str {
    
    NSArray * arr = [[str substringToIndex:str.length - 1] componentsSeparatedByString:@","];
    
    NSArray * sortArr = [arr sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        
        NSString *obj11 = obj1;
        NSString *obj22 = obj2;
        
        return [obj11 compare:obj22];
    }];
    
    NSMutableString *retr = [NSMutableString string];
    
    for (NSString *arrStr in sortArr) {
        [retr appendString:[NSString stringWithFormat:@"%@,",arrStr]];
    }
    
    return retr;
}

- (NSArray *)getSingleArray {
    
    return [self.singleDic.allKeys sortedArrayUsingSelector:@selector(compare:)];
}

- (NSArray *)getDoubleArray {
    
    return [self.doubleDic.allKeys sortedArrayUsingSelector:@selector(compare:)];
}

- (void)sureClick {
    
    
    
    if (self.examList.nTopicsTotal > self.answerArr. count) {
        [MBProgressHUD showMessage:[NSString stringWithFormat:@"本试卷共有%ld道题，还有%ld道没答",self.examList.nTopicsTotal,(self.examList.nTopicsTotal-self.answerArr.count)] RemainTime:2 ToView:self.view userInteractionEnabled:NO];
        return;
    }
    
    [DataRequest GetWithURL:[NSString stringWithFormat:@"%@?nTopicsId=%ld",SubmitURL,self.examList.nId] :^(id reponserObject) {
        
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            ScoreController *scoreC = [ScoreController new];
            
            scoreC.nScore = [reponserObject[@"nScore"] integerValue];
            
            scoreC.isPass = reponserObject[@"isPass"];
            
            [self.navigationController pushViewController:scoreC animated:YES];
        }
    
    }];
}

- (void)listSelect:(UIButton *)btn {
    
    DLog(@"%ld",btn.tag - 1000);
    
    self.AnswerSheetBlock(btn.tag - 1000);
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
