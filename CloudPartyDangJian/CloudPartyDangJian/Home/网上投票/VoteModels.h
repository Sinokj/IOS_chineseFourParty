//
//  VoteModels.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VoteList,Isvoted,VoteChoices;
@interface VoteModels : NSObject

@property (nonatomic, strong) NSArray<VoteList *> *objects;

@end
@interface VoteList : NSObject

@property (nonatomic, copy) NSString *vcType;

@property (nonatomic, copy) NSString *dtEnd;

@property (nonatomic, strong) Isvoted *isVoted;

@property (nonatomic, copy) NSString *vcTopic;

@property (nonatomic, strong) NSArray<VoteChoices *> *choices;

@property (nonatomic, assign) NSInteger nMaxVote;

@property (nonatomic, assign) NSInteger nId;

@end

@interface Isvoted : NSObject

@property (nonatomic, assign) NSInteger isVoted;

@property (nonatomic, copy) NSString *voteOptions;

@end

@interface VoteChoices : NSObject

@property (nonatomic, assign) NSInteger nOptionId;

@property (nonatomic, assign) NSInteger nCount;

@property (nonatomic, copy) NSString *vcOption;

@property (nonatomic, assign) BOOL isSelect;

@end

