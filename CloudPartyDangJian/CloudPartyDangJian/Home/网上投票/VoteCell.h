//
//  VoteCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VoteCell : UITableViewCell

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *endTime;

@property (nonatomic, assign) NSInteger isVoted;

//评议调用
- (void)setTitle:(NSString *)title andBeginTime:(NSString *)begin andEndTime:(NSString *)end andState:(NSInteger)state;

@end
