//
//  VoteListController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/27.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "VoteListController.h"
#import "VoteCell.h"
#import "VoteController.h"

@interface VoteListController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;
/**
 *  只显示投票之后的arr;
 */
@property (nonatomic, strong) NSMutableArray *retrievalArr;

@end

@implementation VoteListController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"投票列表";
    
    self.retrievalArr = [NSMutableArray array];
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.isFirst) {
        
        [self requestData];
    }
}

- (void)requestData {
    [DataRequest GetWithURL:VoteListURL :^(id reponserObject) {
        
        
        self.voteModels = [VoteModels mj_objectWithKeyValues:reponserObject];
        
        [self.tableV reloadData];
    
    }];

}

- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableV registerClass:[VoteCell class] forCellReuseIdentifier:@"VoteCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isAll) {
        return [self getCount];
    }else {
        
        return self.voteModels.objects.count;
    }
}

- (NSInteger)getCount {
    [self.retrievalArr removeAllObjects];
    for (VoteList *voteList in self.voteModels.objects) {
        if (voteList.isVoted.isVoted == 1) {
            [self.retrievalArr addObject:voteList];
        }
    }
    return self.retrievalArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    VoteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VoteCell" forIndexPath:indexPath];
    
    if (self.isAll) {
        
        VoteList *list = self.retrievalArr[indexPath.row];
        
        cell.title = list.vcTopic;
        
        cell.endTime = list.dtEnd;
        
        cell.isVoted = list.isVoted.isVoted;

    }else {

        cell.title = self.voteModels.objects[indexPath.row].vcTopic;
        
        cell.endTime = self.voteModels.objects[indexPath.row].dtEnd;
        
        cell.isVoted = self.voteModels.objects[indexPath.row].isVoted.isVoted;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
    
    
    VoteController *voteC = [VoteController new];
    
    self.isFirst = NO;

    voteC.voteModel = self.voteModels.objects[indexPath.row];
    
    [self.navigationController pushViewController:voteC animated:YES];
        

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 85;
}


@end
