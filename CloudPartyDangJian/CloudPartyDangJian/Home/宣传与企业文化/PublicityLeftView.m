//
//  PublicityLeftView.m
//  EPartyConstruction
//
//  Created by df on 2017/6/21.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "PublicityLeftView.h"

#define leftWidth 100

#define ScreenWidth ([[UIScreen mainScreen] bounds].size.width)
#define ScreenHeight ([[UIScreen mainScreen] bounds].size.height)

#define mHeight 60
#define cellHeight 50

#define cellColor [UIColor colorWithRed:233/255.0 green:240/255.0 blue:245/255.0 alpha:1]

@interface PublicityLeftView ()<UITableViewDelegate,UITableViewDataSource>

//@property (nonatomic, strong) NSArray *leftArr;

@end

@implementation PublicityLeftView


- (instancetype)initWithFrame:(CGRect)frame {
    
    if (self = [super initWithFrame:frame]) {
        
        [self prepareLayout];
    }
    
    return self;
}

- (void)prepareLayout {
    
    
    self.leftArr = @[@"建筑",
                     @"保障中心",
                     @"物业",
                     @"通信",
                     @"动力",
                     @"幼儿园",
                     @"地产运营部",
                     @"博物馆",
                     @"监理",
                     @"深万源",
                     @"设计",
                     @"绿化",
                     @"大连公司"
                     ];
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height) style:(UITableViewStylePlain)];
    
    [self addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.allowsSelection = NO;
    
    self.tableV.showsVerticalScrollIndicator = NO;
    
    self.tableV.showsHorizontalScrollIndicator = NO;
    
    self.tableV.userInteractionEnabled = NO;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.tableV.bounces = NO;
    
    [self.tableV registerClass:[PublicityLeftCell class] forCellReuseIdentifier:@"PublicityLeftCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _leftArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PublicityLeftCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PublicityLeftCell" forIndexPath:indexPath];
    
    cell.subtypeView.text = self.leftArr[indexPath.row];
    
    if (indexPath.row % 2 == 1) {
        
        cell.backgroundColor = cellColor;
        
    }else {
        
        cell.backgroundColor = [UIColor whiteColor];
    }

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return cellHeight;
}


@end

@implementation PublicityLeftCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        self.subtypeView = [[SubtypeView alloc] initWithFrame:CGRectMake(0, 0, leftWidth - 1, cellHeight - 1)];
        

        [self.contentView addSubview:self.subtypeView];
        
    }
    
    return self;
}

@end
