//
//  PublicityCell.m
//  EPartyConstruction
//
//  Created by df on 2017/6/21.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "PublicityCell.h"

#import "SubtypeView.h"

#define columnNumber 11
#define mWidth 100
#define cellHeight 50

@interface PublicityCell ()

@property (nonatomic, strong) NSMutableArray<SubtypeView *> *labelArr;

@property (nonatomic, strong) NSArray *keyArr;

@end

@implementation PublicityCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self prepareLayout];
    }
    
    return self;
}

- (void)prepareLayout {
    
    self.labelArr = [NSMutableArray arrayWithCapacity:12];
    
    self.keyArr = @[@"wanyuanshiye",
                    @"gongsiguanwei",
                    @"hangtianchangzheng",
                    @"changzhengtai",
                    @"yuanweixin",
                    @"yuanguanwang",
                    @"hangtianbao",
                    @"hangtianshoujibao",
                    @"jituanguanwei",
                    @"jituanguanwang",
                    @"nOther",
                    ];
    
//    self.contentView.backgroundColor = [UIColor blackColor];
    
    for (int i = 0; i < columnNumber; i++) {
        
        SubtypeView *headView=[[SubtypeView alloc]initWithFrame:CGRectMake(i * (mWidth), 0, mWidth, cellHeight - 1)];
        
//         headView.backgroundColor=[UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1];
        
        headView.backgroundColor = [UIColor clearColor];
        
        headView.textKey = self.keyArr[i];
        
        [self.contentView addSubview:headView];
        
        [self.labelArr addObject:headView];
        
        
    }
    
    
}

- (void)setPublicity:(Publicity *)publicity {
    
    _publicity = publicity;
    
    for (SubtypeView *label in self.labelArr) {
                
        label.text = [NSString stringWithFormat:@"%@",[publicity valueForKey:label.textKey]];
    }
}

@end
