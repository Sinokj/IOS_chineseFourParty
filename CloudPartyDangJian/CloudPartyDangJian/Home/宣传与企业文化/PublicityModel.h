//
//  PublicityModel.h
//  EPartyConstruction
//
//  Created by df on 2017/6/22.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Publicity;
@interface PublicityModel : NSObject

@property (nonatomic, assign) NSInteger nRes;

@property (nonatomic, strong) NSArray<Publicity *> *objects;

@end

@interface Publicity : NSObject

@property (nonatomic, copy) NSString *vcPartyGroup;

@property (nonatomic, assign) NSInteger jituanguanwei;
@property (nonatomic, assign) NSInteger hangtianshoujibao;
@property (nonatomic, assign) NSInteger wanyuanshiye;
@property (nonatomic, assign) NSInteger jituanguanwang;
@property (nonatomic, assign) NSInteger gongsiguanwei;
@property (nonatomic, assign) NSInteger hangtianchangzheng;
@property (nonatomic, assign) NSInteger yuanweixin;
@property (nonatomic, assign) NSInteger changzhengtai;
@property (nonatomic, assign) NSInteger nOther;
@property (nonatomic, assign) NSInteger yuanguanwang;
@property (nonatomic, assign) NSInteger nId;
@property (nonatomic, assign) NSInteger hangtianbao;


@end
