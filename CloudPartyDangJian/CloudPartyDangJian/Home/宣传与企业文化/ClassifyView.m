//
//  ClassifyView.m
//  EPartyConstruction
//
//  Created by df on 2017/6/21.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "ClassifyView.h"


#define mWidth 80
#define mHeight 60

#define leftWidth 100


@implementation ClassifyView


- (instancetype)initWithFrame:(CGRect)frame {
    
    if(self = [super initWithFrame:frame]) {
        
        [self drawLineWitnStartPoint:CGPointMake(leftWidth/2, 0) endPoint:CGPointMake(leftWidth,mHeight)];
        
        [self drawLineWitnStartPoint:CGPointMake(0, mHeight/2) endPoint:CGPointMake(leftWidth,mHeight)];
        
        [self drawLineWitnStartPoint:CGPointMake(leftWidth, 0) endPoint:CGPointMake(leftWidth,mHeight)];

        
        [self addLabel:@"媒体" andFrame:CGRectMake(leftWidth/2+20,  5, leftWidth/2-10, 15)];
        
        [self addLabel:@"单位" andFrame:CGRectMake(5,  mHeight/2+10, leftWidth/2, 15)];
        
        [self addLabel:@"上稿量" andFrame:CGRectMake(20,  mHeight/4, leftWidth/2, 15)];
    }
    
    return self;
}

- (void)addLabel:(NSString *)text andFrame:(CGRect)frame {
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:frame];
    
    label1.text = text;
    
    label1.textColor = [UIColor whiteColor];
    
    label1.font = [UIFont systemFontOfSize:14];
    
    [self addSubview:label1];
    
}

- (void)drawLineWitnStartPoint:(CGPoint)start endPoint:(CGPoint)end {
    
    CAShapeLayer *solidShapeLayer = [CAShapeLayer layer];
    
    CGMutablePathRef solidShapePath =  CGPathCreateMutable();
    
    [solidShapeLayer setFillColor:[[UIColor clearColor] CGColor]];
    
    [solidShapeLayer setStrokeColor:[[UIColor whiteColor] CGColor]];
    
    solidShapeLayer.lineWidth = 0.5f ;
    
    CGPathMoveToPoint(solidShapePath, NULL, start.x, start.y);
    
    CGPathAddLineToPoint(solidShapePath, NULL, end.x,end.y);
    
    [solidShapeLayer setPath:solidShapePath];
    
    CGPathRelease(solidShapePath);
    
    [self.layer addSublayer:solidShapeLayer];
}

@end
