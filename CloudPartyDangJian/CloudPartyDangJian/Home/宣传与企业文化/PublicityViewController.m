//
//  PublicityViewController.m
//  EPartyConstruction
//
//  Created by df on 2017/6/20.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "PublicityViewController.h"

#import "SubtypeView.h"

#import "ClassifyView.h"

#import "PublicityCell.h"

#import "PublicityLeftView.h"

#import "PublicityModel.h"


#define mWidth 100
#define mHeight 60

#define cellHeight 50

#define leftWidth 100

#define columnNumber 11

#define cellColor [UIColor colorWithRed:233/255.0 green:240/255.0 blue:245/255.0 alpha:1]
#define headColor [UIColor colorWithRed:66/255.0 green:173/255.0 blue:234/255.0 alpha:1]


@interface PublicityViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *rightTableV;

@property (nonatomic, strong) PublicityLeftView *leftView;

@property (nonatomic, strong) UIView *headView;

@property (nonatomic, strong) NSArray *topArr;

@property (nonatomic, strong) PublicityModel *publicityM;

@end

@implementation PublicityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    self.title = @"上稿量统计汇总表";
    
    self.topArr = [NSArray arrayWithObjects:@"万源视野",
                   @"公司官微",
                   @"航天长征",
                   @"长征台",
                   @"院微信公众号",
                   @"院官网",
                   @"中国航天报",
                   @"中国航天手机报",
                   @"集团官微",
                   @"集团官网",
                   @"其他媒体",
                   nil];
    
    
    [self prepareLayout];
    
    [self requestData];
}

- (void)requestData {
    
    [DataRequest PostWithURL:GetArticlePublishStatics parameter:nil :^(id reponserObject) {
        
        self.publicityM = [PublicityModel mj_objectWithKeyValues:reponserObject];
        
        [self.rightTableV reloadData];
        
    }];
}

- (CAGradientLayer *)drawLinearGradient:(UIView *)view{
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.colors = [NSArray arrayWithObjects:
                       (id)[UIColor colorWithRed:106/255.0 green:169/255.0 blue:231/255.0 alpha:1.0].CGColor,
                       (id)[UIColor colorWithRed:65/255.0 green:145/255.0 blue:218/255.0 alpha:1.0].CGColor,
                       nil];

    return gradient;
}

- (void)prepareLayout {
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    
//    左上角
    ClassifyView *classifyView = [[ClassifyView alloc] initWithFrame:CGRectMake(0, 0, leftWidth, mHeight)];
    
//    classifyView.backgroundColor = [UIColor whiteColor];
    
    [classifyView.layer insertSublayer:[self drawLinearGradient:classifyView] atIndex:0];
    
    [self.view addSubview:classifyView];
    
//    头部
    UIView *Headview = [[UIView alloc] initWithFrame:CGRectMake(0, 0, mWidth * columnNumber , mHeight)];
    
    self.headView = Headview;
    
    [Headview.layer addSublayer:[self drawLinearGradient:Headview]];
    
    
    self.headView.backgroundColor = [UIColor whiteColor];
    
    for(int i = 0; i < columnNumber; i++){
        
        SubtypeView *headView = [[SubtypeView alloc]initWithFrame:CGRectMake(i * (mWidth), 0, mWidth, mHeight)];
        
//        headView.backgroundColor = [UIColor whiteColor];
        
        headView.text = self.topArr[i];
        
        headView.textColor = [UIColor whiteColor];
        
//        headView.textLabel.text = self.topArr[i];
        
        [self.headView addSubview:headView];
    }

//   左部
    self.leftView = [[PublicityLeftView alloc] initWithFrame:CGRectMake(0, mHeight, leftWidth, ScreenHeight - 64 - mHeight)];
    
    [self.view addSubview:self.leftView];
    
//   右部
    self.rightTableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, mWidth * columnNumber, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    self.rightTableV.delegate = self;
    
    self.rightTableV.dataSource = self;
    
    self.rightTableV.allowsSelection = NO;
    
    self.rightTableV.bounces = NO;

    self.rightTableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.rightTableV registerClass:[PublicityCell class] forCellReuseIdentifier:@"PublicityCell"];
    
    
    UIScrollView *myScrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(leftWidth, 0, ScreenWidth - leftWidth, ScreenHeight)];
    
    [myScrollView addSubview: self.rightTableV];
    
    myScrollView.bounces=NO;
    
    myScrollView.contentSize=CGSizeMake(mWidth * columnNumber, 0);
    
    [self.view addSubview:myScrollView];

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 13;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return cellHeight;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    return self.headView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return mHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PublicityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PublicityCell" forIndexPath:indexPath];

    if (indexPath.row % 2 == 1) {
        
        cell.backgroundColor = cellColor;
        
    }else {
        
        cell.backgroundColor = [UIColor whiteColor];
    }
    
    if ([self.leftView.leftArr[indexPath.row] isEqualToString:self.publicityM.objects[indexPath.row].vcPartyGroup]) {
        
        cell.publicity = self.publicityM.objects[indexPath.row];
        
    }else {
        
        if (self.publicityM.objects.count > 0) {
            
            for (Publicity *pub in self.publicityM.objects) {
                
                if ([pub.vcPartyGroup isEqualToString:self.leftView.leftArr[indexPath.row]]) {
                    
                    cell.publicity = pub;
                    
                }
            }
        }
        
        
    }
    
    return cell;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    CGFloat offsetY = self.rightTableV.contentOffset.y;
    
    CGPoint timeOffsetY = self.leftView.tableV.contentOffset;
    
    timeOffsetY.y = offsetY;
    
    self.leftView.tableV.contentOffset = timeOffsetY;
    
    if(offsetY == 0) {
        
        self.leftView.tableV.contentOffset=CGPointZero;
    }
}

@end
