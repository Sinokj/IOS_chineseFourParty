//
//  SuccessViewController.h
//  EPartyConstruction
//
//  Created by df on 2017/5/10.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "ViewController.h"

@interface SuccessViewController : ViewController

@property (nonatomic, strong) NSString *money;

@end
