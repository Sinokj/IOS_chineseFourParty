//
//  HomeHead.h
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/9/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDCycleScrollView.h>

@interface HomeHead : UICollectionReusableView<SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *cycleScrollView;

@end
