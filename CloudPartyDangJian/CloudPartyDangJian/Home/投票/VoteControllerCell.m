//
//  VoteControllerCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "VoteControllerCell.h"
#import "DyfTool.h"

@interface VoteControllerCell ()

@property (nonatomic, strong) UIButton *choseBtn;

@property (nonatomic, strong) UILabel *contentL;

@end
@implementation VoteControllerCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    
    self.choseBtn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    
    self.choseBtn.frame = CGRectMake(20, 10, 20, 20);
    
    [self.choseBtn setImage:[UIImage imageNamed:@"unselect"] forState:(UIControlStateNormal)];
    [self.choseBtn setImage:[UIImage imageNamed:@"select"] forState:(UIControlStateSelected)];
    
    //    self.choseBtn.enabled = NO;
    
    [self.contentView addSubview:self.choseBtn];
    
    self.contentL = [UILabel customLablWithFrame:CGRectMake(self.choseBtn.right, 5, ScreenWidth - 60, 30) andTitle:@"" andFontNumber:15];
    
    [self.contentView addSubview:self.contentL];
    
    
}

- (void)setVoteChoices:(VoteChoices *)voteChoices {
    if (_voteChoices != voteChoices) {
        _voteChoices = nil;
        _voteChoices = voteChoices;
        
        self.contentL.text = voteChoices.vcOption;
    }
    self.choseBtn.selected = voteChoices.isSelect;

}

- (void)selectBtnClickWithRow:(NSInteger)row andModel:(VoteList *)voteList {
    
    if ([voteList.vcType isEqualToString:@"单选"]) {
        for (VoteChoices *choice in voteList.choices) {
            choice.isSelect = NO;
        }
        voteList.choices[row].isSelect = !voteList.choices[row].isSelect;
    }else {
        NSInteger n = 0;
        for (VoteChoices *choice in voteList.choices) {
            if (choice.isSelect) {
                n++;
            }
        }
        if (!voteList.choices[row].isSelect) {
            n++;
        }
        if (n <= voteList.nMaxVote) {
            voteList.choices[row].isSelect = !voteList.choices[row].isSelect;
        }
    }

}


@end
