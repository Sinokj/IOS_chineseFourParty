//
//  VoteControllerCell.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VoteModels.h"

@interface VoteControllerCell : UITableViewCell

@property (nonatomic, strong) VoteChoices *voteChoices;

- (void)selectBtnClickWithRow:(NSInteger)row andModel:(VoteList *)voteList;
@end
