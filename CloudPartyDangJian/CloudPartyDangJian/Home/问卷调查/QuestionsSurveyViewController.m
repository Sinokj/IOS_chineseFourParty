//
//  QuestionsSurveyViewController.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/5.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "QuestionsSurveyViewController.h"
#import "QuestionSurveyTableViewCell.h"
#import "QuestionSurveyWebViewController.h"
#import "DataRequest.h"
#import "HeaderUrl.h"
#import "WebViewController.h"
@interface QuestionsSurveyViewController ()<UITableViewDelegate,UITableViewDataSource,WebViewDelegate>
@property(nonatomic,strong)UITableView *tableView;
@property (nonatomic, strong) NSIndexPath *indexp;
@end

@implementation QuestionsSurveyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareLayout];
    [self reloadData];
    
}
-(void)prepareLayout{
    self.tableView = [[UITableView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.title =@"问卷调查";
    // 设置预估行高
    self.tableView.estimatedRowHeight = 80;
    // 自动计算行高
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    // 清空分割线的内边距
    self.tableView.separatorInset = UIEdgeInsetsZero;
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    [self.tableView registerClass:[QuestionSurveyTableViewCell class] forCellReuseIdentifier:@"QuestionSurveyID"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.questionsurveyModel.objects.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    QuestionSurveyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"QuestionSurveyID" forIndexPath:indexPath];
    cell.qustionsurvey = self.questionsurveyModel.objects[indexPath.row];
   // self.WebQuestionsurvey = cell.qustionsurvey;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//        QuestionSurveyWebViewController *questionwebVC = [[QuestionSurveyWebViewController alloc] init];
//        questionwebVC.webNld = self.WebQuestionsurvey.nId;
//        DLog(@"%ld",(long)questionwebVC.webNld);
//        [self.navigationController pushViewController:questionwebVC animated:YES];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];

    WebViewController *questionwebVC = [WebViewController new];
    QuestionSurvey *survey = self.questionsurveyModel.objects[indexPath.row];
    questionwebVC.type = @"1";
    //questionwebVC.str = @"http://123.57.84.199:8011/policePartyApp/getTopics_Content.do?nId=8";
    questionwebVC.str = [NSString stringWithFormat:@"%@/%@?nId=%ld",HostUrl,questionSurveyWeb,survey.nId];
    questionwebVC.articleId = survey.nId;
    questionwebVC.delegate = self;
    questionwebVC.tit = self.title;
    self.indexp = indexPath;
    [self.navigationController pushViewController:questionwebVC animated:YES];
}
-(void)getNewState:(NSInteger)state{
//    self.sourceArr[self.indexp.row].nStatus = state;
    
    [self.tableView reloadRowsAtIndexPaths:@[self.indexp] withRowAnimation:(UITableViewRowAnimationNone)];
}
-(void)reloadData{
    [DataRequest GetWithURL:questionSurvey :^(id reponserObject) {
        self.questionsurveyModel = [QuestionSurveyModel mj_objectWithKeyValues:reponserObject];
        [self.tableView reloadData];
    }];
}
@end
