//
//  QuestionSurveyWebViewController.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/5.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "QuestionSurveyWebViewController.h"
#import "DataRequest.h"
#import "HeaderUrl.h"
@interface QuestionSurveyWebViewController ()<UIWebViewDelegate>
@property(nonatomic,weak)UIWebView *questionWeb;
@end

@implementation QuestionSurveyWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"问卷调查";
    [self preparelayout];
}
-(void)preparelayout{
    UIWebView *webView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.questionWeb = webView;
    [self.view addSubview:webView];
    self.questionWeb.delegate = self;

//    NSURL *url = [NSURL URLWithString:@"http://123.57.84.199:8011/policePartyApp/getTopics_Content.do?nId="];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://123.57.84.199:8011/policePartyApp/getTopics_Content.do?nId=%ld",(long)self.webNld]];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.questionWeb loadRequest:request];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 - (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType;
 - (void)webViewDidStartLoad:(UIWebView *)webView;
 - (void)webViewDidFinishLoad:(UIWebView *)webView;
 - (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error;
*/

@end
