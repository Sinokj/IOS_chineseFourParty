//
//  QuestionSurveyModel.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/5.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
@class QuestionSurvey;
@interface QuestionSurveyModel : NSObject
@property(nonatomic,strong)NSArray<QuestionSurvey *> *objects;
@end

@interface QuestionSurvey : NSObject
@property(nonatomic,copy)NSString *vcTitle;
@property(nonatomic,assign)NSInteger nId;
@end
