//
//  DemocracyModel.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/4.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Democracy;
@interface DemocracyModel : NSObject

@property (nonatomic, strong) NSArray<Democracy *> *objects;

@end

@interface Democracy : NSObject

@property (nonatomic, copy) NSString *dtBegin;
@property (nonatomic, copy) NSString *dtReg;

@property (nonatomic, copy) NSString *vcTitle;

@end
