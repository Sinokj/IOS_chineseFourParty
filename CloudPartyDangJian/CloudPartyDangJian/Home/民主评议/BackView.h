//
//  BackView.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/7.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "thoughtModel.h"
typedef void (^CallBackBlcok) (NSString *text);
typedef void(^reloadDataBlock) ();
@class BackView;
@interface BackView : UIView
@property(nonatomic,strong)thought *thoughtM;
@property(nonatomic,strong)GreadPerson *greadpersonModel;
//@property(nonatomic,weak)id<BackViewDelegate> delegate;
@property(nonatomic,copy)CallBackBlcok callBackBlock;
@property(nonatomic,copy)reloadDataBlock reloaddataBlock;
- (void)returnText:(CallBackBlcok)block;
-(void)returnScore:(reloadDataBlock)block;
@end
