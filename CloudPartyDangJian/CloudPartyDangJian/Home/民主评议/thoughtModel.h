//
//  thoughtModel.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/4.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
@class thought,VOchoices,GreadPerson;

@interface thoughtModel : NSObject
@property (nonatomic, strong) NSArray<thought*> *objects;
@property(nonatomic,copy)NSString *writeText;
@end

@interface thought : NSObject
@property (nonatomic, strong) NSArray< VOchoices*> *choices;
@property(nonatomic,strong) NSArray<GreadPerson *>
*beGreadPerson;

@end
@interface VOchoices: NSObject
@property(nonatomic,copy)NSString *vcOption;
@property(nonatomic,assign)NSInteger nScore;
@end

@interface GreadPerson : NSObject
//@property(nonatomic,copy)NSString *beGreadPerson;
@property(nonatomic,copy)NSString *BEGreadPerson;

@end
