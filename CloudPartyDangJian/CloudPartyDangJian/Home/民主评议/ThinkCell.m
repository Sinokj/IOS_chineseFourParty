//
//  ThinkCell.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/4.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "ThinkCell.h"
#import <Masonry.h>
#define Margin 8
#define TMargin 3
#define BMargin 3
#define NLMargin 6

@interface ThinkCell ()<UITextFieldDelegate>
@property(nonatomic,weak)UILabel *thinkLabel;
@property(nonatomic,weak)UILabel *writeLabel;
@property(nonatomic,weak)UILabel *scoreLabel;


@end
@implementation ThinkCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // 添加子控件
        [self setupUI];
    }
    return self;
}

- (void)setupUI{
    UILabel *numberLabel = [[UILabel alloc] init];
      numberLabel.text = @"1";
    numberLabel.font = [UIFont systemFontOfSize:15];
    [self.contentView addSubview:numberLabel];
    [numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(Margin);
        make.top.offset(Margin+2);
    //make.height.offset(7);
//        make.height.offset(8);
        make.width.offset(Margin);
    }];

    self.numberLabel = numberLabel;
            UILabel *thinkLabel = [[UILabel alloc] init];
    thinkLabel.font = [UIFont systemFontOfSize:16];
        thinkLabel.text = @"党党党党党党党党党党党党党党党党党党党党党党党党";
        thinkLabel.numberOfLines = 0;
        [self.contentView addSubview:thinkLabel];
        [thinkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            //make.centerY.equalTo(numberLabel.mas_centerY);
make.top.equalTo(numberLabel.mas_top);
make.left.equalTo(numberLabel.mas_right).offset(Margin);
make.right.offset(-Margin);
        }];
    //[self.thinkLabel sizeToFit];
    self.thinkLabel = thinkLabel;
        UITextField *writeText = [[UITextField alloc] init];
        writeText.borderStyle = UITextBorderStyleLine;
//        writeText.backgroundColor = [UIColor blueColor];
        writeText.font = [UIFont fontWithName:@"Arial" size:17.0f];
        writeText.textColor = [UIColor redColor];
        writeText.clearsOnBeginEditing = YES;
        writeText.keyboardType = UIKeyboardTypeNumberPad;
    writeText.delegate = self;
        [self.contentView addSubview:writeText];
        [writeText mas_makeConstraints:^(MASConstraintMaker *make) {
//            make.top.equalTo(thinkLabel.mas_bottom).offset(5);
           // make.left.offset(10);
            make.left.equalTo(thinkLabel.mas_left); make.top.equalTo(thinkLabel.mas_bottom).offset(Margin);
            make.width.offset(50);
            make.height.offset(30);
        }];
    self.writeTextField = writeText;
    UILabel *scoreLabel = [[UILabel alloc] init];
        scoreLabel.text = @"分";
        [self.contentView addSubview:scoreLabel];
        [scoreLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(writeText);
            make.left.equalTo(writeText.mas_right).offset(Margin);
            //make.width.height.offset(6);
        }];
        self.scoreLabel =scoreLabel;
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            // 让contentView距离父控件"cell"四边0间距
make.edges.equalTo(self).mas_offset(UIEdgeInsetsZero);
make.bottom.equalTo(writeText.mas_bottom).offset(Margin);
        }];
}

-(void)setVochoices:(VOchoices *)vochoices{
    if (_vochoices != vochoices) {
        _vochoices = nil;
        _vochoices = vochoices;
        //self.thinkLabel.text = vochoices.vcOption;
        self.thinkLabel.text = [NSString stringWithFormat:@"%@  (%ld分)",vochoices.vcOption,(long)vochoices.nScore];
            }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self endEditing:YES];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    self.textbackBlock(textField.text);
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSMutableString *newtxt = [NSMutableString stringWithString:textField.text];
            [newtxt replaceCharactersInRange:range withString:string];

            if (range.location == 0 && string.length == 0) {
            self.textbackBlock(nil);
                return YES;
        }else if(range.location == 0 && string.length > 0){
            self.textbackBlock(string);
            return YES;
        }else if (newtxt.length > 2||[newtxt intValue]>10){
            return NO;
        }
        else {
            self.textbackBlock(textField.text);
        }
        return YES;
}
- (void)setContentText:(NSString *)contentText{
    _contentText = contentText;
    self.writeTextField.text = contentText;
}

@end
