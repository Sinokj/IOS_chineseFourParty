//
//  DemocracySuggestViewController.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/1.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "DemocracySuggestViewController.h"
#import "DemocracySuggestTableViewCell.h"

#import "DataRequest.h"
#import "HeaderUrl.h"
#import <MJExtension.h>
#import "SuggestViewController.h"
@interface DemocracySuggestViewController ()

@end

@implementation DemocracySuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerNib:[UINib nibWithNibName:@"DemocracySuggestTableViewCell" bundle:nil] forCellReuseIdentifier:@"DemocracySuggestCellID"];
    [self requestData];
    self.title = @"民主评议";
    self.tableView.separatorInset = UIEdgeInsetsZero;
    // 设置预估行高
    self.tableView.estimatedRowHeight = 300;
    // 自动计算行高
    //self.tableView.rowHeight = UITableViewAutomaticDimension;
//    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:nil action:nil];
//    self.navigationItem.backBarButtonItem = item;
    //导航
//self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStyleDone target:nil action:nil];
}
//[NSString stringWithFormat:@"%@/%@",HostUrl,democryacySuggest]
- (void)requestData {
    [DataRequest GetWithURL:democryacySuggest :^(id reponserObject) {

self.democracyModelM = [DemocracyModel mj_objectWithKeyValues:reponserObject];
        
        [self.tableView reloadData];
    }];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return self.democracyModelM.objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DemocracySuggestTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DemocracySuggestCellID" forIndexPath:indexPath];
    cell.democracy = self.democracyModelM.objects[indexPath.row];
    return cell;
    
    }
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 117;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        SuggestViewController *suggestVC = [[SuggestViewController alloc] init];
        [self.navigationController pushViewController:suggestVC animated:YES];
    }
}
@end
