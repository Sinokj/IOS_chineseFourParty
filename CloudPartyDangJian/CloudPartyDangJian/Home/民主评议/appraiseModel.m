//
//  appraiseModel.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/12.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "appraiseModel.h"

@implementation appraiseModel
- (instancetype)initWithDict:(NSDictionary *)dict
{
    self = [super init];
    if (self) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)momentWithDict:(NSDictionary *)dict {
    
    
    return [[self alloc] initWithDict:dict];
}
@end
