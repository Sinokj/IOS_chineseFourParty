//
//  SuggestViewController.h
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/4.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "thoughtModel.h"
typedef void (^BtnClickBackBlcok) (NSInteger writetext,NSString *titleButtonTitle);
@interface SuggestViewController : ViewController
@property(nonatomic,strong)thoughtModel *thoughtModelArrM;
@property(nonatomic,strong)thought *thoughtM;
@property(nonatomic,copy)BtnClickBackBlcok BtnClickblock;
@property(nonatomic,copy)NSString *writeString;
//@property(nonatomic,strong)VOchoices *vochoices;
- (void)returnText:(BtnClickBackBlcok)block;
@end
