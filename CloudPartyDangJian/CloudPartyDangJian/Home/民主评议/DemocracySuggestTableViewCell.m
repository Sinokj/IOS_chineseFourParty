//
//  DemocracySuggestTableViewCell.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/4.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "DemocracySuggestTableViewCell.h"
@interface DemocracySuggestTableViewCell()
@property (weak, nonatomic) IBOutlet UIImageView *democracyImageView;
 
@property (weak, nonatomic) IBOutlet UILabel *startTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *zhiLabel;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *democracySuggestLabel;

@end
@implementation DemocracySuggestTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        self.democracyImageView.image = [UIImage imageNamed:@"ic_doc"];
    }
    return self;
}
-(void)setDemocracy:(Democracy *)democracy{
    if (_democracy != democracy) {
        _democracy = nil;
        _democracy = democracy;
        self.startTimeLabel.text = democracy.dtBegin;
        self.endTimeLabel.text = democracy.dtReg;
        self.democracySuggestLabel.text = democracy.vcTitle;
        self.democracyImageView.image = [UIImage imageNamed:@"ic_doc"];
        //self.zhiLabel.text = @"至";
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
