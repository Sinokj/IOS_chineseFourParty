//
//  SuggestsM.h
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/10/8.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SuggM;
@interface SuggestsM : NSObject

@property (nonatomic, strong) NSArray<SuggM *> *objects;


@end

@interface SuggM : NSObject

@property (nonatomic, copy) NSString *dtBegin;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *vcRegister;

@property (nonatomic, copy) NSString *vcTitle;

@property (nonatomic, copy) NSString *dtEnd;

@property (nonatomic, copy) NSString *dtReg;

@property (nonatomic, assign) NSInteger nJoined;

@end

