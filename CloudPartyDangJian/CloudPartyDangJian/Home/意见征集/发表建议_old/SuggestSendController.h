//
//  SuggestSendController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/26.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "SuggestModel.h"

@interface SuggestSendController : ViewController

@property (nonatomic, strong) Suggest *suggest;

@end
