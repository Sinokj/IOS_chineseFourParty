//
//  SuggestCell.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/3.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "SuggestCell.h"
#import "DyfTool.h"

@interface SuggestCell ()

//@property (nonatomic, strong) UILabel *titleLabel;
//@property (nonatomic, strong) UILabel *detailLabel;
//@property (nonatomic, strong) UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLab;
@property (weak, nonatomic) IBOutlet UILabel *startTimeLab;
@property (weak, nonatomic) IBOutlet UILabel *endTimeLab;
@property (weak, nonatomic) IBOutlet UIButton *statusBtn;

@end

@implementation SuggestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




//- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
//    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        [self prepareLayout];
//    }
//    return self;
//}

//- (void)prepareLayout {
//
//    self.contentView.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
//
//    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, ScreenWidth - 6, 80)];
//
//    [self.contentView addSubview:bgView];
//
//    bgView.backgroundColor = [UIColor whiteColor];
//
//    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
//
//    imageV.image = [UIImage imageNamed:@"ic_doc"];
//
//    [bgView addSubview:imageV];
//
//    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(imageV.right + 10, 5, bgView.width - 60, 20) andTitle:@"测试" andFontNumber:15];
//
//    [bgView addSubview:self.titleLabel];
//
//    self.timeLabel = [UILabel customLablWithFrame:CGRectMake(10, imageV.bottom + 10, self.titleLabel.width, 20) andTitle:@"到期时间:2016-11-11 22:11" andFontNumber:14];
//
//    [bgView addSubview:self.timeLabel];
//
//    self.detailLabel = [UILabel customLablWithFrame:CGRectMake(self.titleLabel.left, self.titleLabel.bottom, self.titleLabel.width, 20) andTitle:@"什么打扫没打算模式摸摸发哦摸吗佛莫萨摩佛山吗" andFontNumber:13];
//
//    self.detailLabel.numberOfLines = 0;
//
//    [bgView addSubview:self.detailLabel];
//
//
//}

- (void)setSuggest:(Suggest *)suggest {
    if (_suggest != suggest) {
        _suggest = nil;
        _suggest = suggest;
        
//        self.titleLabel.text = suggest.vcTopic;
//
//        self.timeLabel.text = [NSString stringWithFormat:@"到期时间 : %@",suggest.dtEnd];
//
//        [self setAttributedStr:[NSString stringWithFormat:@"已征集到%ld条建议",suggest.nCount] andFrome:4 andLength:[[NSString stringWithFormat:@"%ld",suggest.nCount] length] withLabel:self.detailLabel];
        
        self.titleLab.text = suggest.vcTopic;
        
        self.startTimeLab.text = [suggest.dtBegin componentsSeparatedByString:@" "][0];
        self.endTimeLab.text = [suggest.dtEnd componentsSeparatedByString:@" "][0];
        
        NSString *status = suggest.vcstatus;
        if ([status isEqualToString:@"进行中"]) {
            [self.statusBtn setImage:ImageName(@"进行中") forState:UIControlStateNormal];
        }
        else if ([status isEqualToString:@"已结束"]) {
            [self.statusBtn setImage:ImageName(@"已关闭") forState:UIControlStateNormal];
        }
        else {
            [self.statusBtn setImage:ImageName(@"已提交") forState:UIControlStateNormal];
        }
        
        
        
    }
}

- (void)setAttributedStr:(NSString *)str andFrome:(NSInteger)start andLength:(NSInteger)length withLabel:(UILabel *)label {
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSDictionary *dic = @{ NSForegroundColorAttributeName:COLOR_RGB(0xffd700)};
    
    [attributedString addAttributes:dic range:NSMakeRange(start, length)];
    
    label.attributedText = attributedString;
    
}










@end
