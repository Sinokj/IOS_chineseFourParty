//
//  SuggestShowController.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/3.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestModel.h"
#import "ViewController.h"
@interface SuggestShowController : ViewController

//@property (nonatomic, strong) NSString *suggestTitle;
//@property (nonatomic, strong) NSString *suggestDetail;
@property (nonatomic, strong) Suggest *suggest;


@end
