//
//  PublishView.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/4.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "PublishView.h"

@implementation PublishView

- (void)setupThisView {
    
    self.textView.layer.cornerRadius = 15.0;
    self.submitBtn.layer.cornerRadius = self.submitBtn.bounds.size.height/2;
    self.submitBtn.layer.borderColor = white.CGColor;
    self.submitBtn.layer.borderWidth = 1.0;
    
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.textView resignFirstResponder];
}

@end
