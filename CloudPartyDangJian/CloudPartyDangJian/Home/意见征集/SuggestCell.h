//
//  SuggestCell.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/3.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SuggestModel.h"
@interface SuggestCell : UITableViewCell
@property (nonatomic, strong) Suggest *suggest;
@end
