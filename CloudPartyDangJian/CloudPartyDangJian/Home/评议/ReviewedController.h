//
//  ReviewedController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "ReviewedListModel.h"
#import "ReviewedCell.h"
@interface ReviewedController : ViewController

@property (nonatomic, strong) ReviewedModel *reviewedModel;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, strong)ReviewedCell *cell;
@end
