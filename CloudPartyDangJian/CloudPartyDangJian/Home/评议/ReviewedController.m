//
//  ReviewedController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/28.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ReviewedController.h"
//#import "ReviewedCell.h"
#import <Masonry.h>

@interface ReviewedController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;
/**
 *  要对beGreadPerson评议
 */
@property (nonatomic, strong) UILabel *beGreadPerson;

//@property (nonatomic, assign) NSInteger index;

@property (nonatomic, strong) UIButton *sendBtn;
/**
 *  保存评分
 */
@property (nonatomic, strong) NSMutableDictionary *soreDic;
@property(nonatomic,strong)NSArray *soreArr;

@end

@implementation ReviewedController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.soreDic = [NSMutableDictionary dictionary];
    
//    self.title = @"评议";
    
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
    
}

- (void)prepareLayout {
    
//    UILabel *titleL = [UILabel customLablWithFrame:CGRectMake(10, 20, 130, 40) andTitle:@"您要评议的人员是 : " andFontNumber:14];
//
//    [self.view addSubview:titleL];
    
    self.beGreadPerson = [UILabel customLablWithFrame:CGRectMake(0, 0, 200, 20) andTitle:@"选择评议人员" andFontNumber:17];
    
//    [self.beGreadPerson sizeToFit];
    
    self.navigationItem.titleView = self.beGreadPerson;
    
    [self.beGreadPerson setTextColor:[UIColor whiteColor]];

    
    self.beGreadPerson.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *tap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(alert)];
    
    [self.beGreadPerson addGestureRecognizer:tap];
    
    self.beGreadPerson.textAlignment = NSTextAlignmentCenter;
//    [self.view addSubview:self.beGreadPerson];

    
    self.tableV = [[UITableView alloc] initWithFrame:self.view.bounds];
    
    [self.view addSubview:self.tableV];
    
    [self.tableV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.allowsSelection = NO;
    
    self.tableV.estimatedRowHeight = 80;
    
//    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableV registerClass:[ReviewedCell class] forCellReuseIdentifier:@"ReviewedCell"];
    
    
    
    UIButton *sendBtn = [UIButton buttonWithType:(UIButtonTypeSystem)];
    
    self.sendBtn = sendBtn;
    
//    [self.view addSubview:sendBtn];
    
    UIView *bgView = [UIView new];
    
    bgView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    bgView.frame = CGRectMake(0, 0, ScreenWidth, 80);
    
    [bgView addSubview:sendBtn];
    
    
    sendBtn.frame = CGRectMake(15, 12, ScreenWidth - 24, 49);
    
    [sendBtn setTitle:@"提  交" forState:(UIControlStateNormal)];
    
    [sendBtn setTintColor:[UIColor whiteColor]];
    
    [sendBtn setBackgroundColor: [UIColor colorWithRed:0.9463 green:0.1012 blue:0.1432 alpha:1.0]];
    
    sendBtn.layer.cornerRadius = 3;
    
    [sendBtn addTarget:self action:@selector(sendBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    sendBtn.hidden = false;
    
    self.tableV.tableFooterView = bgView;
    
    [self alert];

}
-(void)getLineWidthWithLabel:(UILabel*)label font:(CGFloat)f {
    
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:label.text];
    
    NSRange strRange = {0,[attri length]};
   
    [attri addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];

    [attri addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:strRange];
    [label setAttributedText:attri];
    
}

- (void)alert {
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"提示" message:@"您要对谁进行评议?" preferredStyle:(UIAlertControllerStyleAlert)];
    for (int i = 0; i < self.reviewedModel.beGreadPerson.count; i++) {
        
        NSString *str = self.reviewedModel.beGreadPerson[i].nStatus == 1 ? [NSString stringWithFormat:@"%@%@",self.reviewedModel.beGreadPerson[i].beGreadPerson,@"  (已评)"] : [NSString stringWithFormat:@"%@",self.reviewedModel.beGreadPerson[i].beGreadPerson];
        
        UIAlertAction *action = [UIAlertAction actionWithTitle:str style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
            if (self.reviewedModel.nStatus == 1) {
                self.sendBtn.hidden = YES;
            }else if ([str hasSuffix:@"(已评)"]) {
                self.sendBtn.hidden = YES;
            }else {
                self.sendBtn.hidden = NO;
            }
            self.index = i;
            self.beGreadPerson.text = self.reviewedModel.beGreadPerson[i].beGreadPerson;
//            [self getLineWidthWithLabel:self.beGreadPerson font:17];
            [self.tableV reloadData];
        }];
        
        [alertC addAction:action];
    }
    [self presentViewController:alertC animated:YES completion:nil];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.reviewedModel.choices.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReviewedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReviewedCell" forIndexPath:indexPath];
    
    cell.choice = self.reviewedModel.choices[indexPath.row];
    cell.CellIndex = self.index;
    cell.CellreviewedModel = self.reviewedModel;
    //cell.optionScorearr = self.reviewedModel.beGreadPerson[self.index].optionScore;
    cell.numberLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row + 1];
    [cell.numberLabel sizeToFit];
    //已评
    if(self.reviewedModel.beGreadPerson[self.index].nStatus == 1) {
#warning
        for (int i = 0; i < self.reviewedModel.beGreadPerson[self.index].optionScore.count; i++) {
            if (self.reviewedModel.beGreadPerson[self.index].optionScore[i].nOptionId == cell.choice.nId) {
               cell.scoreTF.text = [NSString stringWithFormat:@"%zd",self.reviewedModel.beGreadPerson[self.index].optionScore[i].nScore];
                //cell.scoreSTring = [NSString stringWithFormat:@"%zd",self.reviewedModel.beGreadPerson[self.index].optionScore[i].nScore];
                cell.scoreTF.enabled = NO;
            }
        }
////        if (cell.choice.nId) {
//        //            self.scoreTF.text = [NSString stringWithFormat:@"%ld",(long)optionScore.nScore];
//
//        cell.scoreTF.enabled = NO;
//
////        }
        
                //cell.scoreTF.text = [NSString stringWithFormat:@"%@",self.soreDic[@(cell.choice.nId)]];
        
        
        //[cell setOptionScore:self.reviewedModel.beGreadPerson[self.index].optionScore[indexPath.row] andDic:self.soreDic];
        //[cell setOptionScore:[self.soreDic objectForKey:@(cell.choice.nId)] andDic:self.soreDic];
        //[cell setOptionScore:self.soreArr[indexPath.row] andDic:self.soreDic];
        
        //        }
    }else {
        
        //        cell.optionScore = nil;
        [cell setOptionScore:nil andDic:self.soreDic];
        
    }
    
    __block ReviewedController *strongSelf = self;
    cell.ReviewedBlock = ^(NSInteger nId , NSString *score) {
        [strongSelf.soreDic setObject:score forKey:@(nId)];
        //[strongSelf.soreDic setObject:score forKey:@(nId-3)];
    };
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  UITableViewAutomaticDimension; //[ReviewedCell CellheightWithStr:[NSString stringWithFormat:@"%@ (%ld)",self.reviewedModel.choices[indexPath.row].vcOption,(long)self.reviewedModel.choices[indexPath.row].nScore]];
}

- (void)sendBtnClick {
    //self.soreDic.allKeys.count+1
    if (self.soreDic.allKeys.count != self.reviewedModel.choices.count) {
        
        
        [MBProgressHUD showMessage:@"请填写所有的选项" RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        
        
        return;
    }
    
    //nTopicsId=0&vcTel=110&answer_0=10&answer_1=10&answer_2=10
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setObject:@(self.reviewedModel.nId) forKey:@"nTopicsId"];
    [dic setObject:self.reviewedModel.beGreadPerson[self.index].vcTel forKey: @"vcTel"];

//    self.soreArr = [[NSArray alloc] init];
//    self.soreArr = self.soreDic.allKeys;
//    self.soreArr = [self.soreArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2){
//        NSComparisonResult result = [obj1 compare:obj2];
//        return result==NSOrderedDescending;
//    }];
    //[dic setObject:@(self.reviewedModel.beGreadPerson[self.index].optionScore[self.index].nScore) forKey:@"nScore"];
    for (NSString *str in self.soreDic.allKeys) {
        NSString *string = [NSString stringWithFormat:@"answer_%@",str];
        DLog(@"%@",string);
//        NSMutableArray * arrM = [[NSMutableArray alloc] initWithCapacity:10];
//        [arrM addObject:self.soreDic[str]];
//        DLog(@"arrM%@",arrM);
        [dic setObject:self.soreDic[str] forKey:string];
        //[dic setObject:self.soreDic[str] forKey:str];
       // [dic setObject:self.soreDic[[NSString stringWithFormat:@"%d",[str intValue] -3]] forKey:[NSString stringWithFormat:@"%d",[str intValue]-3]];
       //[dic setObject:self.soreDic[self.choice.nId] forKey:@(self.reviewedModel.nId)];
    }
//    for (int i = 0; i < arrM.count; i++) {
//        [dic setObject:arrM forKey:@(i)];
//    }
//    for (int i = 0; i < self.soreArr.count; i++) {
//        NSString *string = [NSString stringWithFormat:@"answer_%@",self.soreArr[i]];
//        DLog(@"%@",string);
//       // [dic setObject:self.soreDic[self.soreArr[i]] forKey:self.soreArr[i]];
//         [dic setObject:self.soreDic[self.soreArr[i]] forKey:string];
//    }
    
    
    [DataRequest PostWithURL:SendReviewed parameter:dic:^(id reponserObject) {
        DLog(@"reponserObject%@",reponserObject);
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"nRes"]] isEqualToString:@"1"]) {
            
            [MBProgressHUD showMessage:@"评议成功" RemainTime:1 ToView:self.view userInteractionEnabled:YES];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                [self.navigationController popViewControllerAnimated:YES];
            });
        }
    }];
}
@end
