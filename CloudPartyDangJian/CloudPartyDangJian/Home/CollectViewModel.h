//
//  CollectViewModel.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ObjectsX;
@interface CollectViewModel : NSObject

//@property (nonatomic, copy) NSString *vcJumpLink;
//@property (nonatomic, assign) NSInteger nTemplate;
//@property (nonatomic, assign) NSInteger nNeedLogin;
//@property (nonatomic, assign) NSInteger nUse;
//@property (nonatomic, assign) NSInteger nSort;
//@property (nonatomic, copy) NSString *vcModule;
//@property (nonatomic, assign) NSInteger nId;
//@property (nonatomic, copy) NSString *vcRemark;
//@property (nonatomic, copy) NSString *vcIconUrl;
//@property (nonatomic, assign) NSInteger unRead;
//
//
//+ (CollectViewModel *)createModelWithDict:(NSDictionary *)dic;

@property (nonatomic, assign) BOOL result;
@property (nonatomic, strong) NSArray<ObjectsX *> *objectX;


@end



@interface ObjectsX : NSObject

@property (nonatomic, copy) NSString *vcJumpLink;
@property (nonatomic, assign) NSInteger nTemplate;
@property (nonatomic, assign) NSInteger nNeedLogin;
@property (nonatomic, assign) NSInteger nUse;
@property (nonatomic, assign) NSInteger nSort;
@property (nonatomic, copy) NSString *vcModule;
@property (nonatomic, assign) NSInteger nId;
@property (nonatomic, copy) NSString *vcRemark;
@property (nonatomic, copy) NSString *vcIconUrl;
@property (nonatomic, assign) NSInteger unRead;

@end





