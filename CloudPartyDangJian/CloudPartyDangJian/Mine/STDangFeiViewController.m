//
//  STDangFeiViewController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/31.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "STDangFeiViewController.h"
#import "STDangFeiTableViewCell.h"
#import "STDangFeiModel.h"
 static NSString * STDangFeiTableViewCellID = @"STDangFeiTableViewCellID";

@interface STDangFeiViewController () <UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSArray *datas;
@end

@implementation STDangFeiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"我的党费";
    self.tableView.rowHeight = 90;
    self.tableView.tableFooterView = [UIView new];
    [self.tableView registerNib:[UINib nibWithNibName:@"STDangFeiTableViewCell" bundle:nil] forCellReuseIdentifier:STDangFeiTableViewCellID];
    [self fetchData];
}

- (NSArray *)datas {
    if (!_datas) {
        _datas = [NSArray array];
    }
    return _datas;
}
- (void)fetchData {
    [DataRequest GetWithURL:DANGFei :^(id reponserObject) {
        if(reponserObject[@"objects"]) {
            self.datas = [STDangFeiModel mj_objectArrayWithKeyValuesArray:reponserObject[@"objects"]];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - DELEGATE DATA
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.datas.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    STDangFeiTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:STDangFeiTableViewCellID];
    STDangFeiModel *dangFei = self.datas[indexPath.row];
    cell.timeLabel.text = dangFei.dtExe;
    NSDate *date = [NSDate dateWithString:dangFei.dtExe format:@"YYYY-MM-dd HH:mm:ss"];
    
    cell.nameLabel.text = [NSString stringWithFormat:@"%@年%@月份党费",@(date.year),@(date.month)];
    cell.moneyLabel.text = [NSString stringWithFormat:@"%.2f元",dangFei.mRealFee];
    return  cell;
}

@end
