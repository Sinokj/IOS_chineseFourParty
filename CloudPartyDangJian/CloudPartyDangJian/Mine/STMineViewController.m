//
//  STMineViewController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "STMineViewController.h"
#import "STMineCell.h"
#import "MineModel.h"
#import "MineSetController.h"
#import "MineListController.h"
#import "MineExamController.h"
#import "STDangFeiViewController.h"
#import "STTuShuBiViewController.h"
#import "MyMessageVC.h"
#import "DESaoMIaoController.h"
#import "PartyArticleListModel.h"
#import "STStarLevelView.h"

@implementation STMineItem
+ (STMineItem *)mineItemWithImageName:(NSString *)imageName categoryName:(NSString *)categoryName {
    STMineItem *item = [STMineItem new];
    item.imageIcon = [UIImage imageNamed:imageName];
    item.categoryName = categoryName;
    return item;
}
@end

// cellID
static NSString *const STMineCellID = @"STMineCellID";
@interface STMineViewController () <UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UIImageView *avatarImageView;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *groupNameLabel;

@property (nonatomic, strong) UILabel *levelPointLabel;
@property (nonatomic, strong) STStarLevelView *starView;
@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) MineModel *mineModel; //用户信息模型
@end

@implementation STMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //绘制视图
    [self setUpView];
    // 初始化一些点击事件
    [self setupEventAction];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 初始化导航栏
    self.navigationController.navigationBar.hidden = YES;
    // 请求用户信息
    [self requestUserInfo];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)setUpView{
    UIView *headBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 570 * kWScale)];
    headBgView.backgroundColor = AppTintColor;
    
    UIImageView *backgroundView = [[UIImageView alloc] init];
    backgroundView.image = [UIImage imageNamed:@"bg_mine"];
    backgroundView.userInteractionEnabled = YES;
    [headBgView addSubview:backgroundView];
    [backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(headBgView);
    }];
    
    // 标题
    UILabel *titleLab = [UILabel customLablWithFrame:CGRectZero andTitle:@"个人中心" andFontNumber:1];
    titleLab.font = [UIFont systemFontOfSize:17];
    titleLab.textColor = [UIColor whiteColor];
    [headBgView addSubview:titleLab];
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(30);
        make.centerX.offset(0);
    }];
    
    // 头像
    self.avatarImageView = [UIImageView new];
    self.avatarImageView.backgroundColor = [UIColor whiteColor];
    self.avatarImageView.userInteractionEnabled = YES;
    [headBgView addSubview:self.avatarImageView];
    self.avatarImageView.sd_layout
    .widthIs(170 * kWScale)
    .heightIs(170 * kWScale)
    .leftSpaceToView(headBgView, 100 * kWScale)
    .topSpaceToView(headBgView, 150 * kWScale);
    self.avatarImageView.sd_cornerRadiusFromWidthRatio = @(0.5);
    
    //昵称
    self.userNameLabel = [UILabel labelWithTextColor:[UIColor whiteColor] font:19 backgroundColor:nil Text:@"姓名"];
    [headBgView addSubview:self.userNameLabel];
    self.userNameLabel.sd_layout
    .centerYEqualToView(self.avatarImageView)
    .leftSpaceToView(self.avatarImageView, 50 * kWScale)
    .heightIs(26)
    .rightSpaceToView(headBgView, 50);
    
    //部门
    self.groupNameLabel = [UILabel labelWithTextColor:[UIColor whiteColor] font:15 backgroundColor:nil Text:@"姓名"];
    [headBgView addSubview:self.groupNameLabel];
    self.groupNameLabel.sd_layout
    .leftEqualToView(self.userNameLabel)
    .topSpaceToView(self.userNameLabel, 6)
    .heightIs(20)
    .rightSpaceToView(headBgView, 20);
    
    //学习得分
    UILabel *scoreLab = [UILabel labelWithTextColor:[UIColor whiteColor] font:14 backgroundColor:nil Text:@"学习得分"];
    [headBgView addSubview:scoreLab];
    scoreLab.sd_layout
    .leftEqualToView(self.avatarImageView)
    .topSpaceToView(self.avatarImageView, 60 * kWScale)
    .heightIs(20);
    [scoreLab setSingleLineAutoResizeWithMaxWidth:100];
    
    //学习等级
    UILabel *levelLab = [UILabel labelWithTextColor:[UIColor whiteColor] font:14 backgroundColor:nil Text:@"学习等级"];
    [headBgView addSubview:levelLab];
    [levelLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userNameLabel).offset(65);
        make.centerY.equalTo(scoreLab);
        make.height.mas_equalTo(20);
        make.width.mas_lessThanOrEqualTo(100);
    }];
    
    //分数
    self.levelPointLabel = [UILabel labelWithTextColor:[UIColor whiteColor] font:30 backgroundColor:nil Text:@"0"];
    [headBgView addSubview:self.levelPointLabel];
    self.levelPointLabel.sd_layout
    .leftEqualToView(self.avatarImageView)
    .topSpaceToView(scoreLab, 10)
    .heightIs(90 * kWScale);
    [self.levelPointLabel setSingleLineAutoResizeWithMaxWidth: 150];
    
    self.starView = [STStarLevelView new];
    [headBgView addSubview:self.starView];
    [self.starView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.userNameLabel);
        make.top.equalTo(levelLab.mas_bottom).offset(10);
        make.width.mas_equalTo(190);
        make.height.mas_equalTo(30);
    }];
    
    if(@available(iOS 11.0, *)){
        self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, -kStatusBarHeight, ScreenWidth, ScreenHeight) style:(UITableViewStyleGrouped)];
    }else{
        self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kStatusBarHeight) style:(UITableViewStyleGrouped)];
    }
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    self.tableV.dataSource = self;
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    //注册xib
    [self.tableV registerNib:[UINib nibWithNibName:@"STMineCell" bundle:nil] forCellReuseIdentifier:STMineCellID];
    self.tableV.tableHeaderView = headBgView;
    self.tableV.tableFooterView = [UIView new];
}

#pragma mark - net
// 请求用户信息
- (void)requestUserInfo {
    [MBProgressHUD hideHUD];
    [DataRequest CatlikeGetWithURL:MineInfo :^(id reponserObject) {
        self.mineModel = [MineModel mj_objectWithKeyValues:reponserObject];
        // 更新用户信息
        [self updateUserInfo];
    }];
}

#pragma mark - private
// 更新用户信息
- (void)updateUserInfo {
    // 头像
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:self.mineModel.baseInfo.vcHeadImgUrl] placeholderImage:[UIImage imageNamed:@"face"]];
    // 名字
    self.userNameLabel.text = self.mineModel.baseInfo.vcName;
    // 所在组
    self.groupNameLabel.text = self.mineModel.baseInfo.vcDeptName;
    [self.starView updateLevel:[self.mineModel.level.nLevel intValue]];
    // 学习分数
    self.levelPointLabel.text = [NSString stringWithFormat:@"%.2f",self.mineModel.nLevelPoint];
}

#pragma mark - Lazy
- (NSArray<STMineItem *> *)items {
    if (_items == nil) {
        _items = @[ @[
                        [STMineItem mineItemWithImageName:@"round1" categoryName:@"我的学习"],
                        [STMineItem mineItemWithImageName:@"round2" categoryName:@"我的答题"],
                        [STMineItem mineItemWithImageName:@"round3" categoryName:@"我的党费"]
                      ],
                    @[
                        [STMineItem mineItemWithImageName:@"round4" categoryName:@"系统设置"]
                    ]
                   ];
    }
    return _items;
}

#pragma mark - 初始化

// 导航栏style
- (void)setupNavStyle {
    self.title = @"个人中心";
    self.tabBarItem.title = @"个人中心";
    // rightIem
    @weakify(self);
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"set2" block:^(id sender) {
        @strongify(self);
        // 去往设置界面
        MineSetController *mineSet = [MineSetController new];
        mineSet.aboutMe = self.mineModel.about;
        [self.navigationController pushViewController:mineSet animated:YES];
    }];
}

#pragma mark - Event
- (void)setupEventAction {
    self.avatarImageView.userInteractionEnabled = YES;
    @weakify(self);
    [self.avatarImageView bk_whenTapped:^{
        @strongify(self);
        // 获取头像
        [STUtils imagePickerWithVc:self maxImagesCount:1 finishPickingBlock:^(NSArray<UIImage *> *photos) {
            @strongify(self);
            // 上传头像
            if(photos && photos.count > 0) {
                [self uploadUserAvatar:photos[0]];
            }
        }];
    }];
}

#pragma mark - Extra Private
// 上传用户头像到服务器
- (void)uploadUserAvatar:(UIImage *)avatarImage {
    
    // 首先直接显示新头像
    dispatch_async(dispatch_get_main_queue(), ^{
        self.avatarImageView.image = avatarImage;
    });
    
    // 上传
    [DataRequest PostWithURL:UpLoadIco parameter:nil andImage:[avatarImage imageByResizeToSize:CGSizeMake(100, 100)]:^(id responseObject) {
        DLog(@"%@",responseObject);
        if ([responseObject[@"result"] isEqualToString:@"uploadSuccess"]) {
            self.avatarImageView.image = avatarImage;
            [MBProgressHUD showMessage:@"上传头像成功" RemainTime:1 ToView:[UIApplication sharedApplication].keyWindow userInteractionEnabled:YES];
        }else {
        }
        
    } failure:^(NSError *error) {
        DLog(@"%@",error);
    }];
}

// 跳转到我的学习
- (void)navigateToMyStudy {
    MineListController *mineList = [MineListController new];
    mineList.PocketTutorPoint = @(self.mineModel.baseInfo.nPocketTutorPoint).description;
    [self.navigationController pushViewController:mineList animated:YES];
}

// 跳转到我的答题
- (void)navigateToMyExam {
    MineExamController *mineExam = [MineExamController new];
    mineExam.exam = self.mineModel.PalmExam;
    mineExam.titleStr = @"我的答题";
    [self.navigationController pushViewController:mineExam animated:YES];
}

// 跳转到我的党费
- (void)navigateToMyRepete {
    STDangFeiViewController *party = [STDangFeiViewController new];
    [self.navigationController pushViewController:party animated:YES];
}

// 我的消息
- (void)navigateToMyMsg{
    MyMessageVC *party = [MyMessageVC new];
    [self.navigationController pushViewController:party animated:YES];
}


#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *temp = self.items[section];
    return temp.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *temp = self.items[indexPath.section];
    
    STMineCell *cell = [tableView dequeueReusableCellWithIdentifier:STMineCellID forIndexPath:indexPath];
    [cell setMineItem:temp[indexPath.row]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section   {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return  0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        // 去往设置界面
        MineSetController *mineSet = [MineSetController new];
        mineSet.aboutMe = self.mineModel.about;
        [self.navigationController pushViewController:mineSet animated:YES];
    }else{
        if (indexPath.row == 0) {
            // 我的学习
            [self navigateToMyStudy];
        } else if (indexPath.row == 1) {
            // 我的答题
            [self navigateToMyExam];
        } else if (indexPath.row == 2){
            // 我的竞赛
            [self navigateToMyRepete];
        }
    }
}
@end
