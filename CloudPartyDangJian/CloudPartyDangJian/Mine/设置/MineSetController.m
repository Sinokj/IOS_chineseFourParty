//
//  MineSetController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineSetController.h"
#import "WebViewController.h"
#import "ResetPWController.h"
#import <Masonry.h>
#import "NSObject+rootVcExtension.h"
#import "LoginController.h"
#import "STNavigationController.h"
@interface MineSetController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@property (nonatomic, strong) NSArray *arr;

@end

@implementation MineSetController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"系统设置";
    
    self.arr = @[@[@"  当前版本"],@[@"  声音",@"  震动"], @[@"  修改密码"]];
    
    [self prepareLayout];
}

- (void)prepareLayout {
    
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 190) style:(UITableViewStyleGrouped)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.scrollEnabled = NO;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    [self.tableV registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];

    [self.tableV registerClass:[UITableViewCell class] forCellReuseIdentifier:@"firstcell"];
    
    UIButton *btn = [UIButton buttonWithType:(UIButtonTypeCustom)];
    btn.backgroundColor = AppTintColor;
    btn.titleLabel.font = FontSystem(18);
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitle:@"退出登录" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(loginOutBtnClick) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:btn];
    @weakify(self);
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(weak_self.view.mas_centerX);
        make.width.mas_equalTo(MYDIMESCALEW(275));
        make.height.mas_equalTo(MYDIMESCALEH(44));
        make.top.equalTo(weak_self.tableV.mas_bottom).offset(MYDIMESCALEH(80));
    }];
    btn.layer.cornerRadius = 22;
    btn.layer.masksToBounds = YES;
    
    
//    UIView *lineV = [UIView new];
//    lineV.backgroundColor = UIColorHex(f5f5f5);
//    [self.view addSubview:lineV];
//    [lineV mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.offset(0);
//        make.top.mas_equalTo(weak_self.tableV.mas_bottom).offset(0);
//        make.height.mas_equalTo(1);
//    }];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1) {
        return 2;
    }else{
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 2) {//修改密码
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"firstcell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        cell.contentView.backgroundColor = UIColorHex(f5f5f5);
        UILabel *label = [UILabel customLablWithFrame:CGRectMake(12, 7.5, ScreenWidth/2, 30) andTitle:self.arr[indexPath.section][indexPath.row] andFontNumber:16];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell addSubview:label];
        return cell;
    }else if(indexPath.section == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell removeAllSubviews];
        
        UILabel *label = [UILabel customLablWithFrame:CGRectMake(12, 7.5, ScreenWidth/2, 30) andTitle:self.arr[indexPath.section][indexPath.row] andFontNumber:16];
        
        NSDictionary* infoDic = [[NSBundle mainBundle] infoDictionary];
        NSString* currentVersion = [infoDic objectForKey:@"CFBundleShortVersionString"];//当前版本号
        
        UILabel *right = [UILabel customLablWithFrame:CGRectMake(ScreenWidth/2, 7.5, ScreenWidth/2 - 20, 30) andTitle:[NSString stringWithFormat:@"V%@",currentVersion] andFontNumber:16];
        
        right.textAlignment = NSTextAlignmentRight;
        
        [cell addSubview:right];
        
        [cell addSubview:label];
        
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell removeAllSubviews];
        
        UILabel *label = [UILabel customLablWithFrame:CGRectMake(12, 7.5, ScreenWidth/2, 30) andTitle:self.arr[indexPath.section][indexPath.row] andFontNumber:16];
        
        UISwitch *swich = [[UISwitch alloc] initWithFrame:CGRectMake(ScreenWidth - 60, 7.5, 40, 30)];
        id voiceOrShake = nil;
        if ([label.text isEqualToString:@"  声音"]) {
            voiceOrShake = [STUtils objectForKey:KSetVoice];
            if (voiceOrShake == nil || [voiceOrShake boolValue] == YES) {
                swich.on = YES;
            }else {
                swich.on = NO;
            }
        }else {
            voiceOrShake = [STUtils objectForKey:KSetShake];
            if (voiceOrShake == nil || [voiceOrShake boolValue] == YES) {
                swich.on = YES;
            }else {
                swich.on = NO;
            }
            
        }
        @weakify(self);
        [swich removeAllBlocksForControlEvents:UIControlEventValueChanged];
        [swich addBlockForControlEvents:UIControlEventValueChanged block:^(UISwitch  * sender) {
            if ([weak_self.arr[indexPath.section][indexPath.row] isEqualToString:@"  声音"]) {
                [STUtils setObject:@(sender.on) forKey:KSetVoice];
            }else {
                [STUtils setObject:@(sender.on) forKey:KSetShake];
            }
        }];
        
        
        swich.tag = indexPath.section + 1000;
        
        [cell addSubview:swich];
        
        [cell addSubview:label];
        
        return cell;
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 45;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section   {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return  0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0.01;
    }else{
        return 5;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 3) {
        
        return;
        
    }else if (indexPath.section ==2) {
        
        ResetPWController *reset = [ResetPWController new];
        
        [self.navigationController pushViewController:reset animated:YES];
    }
}


- (void)loginOutBtnClick {
 
    [DataRequest PostWithURL:LoginOut parameter:nil :^(id reponserObject) {
        if ([[NSString stringWithFormat:@"%@",reponserObject[@"result"]] isEqualToString:@"1"]) {

            //登录状态
            [STUtils setObject:@0 forKey:KIsLogin];
            
            //清楚cookie
            [STUtils clearCookie];
            
            [MBProgressHUD showMessage:@"退出成功" RemainTime:1 ToView:self.view userInteractionEnabled:YES];

//            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//
//
//                [self.homeVc updateInfoAfterLoginIn:@"2"];
//
//                [STUtils setObject:@"2" forKey:KDangJianType];
//                self.tabBarVc.selectedIndex = 0;
//                [self.bullVc reloadData];
//                
//                [self.webVc.webV reload];
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    self.tabBarVc.tabBar.hidden = NO;
//                    [self.navigationController popViewControllerAnimated:NO];
//
//                    [[NSNotificationCenter defaultCenter] postNotificationName:@"isLoginUpdateData" object:nil userInfo:nil];
//                });
//            });
            
            LoginController* logonVC = [[LoginController alloc]init];
            UINavigationController *loginNav = [[STNavigationController alloc] initWithRootViewController:logonVC];
            [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:loginNav animated:NO completion:nil];
            
        }else {
            [MBProgressHUD showMessage:reponserObject[@"vcRes"] RemainTime:2 ToView:self.view userInteractionEnabled:YES];
        }
    }];
}

@end
