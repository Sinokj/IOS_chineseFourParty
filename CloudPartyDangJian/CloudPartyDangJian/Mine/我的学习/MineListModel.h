//
//  MineListModel.h
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/12/21.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListModel.h"

@class MineLists;
@interface MineListModel : NSObject

@property (nonatomic, strong) NSArray<NewList *> *objects;

@end
@interface MineLists : NSObject

@property (nonatomic, assign) NSInteger nclick;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, copy) NSString *dtReg;

@property (nonatomic, assign) NSInteger nStatus;

@property (nonatomic, assign) BOOL bCustom;

@property (nonatomic, copy) NSString *vcContent;

@property (nonatomic, copy) NSString *vcRegister;

@property (nonatomic, assign) NSInteger nLogin;

@property (nonatomic, copy) NSString *vcUrl;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *vcGroupId;

@property (nonatomic, copy) NSString *vcType;

@property (nonatomic, assign) NSInteger nShow;

@property (nonatomic, copy) NSString *vcPlatform;

@property (nonatomic, copy) NSString *vcDescribe;

@property (nonatomic, assign) NSInteger nShared;

@property (nonatomic, copy) NSString *vcMemo;

@property (nonatomic, assign) NSInteger nOrder;

@property (nonatomic, assign) NSInteger rn;

@property (nonatomic, copy) NSString *vcTitle;

@property (nonatomic, copy) NSString *dtRead;

@property (nonatomic, copy) NSString *vcPath;

@end

