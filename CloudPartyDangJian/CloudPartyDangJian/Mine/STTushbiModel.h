//
//  STTushbiModel.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/9.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface STTushbiModel : NSObject
@property (nonatomic , copy) NSString              * nTokenFee;
@property (nonatomic , copy) NSString              * vcOrderNo;
@property (nonatomic , copy) NSString              * vcType;
@property (nonatomic , copy) NSString              * vcPersonTel;
@property (nonatomic , copy) NSString              * nId;
@property (nonatomic , copy) NSString              * dtReg;
@end

NS_ASSUME_NONNULL_END
