//
//  MineModel.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class Baseinfo,Discussions,Content,MineExam,Level;
@interface MineModel : NSObject

@property (nonatomic, strong) NSString *PocketTutorPoint;

@property (nonatomic, assign) CGFloat nLevelPoint;

@property (nonatomic, strong) Baseinfo *baseInfo;

@property (nonatomic, strong) Level *level;

@property (nonatomic, strong) NSArray<MineExam *> *exam;

/// 我的答题
@property (nonatomic, strong) NSArray<MineExam *> *PalmExam;

@property (nonatomic, strong) NSArray<MineExam *> *UncorruptedExam;

@property (nonatomic, copy) NSString *about;

@property (nonatomic, strong) NSArray<Discussions *> *discussions;

@end

@interface Baseinfo : NSObject

@property (nonatomic, copy) NSString *vcTel;

@property (nonatomic, copy) NSString *vcName;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *vcHeadImgUrl;

@property (nonatomic, copy) NSString *vcLevel;

@property (nonatomic, copy) NSString *vcDeptName;

@property (nonatomic, assign) CGFloat nPocketTutorPoint;
@end

@interface Level: NSObject
@property (nonatomic, copy) NSString *nLevel;
@property (nonatomic, copy) NSString *vcLevel;
@end


@interface Discussions : NSObject

@property (nonatomic, copy) NSString *vcName;

@property (nonatomic, strong) NSArray<Content *> *content;

@property (nonatomic, copy) NSString *vcTitle;

@property (nonatomic, copy) NSString *vcTel;

@property (nonatomic, assign) NSInteger nScore;

@property (nonatomic, assign) NSInteger nId;

@property (nonatomic, copy) NSString *vcBeGreadPerson;

@end

@interface Content : NSObject

@property (nonatomic, copy) NSString *vcOption;

@property (nonatomic, assign) NSInteger nOptionScore;

@property (nonatomic, assign) NSInteger nScore;

@end

@interface MineExam : NSObject

@property (nonatomic, assign) BOOL isPass;

@property (nonatomic, assign) NSInteger nPass;

@property (nonatomic, copy) NSString *vcTitle;

@property (nonatomic, copy) NSString *nId;

@property (nonatomic, assign) NSInteger nScore;

@property (nonatomic, assign) NSInteger answerdNumber;
@property (nonatomic, assign) NSInteger nTopicsTotal;
@property (nonatomic, assign) NSInteger examedNumber;
@property (nonatomic, assign) NSInteger nTotal;
@property (nonatomic, copy) NSString *vcRegister;
@property (nonatomic, copy) NSString *dtReg;
@property (nonatomic, copy) NSString *dtEnd;
@end

