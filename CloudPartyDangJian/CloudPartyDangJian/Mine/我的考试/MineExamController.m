//
//  MineExamController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineExamController.h"
#import "MineExamCell.h"
#import "PrepareExamController.h"

@interface MineExamController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@end

@implementation MineExamController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = self.titleStr;
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
}

- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.backgroundColor = UIColorFromHex(0xE3E3E3);
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
//    [self.tableV registerNib:[UINib nibWithNibName:@"ExamListCell" bundle:nil] forCellReuseIdentifier:@"MineExamCell"];
//    [self.tableV registerClass:[MineExamCell class] forCellReuseIdentifier:@"MineExamCell"];
    
    [self.tableV registerNib:[UINib nibWithNibName:@"MineExamCell" bundle:nil] forCellReuseIdentifier:@"MineExamCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.exam.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MineExamCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MineExamCell" forIndexPath:indexPath];
    
    cell.mineExamM = self.exam[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ExamLists *examList = [ExamLists new];
    
    examList.isBegin = 2;
    
    examList.nId = [self.exam[indexPath.row].nId integerValue];
    
    examList.answerdNumber = self.exam[indexPath.row].answerdNumber;
    examList.nTopicsTotal = self.exam[indexPath.row].nTopicsTotal;
    examList.examedNumber = self.exam[indexPath.row].examedNumber;
    examList.nTotal = self.exam[indexPath.row].nTotal;
    examList.nPass = self.exam[indexPath.row].nPass;
    examList.vcRegister = self.exam[indexPath.row].vcRegister;
    examList.dtReg = self.exam[indexPath.row].dtReg;
    examList.vcTitle = self.exam[indexPath.row].vcTitle;
    
    
    
    PrepareExamController *prepareE = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"PrepareExamController"];
    prepareE.title = @"试题";
    prepareE.isMine = YES;
    
    prepareE.nId = self.exam[indexPath.row].nId;
    
    prepareE.examList = examList;
    
    [self.navigationController pushViewController:prepareE animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100 ;
}


@end
