//
//  MineExamCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineExamCell.h"
#import "DyfTool.h"

@interface MineExamCell ()


@property(nonatomic, strong) UIImageView *imageV;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

//@property(nonatomic, strong) UILabel *titleLabel;
/**
 *  得分8
 */
@property (weak, nonatomic) IBOutlet UILabel *scoreL;
//@property (nonatomic, strong) UILabel *scoreL;
/**
 *  考试结果
 */
@property (weak, nonatomic) IBOutlet UIImageView *resultL;
//@property (nonatomic, strong) UILabel *resultL;

// 考试时间阶段
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
// 考试结果新的放在了右下角
@property (weak, nonatomic) IBOutlet UIImageView *resultImageV;


@end
@implementation MineExamCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.contentView.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, ScreenWidth - 6, 80)];
    
    [self.contentView addSubview:bgView];
    
    bgView.backgroundColor = [UIColor whiteColor];
    
    self.imageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 40, 40)];
    
    self.imageV.image = [UIImage imageNamed:@"ic_doc"];
    
    [bgView addSubview:self.imageV];
    
    
    self.titleLabel = [UILabel customLablWithFrame:CGRectMake(self.imageV.right + 10, 8, ScreenWidth - 120, 20) andTitle:@"测试" andFontNumber:15];
    
    self.titleLabel.centerY = self.imageV.centerY;
    
    [bgView addSubview:self.titleLabel];
    
    
    self.scoreL = [UILabel customLablWithFrame:CGRectMake(10, self.imageV.bottom + 10, ScreenWidth / 2 - 20, 20) andTitle:@"得分" andFontNumber:13];
    
    [bgView addSubview:self.scoreL];
    
    
//    self.resultL = [UILabel customLablWithFrame:CGRectMake(self.scoreL.right, self.scoreL.top, ScreenWidth / 2 - 20, 20) andTitle:@"考试结果" andFontNumber:13];
//
//    self.resultL.textAlignment = NSTextAlignmentRight;
    
    [bgView addSubview:self.resultL];
}

- (void)awakeFromNib {
    [super awakeFromNib];
//    [STUtils setupView:self.resultContentView cornerRadius:5 bgColor:AppTintColor borderW:0 borderColor:nil];
}

-(void)setMineExamM:(MineExam *)mineExamM {
    if (_mineExamM != mineExamM) {
        _mineExamM = nil;
        _mineExamM = mineExamM;
        
        self.titleLabel.text = mineExamM.vcTitle;
        
        
        self.scoreL.text = @(mineExamM.nScore).description;
        
        
        // 时间
            
        NSDateFormatter *format = [NSDateFormatter new];
        format.dateFormat = @"YYYY-MM-dd HH:mm:ss";
        self.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",[[format dateFromString:mineExamM.dtReg] stringWithFormat:@"YYYY-MM-dd"],[[format dateFromString:mineExamM.dtEnd] stringWithFormat:@"YYYY-MM-dd"]];
        
//        [self setAttributedStr:[NSString stringWithFormat:@"%ld",(long)] andFrome:4 andLength:[[NSString stringWithFormat:@"%ld",(long)mineExamM.nScore] length] withLabel:self.scoreL andColor:COLOR_RGB(0xffd700)];
        
        if (!mineExamM.isPass) {
            
            self.resultL.image = [UIImage imageNamed: @"label_lose"];
            self.resultImageV.image = [UIImage imageNamed: @"icon_lose2"];
//            [self setAttributedStr:[NSString stringWithFormat:@"考试结果: 未通过"] andFrome:6 andLength:3 withLabel:self.resultL andColor:[UIColor redColor]];
        }else {
//            self.resultLabel.text = @"已通过";
            self.resultImageV.image = [UIImage imageNamed: @"icon_complete2"];
//            self.resultContentView.backgroundColor = AppTintColor;
            self.resultL.image = [UIImage imageNamed: @"label_success"];
//            [self setAttributedStr:[NSString stringWithFormat:@"考试结果: 通过"] andFrome:6 andLength:2 withLabel:self.resultL andColor:[UIColor greenColor]];
        }
        
    }
}

- (void)setAttributedStr:(NSString *)str andFrome:(NSInteger)start andLength:(NSInteger)length withLabel:(UILabel *)label andColor:(UIColor *)color{
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    
    NSDictionary *dic = @{ NSForegroundColorAttributeName:color};
    
    [attributedString addAttributes:dic range:NSMakeRange(start, length)];
    
    label.attributedText = attributedString;
    
}
@end
