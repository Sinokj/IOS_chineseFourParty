//
//  STDangFeiTableViewCell.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/31.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "STDangFeiTableViewCell.h"

@implementation STDangFeiTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
