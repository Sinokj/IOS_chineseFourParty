//
//  STStarLevelView.h
//  CloudPartyDangJian
//
//  Created by 李子栋 on 2019/11/5.
//  Copyright © 2019 TWO. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface STStarLevelView : UIView

- (void)updateLevel:(int)level;

@end

NS_ASSUME_NONNULL_END
