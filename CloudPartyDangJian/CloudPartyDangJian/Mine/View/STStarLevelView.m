//
//  STStarLevelView.m
//  CloudPartyDangJian
//
//  Created by 李子栋 on 2019/11/5.
//  Copyright © 2019 TWO. All rights reserved.
//

#import "STStarLevelView.h"

@interface STStarLevelView ()

@property (nonatomic, strong) UIImageView *one;
@property (nonatomic, strong) UIImageView *two;
@property (nonatomic, strong) UIImageView *three;
@property (nonatomic, strong) UIImageView *four;
@property (nonatomic, strong) UIImageView *five;
@property (nonatomic, strong) UIImageView *six;
@property (nonatomic,   copy) NSArray <UIImageView *>*imageViews;

@end

@implementation STStarLevelView

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInitialization];
    }
    return self;
}

#pragma mark - CommonInitialization

- (void)commonInitialization {
    [self addSubview:self.one];
    [self addSubview:self.two];
    [self addSubview:self.three];
    [self addSubview:self.four];
    [self addSubview:self.five];
    [self addSubview:self.six];
    
    for (int i = 0; i < self.imageViews.count; i ++) {
        UIImageView *imageView = self.imageViews[i];
        imageView.image = [UIImage imageNamed:@"icon_mine_star_level_nor"];
        [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).offset(32 * i);
            make.top.equalTo(self);
            make.width.height.mas_equalTo(30);
        }];
    }
}

#pragma mark - Public

- (void)updateLevel:(int)level {
    if (level < 1) {
        level = 1;
    }
    if (level > 6) {
        level = 6;
    }
    for (int i = 0; i < self.imageViews.count; i ++) {
        UIImageView *imageView = self.imageViews[i];
        if (i + 1 > level) {
            imageView.image = [UIImage imageNamed:@"icon_mine_star_level_nor"];
        }
        else {
            imageView.image = [UIImage imageNamed:@"icon_mine_star_level_sel"];
        }
    }
}

#pragma mark - Getters

- (UIImageView *)one {
    if (_one) {
        return _one;
    }
    _one = [UIImageView new];
    return _one;
}

- (UIImageView *)two {
    if (_two) {
        return _two;
    }
    _two = [UIImageView new];
    return _two;
}

- (UIImageView *)three {
    if (_three) {
        return _three;
    }
    _three = [UIImageView new];
    return _three;
}

- (UIImageView *)four {
    if (_four) {
        return _four;
    }
    _four = [UIImageView new];
    return _four;
}

- (UIImageView *)five {
    if (_five) {
        return _five;
    }
    _five = [UIImageView new];
    return _five;
}

- (UIImageView *)six {
    if (_six) {
        return _six;
    }
    _six = [UIImageView new];
    return _six;
}

- (NSArray<UIImageView *> *)imageViews {
    if (_imageViews) {
        return _imageViews;
    }
    _imageViews = @[self.one, self.two, self.three,
                    self.four, self.five, self.six];
    return _imageViews;
}

@end
