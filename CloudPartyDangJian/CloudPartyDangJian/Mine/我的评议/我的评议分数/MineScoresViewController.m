//
//  MineScoresViewController.m
//  EPartyConstruction
//
//  Created by ZJXN on 2017/12/15.
//  Copyright © 2017年 Dyf. All rights reserved.
//

#import "MineScoresViewController.h"
#import "ThinkCell.h"
@interface MineScoresViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableV;
@end

@implementation MineScoresViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    [self prepareLayout];
}
-(void)prepareLayout{
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    self.tableV.estimatedRowHeight = 300;
    [self.tableV registerClass:[ThinkCell class] forCellReuseIdentifier:@"MineScoresCellID"];
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ThinkCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MineScoresCellID" forIndexPath:indexPath];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
