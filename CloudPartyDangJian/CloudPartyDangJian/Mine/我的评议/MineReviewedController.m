//
//  MineReviewedController.m
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineReviewedController.h"
#import "MineReviewedCell.h"
#import "MineReviewedScoreController.h"

@interface MineReviewedController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableV;

@end

@implementation MineReviewedController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.title = @"我的评议";
    
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.superBtn];
    
    [self prepareLayout];
}


- (void)prepareLayout {
    
    self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64) style:(UITableViewStylePlain)];
    
    [self.view addSubview:self.tableV];
    
    self.tableV.delegate = self;
    
    self.tableV.dataSource = self;
    
    self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableV registerClass:[MineReviewedCell class] forCellReuseIdentifier:@"MineReviewedCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.discussionsM.count;
    //return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MineReviewedCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MineReviewedCell" forIndexPath:indexPath];
    
    Discussions *discuss = self.discussionsM[indexPath.row];
    
    cell.discuss = discuss;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
    MineReviewedScoreController *mineReviewedScore = [MineReviewedScoreController new];
    
    mineReviewedScore.content = self.discussionsM[indexPath.row].content;
    
    [self.navigationController pushViewController:mineReviewedScore animated:YES];

//    MineReviewedScoreController *mineReviewedScore = [MineReviewedScoreController new];
//
//    mineReviewedScore.content = self.discussionsM[indexPath.row].content;
//
//    [self.navigationController pushViewController:mineReviewedScore animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

@end
