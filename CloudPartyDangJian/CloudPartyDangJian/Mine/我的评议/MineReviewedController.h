//
//  MineReviewedController.h
//  EPartyConstruction
//
//  Created by SINOKJ on 16/7/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "ViewController.h"
#import "MineModel.h"

@interface MineReviewedController : ViewController

@property (nonatomic, strong) NSArray<Discussions *> *discussionsM;

@end
