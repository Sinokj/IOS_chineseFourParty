//
//  MineReviewedCell.m
//  EPartyConstruction
//
//  Created by SINOKJ on 2016/9/29.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "MineReviewedCell.h"
#import "DyfTool.h"

@interface MineReviewedCell ()

@property (nonatomic, strong) UILabel *nameL;

@property (nonatomic, strong) UILabel *titleL;

@property (nonatomic, strong) UILabel *scoreL;

@end
@implementation MineReviewedCell


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self prepareLayout];
    }
    return self;
}

- (void)prepareLayout {
    
    self.contentView.backgroundColor = [UIColor colorWithRed:0.8943 green:0.8943 blue:0.8943 alpha:1.0];
    
    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(3, 0, ScreenWidth - 6, 65)];
    
    [self.contentView addSubview:bgView];
    
    bgView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *imageV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 55, 55)];
    
    imageV.image = [UIImage imageNamed:@"ic_doc"];
    
    [bgView addSubview:imageV];
    
    self.nameL = [UILabel customLablWithFrame:CGRectMake(imageV.right + 10, 5, bgView.width - 60, 20) andTitle:@"测试" andFontNumber:15];
    
    [bgView addSubview:self.nameL];
    
    self.titleL = [UILabel customLablWithFrame:CGRectMake(imageV.right + 10, self.nameL.bottom, bgView.width - 60, 20) andTitle:@"测试" andFontNumber:15];
    
    [bgView addSubview:self.titleL];
    
    self.scoreL = [UILabel customLablWithFrame:CGRectMake(imageV.right + 10, self.titleL.bottom, bgView.width - 60, 20) andTitle:@"测试" andFontNumber:15];
    
    [bgView addSubview:self.scoreL];
    
}

- (void)setDiscuss:(Discussions *)discuss {
    if (_discuss != discuss) {
        _discuss = nil;
        _discuss = discuss;
        
        self.nameL.text = [NSString stringWithFormat:@"评议对象:  %@",discuss.vcName];
        
        self.titleL.text = [NSString stringWithFormat:@"评议标题:  %@",discuss.vcTitle];
        
        self.scoreL.text = [NSString stringWithFormat:@"评议分数:  %ld",discuss.nScore];
    }
}


@end
