//
//  WebViewController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "WebViewController.h"
#import "UIImage+ImageSize.h"

#import <ShareSDK/ShareSDK.h>
#import <ShareSDKUI/ShareSDK+SSUI.h>
// 自定义分享菜单栏需要导入的头文件
#import <ShareSDKUI/SSUIShareActionSheetStyle.h>
//自定义分享编辑界面所需要导入的头文件
#import <ShareSDKUI/SSUIEditorViewStyle.h>

#import <MJRefresh.h>
#import <WebKit/WebKit.h>

#define TRUEW(v) (v*([UIScreen mainScreen].bounds.size.width)/320)


@interface WebViewController ()<WKNavigationDelegate,UIWebViewDelegate,UIAlertViewDelegate>


//@property (nonatomic, strong) WKWebView *webV;
@property (nonatomic, strong) UIButton *leftBtn;

@property (nonatomic, strong) NSMutableArray *mUrlArray;

@property (nonatomic, strong) UIButton *bgBtnView;
@property (nonatomic, strong) UIImageView *imgView;

@property(nonatomic,strong)UIButton *closeBtn;

@end

@interface UIWebView (JavaScriptAlert)

- (void)webView:(UIWebView *)sender runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(id)frame;

@end

@implementation WebViewController

- (NSMutableArray *)mUrlArray {
    if (_mUrlArray) {
        _mUrlArray = [NSMutableArray array];
    }
    return _mUrlArray;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.webV reload];
    
    
    
//    if ([self.tit isEqualToString:@"图书阅览"]) {
        UIBarButtonItem *leftItem = [UIBarButtonItem barButtonItemWithImageName:@"back" title:@"返回" block:^(id sender) {
            if (self.webV.canGoBack) {
                [self.webV goBack];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
        self.navigationItem.leftBarButtonItem = leftItem;
        
        //关闭按钮
        NSInteger top = [UIScreen mainScreen].bounds.size.height == 812 ? 0 : 0;
        NSInteger margin = 10;
        NSInteger buttonHeight = 44;
        NSInteger buttonWidth = 44;
        
        self.closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.closeBtn.frame = CGRectMake(10 + 64, top, buttonWidth, buttonHeight);
        [self.closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
        [self.closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
        [self.closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.navigationController.navigationBar addSubview:self.closeBtn];
//    }
    
    if ([@"关于我们" isEqualToString:self.tit]) {
        self.closeBtn.hidden = YES;
        self.navigationItem.leftBarButtonItem = nil;
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self.closeBtn removeFromSuperview];
}

//关闭整个web
- (void)close{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    if (self.tit == nil || [self.tit isEqualToString:@""]) {
        self.title = @"新闻";
    }else {
        self.title = self.tit;
    }
    
    // 如果不是首页的有leftitem
    if (![@"关于我们" isEqualToString:self.tit]) {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:self.leftBtn];
    }
    
    if (self.canShare == 1) {
        @weakify(self);
        self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"share2" block:^(id sender) {
            [weak_self shareClick];
        }];
    }
    
    self.webV = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - 64)];
    [self.view addSubview:self.webV];
    self.webV.backgroundColor = [UIColor whiteColor];
    self.webV.delegate = self;
    
    NSMutableString *str = [NSMutableString string];
    for (NSHTTPCookie *cookie in NSHTTPCookieStorage.sharedHTTPCookieStorage.cookies) {
        [str appendString:[NSString stringWithFormat:@"%@=%@",cookie.name,cookie.value]];
    }
    self.webV.scalesPageToFit = YES;
    if ([self.type isEqualToString:@"1"]) {
        [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:NO];
        [self.webV loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.str]]];
    }else {
        NSString *headPath = [[NSBundle mainBundle] pathForResource:@"head" ofType:@"html"];
        NSString *footPath = [[NSBundle mainBundle] pathForResource:@"food" ofType:@"html"];
        NSString *strHead = [NSString stringWithContentsOfFile:headPath encoding:NSUTF8StringEncoding error:nil];
        NSString *strFoot = [NSString stringWithContentsOfFile:footPath encoding:NSUTF8StringEncoding error:nil];
        NSString *html = [NSString stringWithFormat:@"%@%@%@",strHead,self.str,strFoot];
        [self.webV loadHTMLString:html baseURL:nil];
    }
    
    @weakify(self);
    self.webV.scrollView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self);
        [self loadData];
    }];
}

- (void)loadData {
    
    if ([self.type isEqualToString:@"1"]) {
        
        [[NSURLCache sharedURLCache] removeCachedResponseForRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.str]]];
        
        [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:NO];
        [self.webV reload];
        [self.webV.scrollView.mj_header endRefreshing];
        
        
    }else {
        
        NSString *headPath = [[NSBundle mainBundle] pathForResource:@"head" ofType:@"html"];
        NSString *footPath = [[NSBundle mainBundle] pathForResource:@"food" ofType:@"html"];
        NSString *strHead = [NSString stringWithContentsOfFile:headPath encoding:NSUTF8StringEncoding error:nil];
        NSString *strFoot = [NSString stringWithContentsOfFile:footPath encoding:NSUTF8StringEncoding error:nil];
        NSString *html = [NSString stringWithFormat:@"%@%@%@",strHead,self.str,strFoot];
        [self.webV loadHTMLString:html baseURL:nil];
        
    }
    
}

- (UIButton*)leftBtn
{
    if (_leftBtn==nil) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _leftBtn.frame = CGRectMake(0, TRUEW(2), TRUEW(15), TRUEW(15));
        [_leftBtn setBackgroundImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [_leftBtn addTarget:self action:@selector(leftBtnClick) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftBtn;
}
- (void)leftBtnClick {
    
    if ([self.delegate respondsToSelector:@selector(getNewState:)]) {
        
        [DataRequest PostWithURL:[NSString stringWithFormat:@"%@nArticleId=%ld", GetSingleReadStatus, self.articleId] parameter:nil :^(id reponserObject) {
            
            id str = [reponserObject valueForKey:@"result"];
            NSInteger result = 0;
            if (![str isKindOfClass:[NSNull class]]) {
                result = [str integerValue];
            }
            
            [self.delegate getNewState:result];
            [self.navigationController popViewControllerAnimated:YES];
        }];
        
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (void)reloadWithUrl:(NSString *)url {
    [self.webV loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}



- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [MBProgressHUD hideHUD];
}

- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    
    [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:NO];
    [self.webV.scrollView.mj_header endRefreshing];
    
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.webV.scrollView.mj_header endRefreshing];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUD];
    /*
    //调整字号
    NSString *str = @"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '95%'";
    [webView stringByEvaluatingJavaScriptFromString:str];
    
    //js方法遍历图片添加点击事件 返回图片个数
    static  NSString * const jsGetImages =
    @"function getImages(){\
    var objs = document.getElementsByTagName(\"img\");\
    for(var i=0;i<objs.length;i++){\
    objs[i].onclick=function(){\
    document.location=\"myweb:imageClick:\"+this.src;\
    };\
    };\
    return objs.length;\
    };";
    
    //注入js方法
    [webView stringByEvaluatingJavaScriptFromString:jsGetImages];
    
    //注入自定义的js方法后别忘了调用 否则不会生效（不调用也一样生效了... 不明白）
    NSString *resurlt = [webView stringByEvaluatingJavaScriptFromString:@"getImages()"];
    //调用js方法
    DLog(@"---调用js方法--:%@,\n%s,\njsMehtods_result = %@", self.class, __func__, resurlt);
    */
    
    
    //这里是js，主要目的实现对url的获取
    static  NSString * const jsGetImages =
    @"function getImages() {\
    var objs = document.getElementsByTagName(\"img\");\
    var imgScr = '';\
    \
    for(var i = 0; i<objs.length; i++) {\
    imgScr = imgScr + objs[i].src + '+';\
    \
    objs[i].onclick=function() {\
    document.location=\"myweb:imageClick:\"+this.src;\
    };\
    \
    };\
    return imgScr;\
    };";
    
    [webView stringByEvaluatingJavaScriptFromString:jsGetImages];//注入js方法
    NSString *urlResult = [webView stringByEvaluatingJavaScriptFromString:@"getImages()"];
    NSArray *imgUrlArr = [NSMutableArray arrayWithArray:[urlResult componentsSeparatedByString:@"+"]];
    //urlResurlt 就是获取到得所有图片的url的拼接；imgUrlArr就是所有图片Url的数组
    DLog(@"imgUrlArr--:%@",imgUrlArr);
    
    
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    //将url转换为string
    NSString *requestString = [[request URL] absoluteString];
    
//    DLog(@"requestString is %@",requestString);
    
    //hasPrefix 判断创建的字符串内容是否以pic:字符开始
    if ([requestString hasPrefix:@"myweb:imageClick:"]) {
        //如果是购买图书不做放大处理
        if ([requestString hasPrefix:@"myweb:imageClick:http://djy.ewanyuan.cn/shopapp/"] ||
            [requestString hasPrefix:@"myweb:imageClick:https://djy.ewanyuan.cn/shopapp/"]) {
            return YES;
        }
        
        NSString *imageUrl = [requestString substringFromIndex:@"myweb:imageClick:".length];
        CGSize size = [UIImage getImageSizeWithURL:imageUrl];
        CGFloat height = (MAIN_SIZE.width - 10) *size.height /size.width;
        
        //创建视图并显示图片
        [self showBigImage:imageUrl andHeight:height];
        
        return NO;
    }
    // 支付宝
    
    NSString* reqUrl = request.URL.absoluteString;
    if ([reqUrl hasPrefix:@"alipays://"] || [reqUrl hasPrefix:@"alipay://"]) {
        // NOTE: 跳转支付宝App
        BOOL bSucc = [[UIApplication sharedApplication]openURL:request.URL];
        
        // NOTE: 如果跳转失败，则跳转itune下载支付宝App
        if (!bSucc) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"提示"
                                                           message:@"未检测到支付宝客户端，请安装后重试。"
                                                          delegate:self
                                                 cancelButtonTitle:@"立即安装"
                                                 otherButtonTitles:nil];
            [alert show];
        }
        return NO;
    }
    
    return YES;
    
}

- (void)didReceiveMemoryWarning {
    
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies]) {
        [storage deleteCookie:cookie];
    }
    
}

- (void)shareClick {
    //1、设置分享参数
    self.shareDescribe = self.shareDescribe == nil || self.shareDescribe.length == 0? @"分享":self.shareDescribe;
    self.shareImage = self.shareImage == nil? @[[UIImage imageNamed:@"dang"]]:self.shareImage;
    self.shareTitle = self.shareTitle == nil? @"":self.shareTitle;
    
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    [shareParams SSDKSetupShareParamsByText:self.shareDescribe
                                     images:self.shareImage
                                        url:[NSURL URLWithString:[self.str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                      title:self.shareTitle
                                       type:SSDKContentTypeAuto];
    
    [SSUIShareActionSheetStyle setActionSheetBackgroundColor:clear];
    [SSUIShareActionSheetStyle setActionSheetColor:RGB(230, 230, 230)];
    [SSUIShareActionSheetStyle setItemNameColor:[UIColor darkGrayColor]];
    [SSUIShareActionSheetStyle setItemNameFont:[UIFont systemFontOfSize:12]];
    [SSUIShareActionSheetStyle setShareActionSheetStyle:ShareActionSheetStyleSimple];
    //2、弹出ShareSDK分享菜单
    [ShareSDK showShareActionSheet:nil
                             items:@[@(SSDKPlatformTypeWechat)
//                                     ,
//                                     @(SSDKPlatformTypeQQ)
                                     ]
                       shareParams:shareParams
               onShareStateChanged:^(SSDKResponseState state, SSDKPlatformType platformType, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error, BOOL end) {
                   
                   switch (state) {
                       case SSDKResponseStateSuccess: {
                           [MBProgressHUD showMessage:@"分享成功" RemainTime:1.0 ToView:self.view userInteractionEnabled:NO];
                           break;
                       }
                       case SSDKResponseStateFail: {
                           [MBProgressHUD showMessage:@"分享失败" RemainTime:1.0 ToView:self.view userInteractionEnabled:NO];
                           break;
                       }
                       case SSDKResponseStateCancel: {
                           [MBProgressHUD showMessage:@"取消分享" RemainTime:1.0 ToView:self.view userInteractionEnabled:NO];
                           break;
                       }
                       default:
                           break;
                   }
                   
    }];
    

}


#pragma mark 显示大图片
-(void)showBigImage:(NSString *)imageUrl andHeight:(CGFloat)height {
    //创建灰色透明背景，使其背后内容不可操作
    self.bgBtnView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, MAIN_SIZE.width, MAIN_SIZE.height)];
    [self.bgBtnView setBackgroundColor:[UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:0.7]];
    [self.bgBtnView addTarget:self action:@selector(removeBigImage) forControlEvents:UIControlEventTouchUpInside];
    [[UIApplication sharedApplication].keyWindow addSubview:self.bgBtnView];

    //创建边框视图
    UIView *borderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAIN_SIZE.width, height + 10)];
    //将图层的边框设置为圆脚
    borderView.layer.cornerRadius = 8;
    borderView.layer.masksToBounds = YES;
    //给图层添加一个有色边框
    borderView.layer.borderWidth = 5;
    borderView.layer.borderColor = [[UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:0.7] CGColor];
    [borderView setCenter:self.bgBtnView.center];
    [self.bgBtnView addSubview:borderView];
    
    //创建显示图像视图
    self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, MAIN_SIZE.width - 10, height)];
    self.imgView.userInteractionEnabled = YES;
    [self.imgView sd_setImageWithURL:[NSURL URLWithString:imageUrl]];
    [borderView addSubview:self.imgView];
    
    //添加关闭视图手势
    [self.imgView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeBigImage)]];
    
    //添加捏合手势
    [self.imgView addGestureRecognizer:[[UIPinchGestureRecognizer alloc]initWithTarget:self action:@selector(handlePinch:)]];
    
    //添加保存图片到相册
    UIButton *saveImgBtn = [[UIButton alloc] initWithFrame:CGRectMake(MAIN_SIZE.width/2 - 20, CGRectGetMaxY(borderView.frame), 40, 40)];
    [saveImgBtn setTitle:@"保存" forState:UIControlStateNormal];
    [saveImgBtn addTarget:self action:@selector(saveImage) forControlEvents:UIControlEventTouchUpInside];
    [self.bgBtnView addSubview:saveImgBtn];
    
}

//关闭大图
-(void)removeBigImage {
    self.bgBtnView.hidden = YES;
}
//捏合手势
- (void) handlePinch:(UIPinchGestureRecognizer*) recognizer {
    //缩放:设置缩放比例
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
}
//保存图片
- (void)saveImage {
    UIImageWriteToSavedPhotosAlbum(self.imgView.image,self,@selector(imageSavedToPhotosAlbum:didFinishSavingWithError:contextInfo:),NULL); // 写入相册
    
}
-(void)imageSavedToPhotosAlbum:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.bgBtnView animated:YES];
    hud.mode = MBProgressHUDModeText;
    
    if(!error){
        hud.label.text = @"保存成功";
    }
    else{
        hud.label.text = @"保存失败";
    }
    
    hud.margin = 10.f;
    [hud setOffset:CGPointMake(0, 150.0f)];
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:3.0];
    
    
}
        
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // NOTE: 跳转itune下载支付宝App
    NSString* urlStr = @"https://itunes.apple.com/cn/app/zhi-fu-bao-qian-bao-yu-e-bao/id333206289?mt=8";
    NSURL *downloadUrl = [NSURL URLWithString:urlStr];
    [[UIApplication sharedApplication]openURL:downloadUrl];
}


@end

@implementation UIWebView (JavaScriptAlert)

- (void)webView:(UIWebView *)sender runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(id)frame {
    
    UIAlertView* customAlert = [[UIAlertView alloc] initWithTitle:@"提示信息" message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    
    [customAlert show];
    
}

@end

