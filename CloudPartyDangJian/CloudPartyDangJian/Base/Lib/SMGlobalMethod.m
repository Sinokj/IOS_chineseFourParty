//
//  SMGlobalMethod.m
//  Smios
//
//  Created by hao on 15/11/27.
//  Copyright (c) 2015年 sanmi. All rights reserved.
//

#import "SMGlobalMethod.h"
#import <UIKit/UIKit.h>

@implementation SMGlobalMethod
//位于中间自动消失的提示框
+ (void)showMiddleMessage:(NSString *)message{
    UIWindow * window = [UIApplication sharedApplication].keyWindow;
    
    UIView *showview =  [[UIView alloc]init];
    showview.backgroundColor = [UIColor blackColor];
    showview.alpha = 1.0f;
    showview.layer.cornerRadius = 5.0f;
    showview.layer.masksToBounds = YES;
    [window addSubview:showview];
    showview.sd_layout
    .centerXEqualToView(window)
    .centerYEqualToView(window);
    
    UILabel *label = [[UILabel alloc]init];
    label.text = message;
    label.textColor = [UIColor whiteColor];
    label.textAlignment = 1;
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont boldSystemFontOfSize:15];
    [showview addSubview:label];
    label.sd_layout
    .leftSpaceToView(showview,20)
    .topSpaceToView(showview,10)
    .heightIs(15);
    [label setSingleLineAutoResizeWithMaxWidth:ScreenWidth * 0.7];
    
    
    [showview setupAutoWidthWithRightView:label rightMargin:20];
    [showview setupAutoHeightWithBottomView:label bottomMargin:10];
    
    [UIView animateWithDuration:1.5 animations:^{
        showview.alpha = 0;
    } completion:^(BOOL finished) {
        [showview removeFromSuperview];
    }];
}


@end
