//
//  DECommenView.h
//  Electric2
//
//  Created by ailin on 2017/6/21.
//  Copyright © 2017年 北京电小二网络科技有限公司. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DECommenAlert : UIView

+ (void)showAlertWithContent:(NSString *)content leftBtn:(NSString *)leftTitle rightBtn:(NSString *)rightTitle tapEvent:(void (^)(NSInteger index))event;
+ (void)showAlertWithContent:(NSString *)content middelBtn:(NSString *)middleBtn tapEvent:(void (^)(NSInteger index))event;
+ (void)dismiss;
@end
