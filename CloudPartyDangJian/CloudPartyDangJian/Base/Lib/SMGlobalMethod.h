//
//  SMGlobalMethod.h
//  Smios
//
//  Created by hao on 15/11/27.
//  Copyright (c) 2015年 sanmi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SMGlobalMethod : NSObject
//位于中间自动消失的提示框
+(void)showMiddleMessage:(NSString *)message;

@end
