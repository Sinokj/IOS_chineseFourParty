//
//  UIView+Extension.m
//  JYB
//
//  Created by hao on 16/8/26.
//  Copyright © 2016年 sanmiao. All rights reserved.
//

#import "UIView+Extension.h"

@implementation UIView (LKExtension)

- (UIViewController *)curVc{
    for (UIView *nextV = self; nextV; nextV = nextV.superview) {
        UIResponder *responder = [nextV nextResponder];
        if (responder && [responder isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)responder;
        }
    }
    return nil;
}

@end

#pragma mark - UIView
@implementation UIView(Extension)
+ (UIView *)viewWithBackgroundColor:(UIColor *)color{
    UIView *view = [UIView new];
    if (color != nil) {
        view.backgroundColor = color;
    }
    return view;
}
@end

#pragma mark - label
@implementation UILabel(Extension)
+ (UILabel *)labelWithTextColor:(UIColor *)textColor font:(CGFloat)textFont backgroundColor:(UIColor *)backColor Text:(NSString *)text 
{
    UILabel *label = [[UILabel alloc]init];
    //文字
    label.text = text;
    //文字颜色
    label.textColor = textColor;
    //字体
    label.font = [UIFont systemFontOfSize:textFont];
    //背景色
    if (backColor != nil) {
        label.backgroundColor = backColor;
    }
    return label;
}

+ (void)changeLineSpaceForLabel:(UILabel *)label WithSpace:(float)space {
    
    NSString *labelText = label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:space];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    label.attributedText = attributedString;
    [label sizeToFit];
    
}

+ (void)changeWordSpaceForLabel:(UILabel *)label WithSpace:(float)space {
    
    NSString *labelText = label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(space)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    label.attributedText = attributedString;
    [label sizeToFit];
    
}

+ (void)changeSpaceForLabel:(UILabel *)label withLineSpace:(float)lineSpace WordSpace:(float)wordSpace {
    
    NSString *labelText = label.text;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText attributes:@{NSKernAttributeName:@(wordSpace)}];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpace];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    label.attributedText = attributedString;
    [label sizeToFit];
}

@end

@implementation UIImageView(Extension)
+ (UIImageView *)alImageViewWithImageName:(NSString *)imageName{
    UIImageView *imageView = [UIImageView new];
    if (imageName != nil) {
        imageView.image = [UIImage imageNamed:imageName];
    }
    return imageView;
}
@end

#pragma mark - button

@implementation UIButton(Extension)

+ (UIButton *)buttonWithTextColor:(UIColor *)textColor font:(CGFloat)textFont backgroudColor:(UIColor *)backColor text:(NSString *)text{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:text forState:UIControlStateNormal];
    if (backColor != nil) {
        button.backgroundColor = backColor;
    }
    button.titleLabel.font = [UIFont systemFontOfSize:textFont];
    if (textColor != nil) {
        [button setTitleColor:textColor forState:UIControlStateNormal];
    }
    return button;
}

+ (UIButton *)buttonWithTextColor:(UIColor *)textColor font:(CGFloat)textFont backgroudColor:(UIColor *)backColor image:(UIImage *)image text:(NSString *)text{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:text forState:UIControlStateNormal];
    if (backColor != nil) {
        button.backgroundColor = backColor;
    }
    button.titleLabel.font = [UIFont systemFontOfSize:textFont];
    if (textColor != nil) {
        [button setTitleColor:textColor forState:UIControlStateNormal];
    }
    [button setImage:image forState:UIControlStateNormal];
    
    return button;
}

/**
 *  创建一个button(只是图片)
 */
+ (UIButton *)buttonWithImageName:(NSString *)imageName{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    if (imageName != nil || ![imageName isEqualToString:@""]) {
        [button setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    }
    return button;
}

- (void)setImagePosition:(ImagePosition)postion spacing:(CGFloat)spacing
{
    [self setTitle:self.currentTitle forState:UIControlStateNormal];
    [self setImage:self.currentImage forState:UIControlStateNormal];
    
    CGFloat imageWidth = self.imageView.image.size.width;
    CGFloat imageHeight = self.imageView.image.size.height;
    CGFloat labelWidth = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].width;
    
    if (postion == ImagePositionRight && labelWidth >= self.frame.size.width - imageWidth) {
        labelWidth = self.frame.size.width - imageWidth;
    }
    
    CGFloat labelHeight = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}].height;
    
    CGFloat imageOffsetX = (imageWidth + labelWidth) / 2 - imageWidth / 2; //image中心移动的x距离
    CGFloat imageOffsetY = imageHeight / 2 + spacing / 2; //image中心移动的y距离
    CGFloat labelOffsetX = (imageWidth + labelWidth / 2) - (imageWidth + labelWidth) / 2; //label中心移动的x距离
    CGFloat labelOffsetY = labelHeight / 2 + spacing / 2; //label中心移动的y距离
    
    CGFloat tempWidth = MAX(labelWidth, imageWidth);
    CGFloat changedWidth = labelWidth + imageWidth - tempWidth;
    CGFloat tempHeight = MAX(labelHeight, imageHeight);
    CGFloat changedHeight = labelHeight + imageHeight + spacing - tempHeight;
    
    switch (postion) {
        case ImagePositionLeft:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, -spacing / 2, 0, spacing / 2);
            self.titleEdgeInsets = UIEdgeInsetsMake(0, spacing / 2, 0, -spacing / 2);
            self.contentEdgeInsets = UIEdgeInsetsMake(0, spacing / 2, 0, spacing / 2);
            break;
            
        case ImagePositionRight:
            self.imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth + spacing / 2, 0, -(labelWidth + spacing / 2));
            self.titleEdgeInsets = UIEdgeInsetsMake(0, -(imageWidth + spacing / 2), 0, imageWidth + spacing / 2);
            self.contentEdgeInsets = UIEdgeInsetsMake(0, spacing / 2, 0, spacing / 2);
            break;
            
        case ImagePositionTop:
            self.imageEdgeInsets = UIEdgeInsetsMake(-imageOffsetY, imageOffsetX, imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(labelOffsetY, -labelOffsetX, -labelOffsetY, labelOffsetX);
            self.contentEdgeInsets = UIEdgeInsetsMake(imageOffsetY, -changedWidth / 2, changedHeight - imageOffsetY, -changedWidth / 2);
            break;
            
        case ImagePositionBottom:
            self.imageEdgeInsets = UIEdgeInsetsMake(imageOffsetY, imageOffsetX, -imageOffsetY, -imageOffsetX);
            self.titleEdgeInsets = UIEdgeInsetsMake(-labelOffsetY, -labelOffsetX, labelOffsetY, labelOffsetX);
            self.contentEdgeInsets = UIEdgeInsetsMake(changedHeight - imageOffsetY, -changedWidth / 2, imageOffsetY, -changedWidth / 2);
            break;
            
        default:
            break;
    }
}

@end


