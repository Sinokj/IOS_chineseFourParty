//
//  WebViewController.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "ViewController.h"

@protocol WebViewDelegate <NSObject>

- (void)getNewState:(NSInteger)state;

@end

@interface WebViewController : ViewController
@property (nonatomic, strong) NSString *type;
@property (nonatomic ,strong) NSString *str;

@property (nonatomic, strong) NSString *tit;
@property (nonatomic, assign) NSInteger nID;
@property (nonatomic, strong)UIWebView *webV;
@property (nonatomic, strong) NSArray *shareImage;
@property (nonatomic, strong) NSString *shareTitle;
@property (nonatomic, strong) NSString *shareDescribe;

@property (nonatomic, assign) NSInteger canShare;

@property (nonatomic, assign) id<WebViewDelegate> delegate;

@property (nonatomic, assign) NSInteger articleId;

@property (nonatomic, strong) NSString *vcType;

- (void)reloadWithUrl:(NSString *)url;
@end

