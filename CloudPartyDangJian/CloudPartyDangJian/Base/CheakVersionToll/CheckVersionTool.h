//
//  CheckVersionTool.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/19.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CheckVersionTool : NSObject
+ (void)checkVersionToUpdate;
@end

NS_ASSUME_NONNULL_END
