//
//  CheckVersionTool.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/11/19.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "CheckVersionTool.h"
#import "VersionModel.h"
#define CURRENT_VERSION [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

@implementation CheckVersionTool
+ (void)checkVersionToUpdate{
    
    //获取最新版本
    [DataRequest PostWithURL:UpdateByIos parameter:nil :^(id reponserObject) {
        VersionModel *model = [VersionModel mj_objectWithKeyValues:reponserObject];
        NSString *currentVersion = model.vcVersion;
        NSString *content = (model.vcContent == nil || model.vcContent.length == 0) ? @"" : model.vcContent;
        if ([self isEqualByCompareLastVersion:CURRENT_VERSION withCurrentVersion:currentVersion]) {
            [self showUpdateWithTitle:@"发现新版本" content:content];
        }
    }];
}

+ (void)showUpdateWithTitle:(NSString *)title content:(NSString *)content
{

    __weak typeof(self) weakSelf = self;
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:content preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {

    }];
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:@"更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [weakSelf openAPPStore];
    }];
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    
    //修改messege对齐方式
    UIView *subView1 = alertController.view.subviews[0];
    UIView *subView2 = subView1.subviews[0];
    UIView *subView3 = subView2.subviews[0];
    UIView *subView4 = subView3.subviews[0];
    UIView *subView5 = subView4.subviews[0];
    //设置内容的对齐方式
    UILabel *mes= subView5.subviews[2];
    mes.textAlignment = NSTextAlignmentLeft;
    
    [[UIApplication sharedApplication].keyWindow.rootViewController presentViewController:alertController animated:YES completion:^{
        
    }];
}


+ (BOOL)isEqualByCompareLastVersion:(NSString *)lastVersion withCurrentVersion:(NSString *)currentVersion {
    NSMutableArray *lastVersionArray = [[lastVersion componentsSeparatedByString:@"."] mutableCopy];
    NSMutableArray *currentVersionArray = [[currentVersion componentsSeparatedByString:@"."] mutableCopy];
    int modifyCount = abs((int)(lastVersionArray.count - currentVersionArray.count));
    if (lastVersionArray.count > currentVersionArray.count) {
        for (int index = 0; index < modifyCount; index ++) {
            [currentVersionArray addObject:@"0"];
        }
    } else if (lastVersionArray.count < currentVersionArray.count) {
        for (int index = 0; index < modifyCount; index ++) {
            [lastVersionArray addObject:@"0"];
        }
    }
    for (int index = 0; index < lastVersionArray.count; index++) {
        if ([currentVersionArray[index] integerValue] > [lastVersionArray[index] integerValue]) {
            return YES;
        } else if ([currentVersionArray[index] integerValue] < [lastVersionArray[index] integerValue]) {
            return NO;
        }
    }
    return NO;
}

+ (void)openAPPStore {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://itunes.apple.com/cn/app/id1450377689?mt=8"]];
}
@end
