//
//  UIBarButtonItem+STExtension.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "UIBarButtonItem+STExtension.h"
#import "UIButton+EnlargeEdge.h"
@implementation UIBarButtonItem (STExtension)
+ (instancetype)barButtonItemWithImageName:(NSString *)imageName block:(void (^)(id))block {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    
    [btn bk_addEventHandler:^(id sender) {
        if (block) {
            block(sender);
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if ([imageName isEqualToString:@"home_search2"] || [imageName isEqualToString:@"share2"] || [imageName isEqualToString:@"back"]) {
            make.width.height.mas_equalTo(MYDIMESCALEW(20));
        }else {
            make.width.height.mas_equalTo(MYDIMESCALEW(22));
        }
    }];
    
    [btn setEnlargeEdge:50];
    
    
    UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return customItem;
}

+ (instancetype)barButtonItemWithImageName:(NSString *)imageName title:(NSString *)title block:(void(^)(id sender))block{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn setTitle:@"返回" forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:16];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.contentEdgeInsets = UIEdgeInsetsMake(0, -25,0, 0);
    
    [btn bk_addEventHandler:^(id sender) {
        if (block) {
            block(sender);
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        if ([imageName isEqualToString:@"home_search2"] || [imageName isEqualToString:@"share2"] || [imageName isEqualToString:@"back"]) {
            make.width.height.mas_equalTo(MYDIMESCALEW(64));
        }else {
            make.width.height.mas_equalTo(MYDIMESCALEW(24));
        }
    }];
    
    [btn setEnlargeEdge:50];
    
    
    UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return customItem;
}

+ (instancetype)barButtonItemWithTitle:(NSString *)title block:(void (^)(id))block {
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:title forState:UIControlStateNormal];
    
    [btn bk_addEventHandler:^(id sender) {
        if (block) {
            block(sender);
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    [btn sizeToFit];
    
//    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
//
//        make.width.height.mas_equalTo(MYDIMESCALEW(22));
//
//    }];
    
    UIBarButtonItem *customItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return customItem;
}

@end
