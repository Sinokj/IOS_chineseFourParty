//
//  STUtils.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "STRunLoopTaskManager.h"
#import <Masonry.h>
/**
 工具类
 */
@interface STUtils : NSObject

/**
 图片选择

 @param vc 当前vc
 @param imageCount 图片数量
 @param finishBlock 选择完毕回调 photos为选择的图片数组
 */
+ (void)imagePickerWithVc:(UIViewController *)vc
           maxImagesCount:(NSInteger)imageCount
       finishPickingBlock:(void(^)(NSArray<UIImage *> *photos))finishBlock;


/**
 设置view的圆角背景色边框属性

 @param view view
 @param radius 圆角
 @param bgColor 背景色
 @param borderW 边框宽
 @param borderColor 边框颜色
 */
+ (void)setupView:(UIView *)view
    cornerRadius:(CGFloat)radius
              bgColor:(UIColor *)bgColor
              borderW:(CGFloat)borderW
          borderColor:(UIColor *)borderColor;

/**
 获取偏好设置的值

 @param key key
 @return value
 */
+ (id)objectForKey: (NSString *)key;

/**
 设置偏好值

 @param object value
 @param key key
 */
+ (void)setObject:(id)object
           forKey:(NSString *)key;

/**
 记录是否登录

 @return 返回登录状态
 */
+ (BOOL)isLogin;


//cookie管理
+ (void)saveCookieWithUrl:(NSString *)url;
+ (void)setupCookie;
+ (void)clearCookie;


@end
