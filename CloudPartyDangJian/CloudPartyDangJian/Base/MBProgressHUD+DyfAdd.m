//
//  MBProgressHUD+DyfAdd.m
//  SinoCommunity
//
//  Created by df on 2017/2/14.
//  Copyright © 2017年 df. All rights reserved.
//

#import "MBProgressHUD+DyfAdd.h"

@implementation MBProgressHUD (DyfAdd)

+ (void)showMessage:(NSString *)message ToView:(UIView *)view userInteractionEnabled:(BOOL)enable {
    
    [self hideHUD];
    
    [self showMessage:message ToView:view RemainTime:0 Model:(MBProgressHUDModeText) userInteractionEnabled:enable];
}

+ (void)showMessage:(NSString *)message RemainTime:(CGFloat)time ToView:(UIView *)view userInteractionEnabled:(BOOL)enable {
    
    [self hideHUD];
    
    [self showMessage:message ToView:view RemainTime:time Model:(MBProgressHUDModeText) userInteractionEnabled:enable];

}

+ (void)showProgressMessage:(NSString *)message RemainTime:(CGFloat)time ToView:(UIView *)view userInteractionEnabled:(BOOL)enable {
    
    [self hideHUD];
    
    [self showMessage:message ToView:view RemainTime:time Model:(MBProgressHUDModeIndeterminate) userInteractionEnabled:enable];

}

+(void)showMessage:(NSString *)message ToView:(UIView *)view RemainTime:(CGFloat)time Model:(MBProgressHUDMode)model userInteractionEnabled:(BOOL)enable {
    
    if (view == nil) {
        
        view =  (UIView *)[[UIApplication sharedApplication] keyWindow];
    }

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    
//    hud.label.text = message;
    hud.detailsLabel.text = message;


    hud.mode = model;
    
    hud.userInteractionEnabled = enable;
    
    hud.bezelView.color = [UIColor blackColor];
    
    hud.contentColor = [UIColor whiteColor];

    hud.removeFromSuperViewOnHide = YES;
    
    [view bringSubviewToFront:hud];
    
//    hud.square = YES;
    
    if (time != 0) {
        
        [hud hideAnimated:YES afterDelay:time];
        
    }else {
        
        [hud hideAnimated:YES afterDelay:15];
    }
    
//    if (!enable) {
//        
//        UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
//        
//        backView.backgroundColor = [UIColor whiteColor];
//        
//        backView.alpha = 0;
//        
//        backView.userInteractionEnabled = enable;
//        
//        [view addSubview:backView];
//        
//    }
}

+ (void)hideHUDForView:(UIView *)view {
    
    if (view == nil) {
        
        view =  (UIView *)[[UIApplication sharedApplication] keyWindow];
    }

    [self hideHUDForView:view animated:YES];
    
}

+ (void)hideHUD {
    
    [self hideHUDForView:nil];
}

@end
