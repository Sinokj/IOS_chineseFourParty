//
//  HeaderUrl.h
//  policePartyAppConstruction
//
//  Created by SINOKJ on 16/7/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//
 
#ifndef HeaderUrl_h
#define HeaderUrl_h

/*   New URL   */

//分享相关号码
//Mob
#define shareMobAppKey @"25956da7cd005"
#define shareMobAppSecret @"17e9948f93e68f622db594b34a152c27"
//微信
#define shareWXAppID @"wx69478824935366a1"
#define shareWXAppSecret @"ed6025fc746137f940eb137520db7c26"
//腾讯
#define shareTencentAppID @"1106903912APP"
#define shareTencentAppSecret @"QbIxzwNKsK1OAlrS"


//主URL



//#define BaseUrl @"http://192.168.1.232:8318/cloudPartyAppStandard"
//#define BaseUrl @"http://www.sinokj.cn:8318/cloudPartyAppStandard"
#define BaseUrl @"https://sw.ewanyuan.cn/cloudPartyAppStandard"



//是否有权限查看模块
#define getVerfityModule @"module/getVerfityModule.do"
//主页轮播和九宫格
#define HomeURL @"module/getHomeData.do"
//版本更新
#define UpdateByIos @"updateByIos.do"
//党委列表
#define DangWeiURL @"module/getCommitteeList.do"
//登陆
#define Login @"loginSecond.do"
//退出登陆
#define LoginOut @"logout.do"

//文章
#define ArticleURL @"article/getArticleList.do"
//获取单条文章阅读状态
#define GetSingleReadStatus @"article/getSingleReadStatus.do?"

//文章列表顶部标签（选择按钮）
#define TopMarkURL @"article/getArticleTagList.do?"

//意见征集列表
#define SuggestList @"getOpinionTopics.do?"
//意见征集内容提交
#define SubmitSuggest @"submitOpinion.do?"
//意见征集
//#define TopicsList @"getTopicsList.do"
//发表意见
//#define SuggestSend @"submitOpinion.do"


//绑定推送
#define BangDingPush @"push/savePushId.do"
//极光AppKey，Secret
#define JiGuangKey @"d4877081aed0876a675d4bb5"
#define JiGuangSecret @"c6a914be451cb5969e13c112"


//三会一课标签
#define SanHuiYiKeMark @"partyarticle/getPartyTypeList.do?"
//三会一课列表
#define SanHuiYiKeList @"meet/getMeetingList.do?"
//三会一课详情
#define SanHuiYiKeContent @"partyarticle/getPartyArticleContent.do?"
//三会一课请假
#define SanHuiYiKeLeave @"meet/confirmLeave.do?"
//三会一课签到
#define SanHuiYiKeSignIn @"meet/signIn.do?"
//三会一课获取附件
#define SanHuiYiKeAttach @"meet/getMeetingAttach.do?"
//三会一课参会
#define SanHuiYiKeConfirmMeeting @"meet/confirmMeeting.do?"
//我的消息列表
#define GetMessagerList @"messager/getMessagerList.do?"
//消息详情(请假)
#define AuditLeave @"meet/auditLeave.do?"
//消息详情(审核)
#define HandleAudit @"audit/handleAudit.do?"
//消息是否同意请假
#define auditLeaveResult @"meet/auditLeaveResult.do?"
//消息是否同意审核
#define resultAudit @"audit/resultAudit.do?"


/*   Old URL   */

//#define HostUrl @"http://192.168.1.7/policePartyApp"
//#define HostUrl @"http://192.168.1.13:80/policePartyApp"
#define HostUrl @"http://sw.ewanyuan.cn/policePartyApp"
//#define HostUrl @"http://218.244.60.138/policePartyApp"
//#define HostUrl @"http://123.57.84.199:8011/policePartyApp"
#define AppKey @"ee28371095c69e7c6d4f8ba0"
#define AppSecret @"d65ddd010e9542e5daa28810"
// 123.57.84.199:8080


// 轮播图
#define Viwepager @"getImgs.do"
//首页红点
#define GetUnReadList @"getUnReadList.do"
#define DANGFei       @"getMyPartyLog.do"
//图书币余额
#define GetMyTokenMoney       @"getMyTokenMoney.do"
//图书记录
#define GetMyTokenPayLog       @"getMyTokenPayLog.do"

//新闻
#define News @"getNews.do"
//三会一课
#define PartyArticleList @"getPartyArticleList.do"
#define PartyArticleListByType @"getPartyArticleListByType.do"
#define PartyTypeList @"getPartyTypeList.do"
//考试列表
#define ExamList @"getTopics.do"
//获取试题
#define ExamsQuestions @"getTopicDetail.do"
//开始考试
#define BeginExam @"beginAnswer.do"
//继续考试 
#define ContinueExam @"getAnswerd.do"
//提交单个习题
#define UpdateAnswer @"updateAnswer.do"

//提交试卷
#define SubmitURL @"submitTopics.do"
//投票
#define VoteListURL @"getVoteTopics.do"


//提交投票信息
#define SendVote @"submitVote.do"
//评议列表
#define ReviewedList @"getDiscussionsTopics.do"
//提交评议
#define SendReviewed @"submitGread.do"
//我的
#define MineInfo @"getAllMyInfo.do"
//修改密码
#define ResetPassWord @"modiPsw.do"

//忘记密码
#define FogetPw @"forgotPswNew.do"
//发送验证码
#define SendSMS @"sendSMSNew.do"
//上传头像
#define UpLoadIco @"updateHeadImg.do"
//绑定推送
#define BindPush @"bindPushInfo.do"


//交费
#define PartyAliPayCheck @"partyAliPayCheck.do?"
//微信缴费
#define WXPay @"wxPay.do?"
//查询缴费结果接口
#define WXGetPayResult @"wxGetPayResult.do?"

//上稿明细汇总
#define GetArticlePublishStatics @"ArticlePublish/getArticlePublishStatics.do?"
// 我的党费
#define getMyPartyLog @"getMyPartyLog.do"
// 搜索文章
#define searchNews @"article/searchNews.do?"
//民主评议
#define democryacySuggest @"getDiscussionsTopics.do"
//问卷调查
#define questionSurvey @"getTopicsList.do?nPageNo=1&nPageSize=10"
//问卷调查网页
#define questionSurveyWeb @"getTopics_Content.do"
//在线考试
#define examinationOnline @"getTopics.do"





// two add
// 关于我们
#define aboutUs     @"about.do?nCommitteeId="
// 启动图
#define StartPhoto  @"startphoto.do"

#endif /* HeaderUrl_h */
