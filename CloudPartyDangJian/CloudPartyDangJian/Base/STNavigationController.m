//
//  STNavigationController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/14.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import "STNavigationController.h"

@interface STNavigationController ()

@end

@implementation STNavigationController

- (instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    if (self = [super initWithRootViewController:rootViewController]) {
        
        [self.navigationBar setBackgroundImage:[UIImage imageWithColor: AppTintColor] forBarMetrics:(UIBarMetricsDefault)];
        self.navigationBar.translucent = NO;
    
        
    }
    return self;
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.viewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
        @weakify(viewController);
        UIBarButtonItem *leftItem = [UIBarButtonItem barButtonItemWithImageName:@"back" block:^(id sender) {
            [weak_viewController.navigationController popViewControllerAnimated:YES];
            [weak_viewController dismissViewControllerAnimated:YES completion:nil];
        }];
        viewController.navigationItem.leftBarButtonItem = leftItem;
        
    }
    [super pushViewController:viewController animated:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return self.visibleViewController.preferredStatusBarStyle;
}

@end
