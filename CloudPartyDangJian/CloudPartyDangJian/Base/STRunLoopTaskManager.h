//
//  STRunLoopTaskManager.h
//  STRunLoopTaskManager
//
//  Created by ZJXN on 2018/5/18.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^TaskBlock)(void);


/**
 runloop任务管理工具类
 */
@interface STRunLoopTaskManager : NSObject

@property (nonatomic, copy, readonly) STRunLoopTaskManager * (^addTask)(TaskBlock taskBlock);


/**
 创建RunLoop任务管理对象

 @return 返回RunLoop任务管理对象
 */
+ (STRunLoopTaskManager *)sharedRunLoopTaskManager;

@end
