//
//  DataRequest.m
//  SinoMT
//
//  Created by SINOKJ on 16/6/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import "DataRequest.h"
#import <AFNetworking.h>
#import "HeaderUrl.h"
#import "MBProgressHUD+DyfAdd.h"
#import "LoginController.h"
#import "AppDelegate.h"


#define SharedApplication  [UIApplication sharedApplication]

@implementation DataRequest

//+ (NSMutableDictionary *) getDeviceInfo
//{
//    UIDevice* curDev = [UIDevice currentDevice];
//    NSString *identifierForVendor = [[UIDevice currentDevice].identifierForVendor UUIDString];
//    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
//    [dic setObject:curDev.name forKey:@"currentDeviceName"];
//    [dic setObject:curDev.systemName forKey:@"currentDeviceSystemName"];
//    [dic setObject:curDev.systemVersion forKey:@"currentDeviceSystemVersion"];
//    [dic setObject:identifierForVendor forKey:@"currentDeviceIdentifier"];
//    return dic;
//    
//}
+(BOOL)IsOutTime:(id)responseObject andIsHome:(BOOL)isHome {
    NSString* str = [NSString stringWithFormat:@"%@",responseObject];
    if (str.length>30) {
        NSString* str2 = [str substringWithRange:NSMakeRange(15, 14)];
        // 注意

        if ([@"sessionTimeOut" isEqualToString:str2] ) {
            
            [STUtils setObject:@0 forKey:KIsLogin];
            
            [STUtils setObject:@2 forKey:KDangJianType];
            UIWindow *aud = [UIApplication sharedApplication].keyWindow;
            LoginController* logonVC = [[LoginController alloc]init];
            logonVC.isTrue = isHome;
            logonVC.hidesBottomBarWhenPushed = YES;
            UITabBarController *tab = (UITabBarController *)aud.rootViewController;
            UINavigationController* vc = tab.selectedViewController;
            [vc pushViewController:logonVC animated:YES];
            return NO;
        }
    }
    return YES;
}

#pragma - mark - 公共请求接口 GET
#pragma - mark - 公共请求接口 GET 验证是否登陆
+(void)GetAgainWithUrl:(NSString*)url andIsHome:(BOOL)isHome title:(NSString*)title :(void(^)(id responseObject))block {
    
    [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:YES];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [MBProgressHUD hideHUD];
        
        if (block)
        {
            if ([DataRequest IsOutTime:responseObject andIsHome:isHome]) {
                block(responseObject);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        
        [MBProgressHUD hideHUD];
//        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"提示" message:@"请求失败,请重试" delegate:nil  cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
    }];
//
}

#pragma - mark - 公共请求接口 POST 验证是否登陆
+(void)PostAgainWithUrl:(NSString*)url andIsHome:(BOOL)isHome dict:(NSDictionary*)dic :(void(^)(id responseObject))block {

    [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:YES];
    
//    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//    [parameter setValuesForKeysWithDictionary:dic];
//    [parameter setValuesForKeysWithDictionary:[self getDeviceInfo]];
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:url parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [MBProgressHUD hideHUD];

        if (block)
        {
            
            if ([DataRequest IsOutTime:responseObject andIsHome:isHome]) {
//
                block(responseObject);
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {

        [MBProgressHUD hideHUD];
        
//        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"提示" message:@"请求失败,请重试" delegate:nil  cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
    }];

}

+(void)CatlikeGetWithURL:(NSString *)Url :(void(^)(id reponserObject))block {
    NSString* str = [NSString stringWithFormat:@"%@/%@",BaseUrl,Url];
    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    
    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        if (block)
        {
            if ([DataRequest IsOutTime:responseObject andIsHome:NO]) {
                block(responseObject);
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        [MBProgressHUD hideHUD];
//        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"提示" message:@"请求失败,请重试" delegate:nil  cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
    }];
    
}

+(void)GetWithURL:(NSString *)Url :(void(^)(id reponserObject))block {
    NSString* str = [NSString stringWithFormat:@"%@/%@",BaseUrl,Url];
    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GetAgainWithUrl:url andIsHome:NO title:@"" :^(id responseObject) {
        if (block)
        {
            block(responseObject);
        }
    }];
}

+(void)NoLoginGetWithURL:(NSString *)Url :(void(^)(id reponserObject))block {
    
    NSString* str = [NSString stringWithFormat:@"%@/%@",BaseUrl,Url];
    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    [MBProgressHUD showProgressMessage:@"正在加载..." RemainTime:0 ToView:nil userInteractionEnabled:YES];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    //    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    //    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    //    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [MBProgressHUD hideHUD];
        
        if (block)  {
            block(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [MBProgressHUD hideHUD];
        
//        UIAlertView *alertView=[[UIAlertView alloc]initWithTitle:@"提示" message:@"请求失败,请重试" delegate:nil  cancelButtonTitle:@"确定" otherButtonTitles: nil];
//        [alertView show];
    }];

}


+(void)GetWithURL:(NSString *)Url andIsHome:(BOOL)isHome :(void(^)(id reponserObject))block {
    NSString* str = [NSString stringWithFormat:@"%@/%@",BaseUrl,Url];
    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    [self GetAgainWithUrl:url andIsHome:isHome title:@"" :^(id responseObject) {
        if (block)
        {
            block(responseObject);
        }
    }];
}


+(void)PostWithURL:(NSString *)URL parameter:(NSDictionary*)dic :(void(^)(id reponserObject))block {
    NSString* str = [NSString stringWithFormat:@"%@/%@",BaseUrl,URL];
//    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *url = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
//    NSString *url = [str stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [self PostAgainWithUrl:url andIsHome:NO dict:dic :^(id responseObject) {
        if (block) {
            block(responseObject);
        }
    }];
    
}
+(void)PostWithURL:(NSString *)URL parameter:(NSDictionary*)dic andImage:(UIImage *)image :(void (^)(id responseObject))block failure:(void(^)(NSError *error))failure {
    NSString* str = [NSString stringWithFormat:@"%@/%@",BaseUrl,URL];
    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
//    NSMutableDictionary *parameter = [NSMutableDictionary dictionary];
//    [parameter setValuesForKeysWithDictionary:dic];
//    [parameter setValuesForKeysWithDictionary:[self getDeviceInfo]];
    //1。创建管理者对象
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager POST:url parameters:dic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        if(image != nil) {
            
            [formData appendPartWithFileData:UIImageJPEGRepresentation(image, 1.0) name:@"image" fileName:@".png" mimeType:@"image/png"];
            
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
        //打印下上传进度
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //请求成功
        block(responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        //请求失败
        failure(error);
    }];
}

+(void)GetAppTest :(void(^)(id reponserObject))block {
    
    NSString* str = @"https://itunes.apple.com/lookup?id=1141303625";//(appId号)//
    NSString* url = [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.requestSerializer= [AFHTTPRequestSerializer serializer];
    manager.responseSerializer= [AFHTTPResponseSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        [MBProgressHUD hideHUD];
        if (block)
        {
            block(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];

}

@end
