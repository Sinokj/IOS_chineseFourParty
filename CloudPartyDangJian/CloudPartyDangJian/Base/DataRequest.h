//
//  DataRequest.h
//  SinoMT
//
//  Created by SINOKJ on 16/6/20.
//  Copyright © 2016年 Dyf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DataRequest : NSObject

+(void)GetWithURL:(NSString *)Url :(void(^)(id reponserObject))block;
+(void)CatlikeGetWithURL:(NSString *)Url :(void(^)(id reponserObject))block;
+(void)NoLoginGetWithURL:(NSString *)Url :(void(^)(id reponserObject))block;

+(void)GetWithURL:(NSString *)Url andIsHome:(BOOL)isHome :(void(^)(id reponserObject))block;

+(void)PostWithURL:(NSString *)URL parameter:(NSDictionary*)dic :(void(^)(id reponserObject))block;

+(void)PostWithURL:(NSString *)URL parameter:(NSDictionary*)dic andImage:(UIImage *)image :(void (^)(id responseObject))block failure:(void(^)(NSError *error))failure;

+(void)GetAppTest :(void(^)(id reponserObject))block;

@end
