//
//  NSObject+rootVcExtension.h
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/18.
//  Copyright © 2018年 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TabbarController.h"
#import "HomeController.h"
#import "BulletinController.h"
#import "STMineViewController.h"
#import "WebViewController.h"

/**
 获取底部的控制器
 */
@interface NSObject (rootVcExtension)
@property (nonatomic, strong, readonly) TabbarController *tabBarVc;
@property (nonatomic, strong, readonly) HomeController *homeVc;
@property (nonatomic, strong, readonly) BulletinController *bullVc;
@property (nonatomic, strong, readonly) STMineViewController *mineVc;
@property (nonatomic, strong, readonly) WebViewController *webVc;
@end
