//
//  AboutUsController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/5/3.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "AboutUsController.h"
#import "WebViewController.h"
#import <WebKit/WebKit.h>
@interface AboutUsController () <WKNavigationDelegate>
@property (nonatomic, strong)WKWebView *webView;

@end

@implementation AboutUsController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    NSMutableString *str = [NSMutableString stringWithString:[NSString stringWithFormat:@"%@/%@%@",BaseUrl,aboutUs,@0]];
//
////    for (NSHTTPCookie *cookie in NSHTTPCookieStorage.sharedHTTPCookieStorage.cookies) {
////        [str appendString:[NSString stringWithFormat:@"%@=%@",cookie.name,cookie.value]];
////    }
//    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:str]]
//     ];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"关于我们";
    // 初始化webView
//    [self setupWebView];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
}

#pragma mark - 初始化
- (void)setupWebView {
    [MBProgressHUD showMessage:@"数据加载中..." ToView:self.view userInteractionEnabled:YES];
    
    WKWebViewConfiguration *config = [WKWebViewConfiguration new];
    self.webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight) configuration:config];
    self.webView.navigationDelegate = self;
    [self.view addSubview:self.webView];
}

#pragma mark - WKNavigationDelegate
- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
@end
