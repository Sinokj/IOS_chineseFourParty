//
//  UIViewController+Add.m
//  CloudPartyDangJian
//
//  Created by 李子栋 on 2019/11/12.
//  Copyright © 2019 TWO. All rights reserved.
//

#import "UIViewController+Add.h"

@implementation UIViewController (Add)

+ (void)load {
    Method one = class_getInstanceMethod([self class], @selector(presentViewController:animated:completion:));
    Method two = class_getInstanceMethod([self class], @selector(add_presentViewController:animated:completion:));
    method_exchangeImplementations(one, two);
}

- (void)add_presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    viewControllerToPresent.modalPresentationStyle = UIModalPresentationFullScreen;
    [self add_presentViewController:viewControllerToPresent animated:flag completion:completion];
}

@end
