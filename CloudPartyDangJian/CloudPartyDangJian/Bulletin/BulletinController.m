//
//  BulletinController.m
//  CloudPartyDangJian
//
//  Created by ZJXN on 2018/4/23.
//  Copyright © 2018年 ZJXN. All rights reserved.
//

#import "BulletinController.h"
#import "ListModel.h"
#import "ListCell.h"
#import "WebViewController.h"
#import "SegmentControlPackage.h"
#import "LLSearchViewController.h"
@interface BulletinController ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,WebViewDelegate>

@property (nonatomic, strong) UITableView *tableV;
@property (nonatomic, strong) ListModel *listM;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray<NewList *> *sourceArr;

@property (nonatomic, strong) NSMutableArray *markArray;
@property (nonatomic, strong) NSMutableArray *markTitleArr;
@property (nonatomic, assign) NSInteger nTagId;//标签栏某一项的ID
@property (nonatomic, strong) SegmentControlPackage *segmentControl;
@property (nonatomic, strong) NSIndexPath *indexPath;

//添加下方滚动视图
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, assign) NSInteger currentNum;//滚动视图当前显示的页面
@property (nonatomic, strong) NSMutableArray *tableArr;

@end

@implementation BulletinController

- (NSMutableArray *)tableArr {
    if (_tableArr == nil) {
        _tableArr = [NSMutableArray array];
    }
    return _tableArr;
}

- (NSMutableArray<NewList *> *)sourceArr {
    if (_sourceArr == nil) {
        _sourceArr = [NSMutableArray array];
    }
    return _sourceArr;
}

- (NSMutableArray *)markArray {
    if (_markArray == nil) {
        _markArray = [NSMutableArray array];
    }
    return _markArray;
}
- (NSMutableArray *)markTitleArr {
    if (_markTitleArr == nil) {
        _markTitleArr = [NSMutableArray array];
    }
    return _markTitleArr;
}


- (void)reloadData {
    [self requestDataWithPage:1];
    self.segmentControl.selectIndex = 0;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshData) name:@"isLoginUpdateData" object:nil];
    
    self.nTagId = 0;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"要闻";
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:0.9463 green:0.1012 blue:0.1432 alpha:1.0];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    
    [self requestDataWithPage:1];
    [self getTheTopMarks];
    
    @weakify(self);
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonItemWithImageName:@"home_search2" block:^(id sender) {
        [weak_self pushToSearchVC];
    }];
}

- (void)pushToSearchVC {
    
    LLSearchViewController *vc = [[LLSearchViewController alloc] init];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:true];
    
}

- (void)getTheTopMarks {
    //获取顶部标签数据
    __weak typeof(self) weakSelf = self;
    NSString *url = [NSString stringWithFormat:@"%@nModuleId=50", TopMarkURL];
    
    [DataRequest PostWithURL:url parameter:nil :^(id reponserObject) {
        
        NSArray *data = reponserObject[@"result"];
        weakSelf.markArray = [NSMutableArray arrayWithArray:data];
        for (NSDictionary *dic in data) {
            [weakSelf.markTitleArr addObject:dic[@"vcTagName"]];
        }
        //标签只有一个时，不显示选择按钮；有多个时，显示选择按钮
        if (weakSelf.markArray.count > 1) {
            [weakSelf setupSegment];
        }
        [weakSelf setupMainView];
        
    }];
    
}
- (void)setupMainView {
    
    //底部ScrollView
    self.mainScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.segmentControl.frame), MAIN_SIZE.width, MAIN_SIZE.height - 64 - 45)];
    [self.view addSubview:self.mainScrollView];
    self.mainScrollView.delegate = self;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.contentSize = CGSizeMake(self.markTitleArr.count * MAIN_SIZE.width, 0);
    
    //TableView
    for (int i = 0; i < self.markTitleArr.count; i ++) {
        
        self.tableV = [[UITableView alloc] initWithFrame:CGRectMake(i *MAIN_SIZE.width, 0, ScreenWidth, CGRectGetHeight(self.mainScrollView.frame)) style:(UITableViewStylePlain)];
        self.tableV.delegate = self;
        self.tableV.dataSource = self;
//        self.tableV.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.mainScrollView addSubview:self.tableV];
        
        MJRefreshNormalHeader *header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshData)];
        self.tableV.mj_header = header;
        @weakify(self);
        self.tableV.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
            @strongify(self);
            self.page +=1;
            [self requestDataWithPage:self.page];
        }];
        self.page = 1;
        self.tableV.tableFooterView = [UIView new];
        
        [self.tableV registerNib:[UINib nibWithNibName:@"ListCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        
        self.tableV.tag = i;
        [self.tableArr addObject:self.tableV];
    }
    
}

- (void)refreshData {
    [self requestDataWithPage:1];
    self.page = 1;
    [self.tableV.mj_header endRefreshing];
}

//标签栏
- (void)setupSegment {
    
    __weak typeof(self) weakSelf = self;
    self.segmentControl = [[SegmentControlPackage alloc] initwithTitleArr:self.markTitleArr iconArr:nil SCType:SCType_SelectChange];
    self.segmentControl.frame = CGRectMake(0, 0, MAIN_SIZE.width, 45);
    self.segmentControl.selectBtnWID = 80.0;
    self.segmentControl.selectBtnSpace = 15;
    self.segmentControl.selectType = ^(NSInteger selectIndex,NSString *selectIndexTitle){
        
        [weakSelf.sourceArr removeAllObjects];
        weakSelf.currentNum = selectIndex;
        //点击标签，设置scrollView的偏移量
        [weakSelf.mainScrollView setContentOffset:CGPointMake(selectIndex *MAIN_SIZE.width, 0) animated:YES];
        
        //标签Id回调
        NSInteger nId = [[weakSelf.markArray[selectIndex] objectForKey:@"nId"] integerValue];
        weakSelf.nTagId = nId;
        [weakSelf requestDataWithPage:1];
        
    };
    [self.view addSubview:self.segmentControl];
    
}
//请求列表数据
- (void)requestDataWithPage:(NSInteger)pageNo {
    
    NSUserDefaults *userDe = [NSUserDefaults standardUserDefaults];
    NSInteger partyId = [[userDe objectForKey:@"partyID"] integerValue];
    
    [DataRequest PostWithURL:[NSString stringWithFormat:@"%@?vcType=首页广告&nPageNo=%ld&nPageSize=10&nCommitteeId=%ld&nTagId=%ld", ArticleURL, (long)pageNo, partyId, self.nTagId] parameter:nil :^(id reponserObject) {
       
        self.listM = [ListModel mj_objectWithKeyValues:reponserObject];
        if (self.listM.objects.count == 0 || self.listM.objects == nil) {
            
            [MBProgressHUD showMessage:@"暂无数据" RemainTime:1.0 ToView:self.view userInteractionEnabled:YES];
        }
        if (pageNo > 1) {
            [self.sourceArr addObjectsFromArray:self.listM.objects];
        }else {
            [self.sourceArr removeAllObjects];
            [self.sourceArr addObjectsFromArray:self.listM.objects];
        }
        
        // 
        if (self.tableArr.count > 0) {
            self.tableV = self.tableArr[self.currentNum];
        }
        [self.tableV reloadData];
        [self.tableV.mj_header endRefreshing];
        [self.tableV.mj_footer endRefreshing];
        
    }];
    
    
}
//tableView代理方法
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.sourceArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.model = self.sourceArr[indexPath.row];
    cell.isShow = YES;
    if ([STUtils objectForKey:KDangJianType] && [[STUtils objectForKey:KDangJianType] integerValue] == 2) {
        cell.alertL.hidden = YES;
    }else {
        cell.alertL.hidden = NO;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.tableV = self.tableArr[self.currentNum];
    [self.tableV deselectRowAtIndexPath:indexPath animated:YES];
    
    WebViewController *webVC = [WebViewController new];
    webVC.type = @"1";
    webVC.str = self.sourceArr[indexPath.row].url;
    
    NewList *neList = self.sourceArr[indexPath.row];
    webVC.canShare = neList.nShared;
    webVC.tit = @"要闻";
    webVC.shareImage = @[neList.vcPath];
    webVC.shareTitle = neList.vcTitle;
    webVC.shareDescribe = neList.vcDescribe;
    webVC.articleId = neList.nId;
    webVC.vcType = neList.vcType;
    
    webVC.delegate = self;
    self.indexPath = indexPath;
    
    webVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webVC animated:YES];
    
}
- (void)getNewState:(NSInteger)state {
    
    self.tableV = self.tableArr[self.currentNum];
    self.sourceArr[self.indexPath.row].nStatus = state;
    [self.tableV reloadRowsAtIndexPaths:@[self.indexPath] withRowAnimation:(UITableViewRowAnimationNone)];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200 * kWScale;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
    
}


//scrollView代理方法
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    if ([scrollView isEqual:self.mainScrollView]) {
        self.currentNum = self.mainScrollView.contentOffset.x / MAIN_SIZE.width;
        self.segmentControl.selectIndex = self.currentNum;
        
//        UITableView * currentTable = self.tableArr[self.currentNum];
//        self.tableV = currentTable;
//        [self.tableV reloadData];
        
        return;
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
}



@end

