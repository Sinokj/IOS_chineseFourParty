//
//  ALMsgCell.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "ALMsgCell.h"

@interface ALMsgCell ()
/** icon */
@property (nonatomic, strong)UIImageView *iconImageView;
/** 标题 */
@property (nonatomic, strong)UILabel *titleLabel;
/** 内容 */
@property (nonatomic, strong)UILabel *contentLabel;
@end

@implementation ALMsgCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self drawCell];
    }
    return self;
}

- (void)drawCell{
    /** icon */
    self.iconImageView = [UIImageView new];
    /** bgView */
    UIView *bgView = [UIView viewWithBackgroundColor:[UIColor whiteColor]];
    [self.contentView sd_addSubviews:@[bgView, self.iconImageView]];
    
    /** 标题 */
    self.titleLabel = [UILabel labelWithTextColor:kTextColor font:16 backgroundColor:nil Text:@"我是标题"];
    self.titleLabel.font = FontBold(16);
    self.titleLabel.numberOfLines = 1;
    /** 内容 */
    self.contentLabel = [UILabel labelWithTextColor:kTextColor font:16 backgroundColor:nil Text:@"我是内容"];
    [bgView sd_addSubviews:@[self.titleLabel, self.contentLabel]];
    
    self.iconImageView.sd_layout
    .widthIs(100 * kWScale)
    .heightIs(100 * kWScale)
    .leftSpaceToView(self.contentView, 16)
    .centerYEqualToView(self.contentView);
    
    bgView.sd_layout
    .leftSpaceToView(self.iconImageView, 10)
    .centerYEqualToView(self.iconImageView)
    .rightSpaceToView(self.contentView, 16);
    
    self.titleLabel.sd_layout
    .leftSpaceToView(bgView, 0)
    .rightSpaceToView(bgView, 0)
    .topSpaceToView(bgView, 0)
    .autoHeightRatio(0);
    
    self.contentLabel.sd_layout
    .leftSpaceToView(bgView, 0)
    .rightSpaceToView(bgView, 0)
    .topSpaceToView(self.titleLabel, 6)
    .autoHeightRatio(0);
    [self.contentLabel setMaxNumberOfLinesToShow:2];
    
    [bgView setupAutoHeightWithBottomView:self.contentLabel bottomMargin:0];
    
}

- (void)setModel:(ALMyMsgModel *)model{
    _model = model;
    if ([model.vcType isEqualToString:@"会议通知"]) {
        self.iconImageView.image = [UIImage imageNamed:@"icon_xx_hy_"];
    }else if ([model.vcType isEqualToString:@"审核通知"]) {
        self.iconImageView.image = [UIImage imageNamed:@"icon_xx_sh_"];
    }else if ([model.vcType isEqualToString:@"温馨提示"]) {
        self.iconImageView.image = [UIImage imageNamed:@"icon_xx_jf__"];
    }else if([model.vcType isEqualToString:@"请假通知"]){
        self.iconImageView.image = [UIImage imageNamed:@"msg_jia"];
    }
    self.titleLabel.text = model.vcTitle;
    self.contentLabel.text = model.vcContent;
}



@end
