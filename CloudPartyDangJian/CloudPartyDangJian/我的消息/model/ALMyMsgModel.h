//
//  ALMyMsgModel.h
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ALMyMsgModel : NSObject
@property (nonatomic , copy) NSString              * vcTitle;
@property (nonatomic , copy) NSString              * vcStates;
@property (nonatomic , copy) NSString              * vcType;
@property (nonatomic , copy) NSString              * dtReg;
@property (nonatomic , copy) NSString              * vcRegister;
@property (nonatomic , copy) NSString              * vcUserId;
@property (nonatomic , assign) NSInteger             originId;
@property (nonatomic , assign) NSInteger               nID;
@property (nonatomic , copy) NSString              * vcContent;
@property (nonatomic , copy) NSString              * vcTel;


//详情(请假)
@property (nonatomic , copy) NSString              *nId;
@property (nonatomic , copy) NSString              *mId;
@property (nonatomic , copy) NSString              *vcName;
@property (nonatomic , copy) NSString              *leaveTime;
@property (nonatomic , copy) NSString              *vcStatus;
@property (nonatomic , copy) NSString              *vcReason;
@property (nonatomic , copy) NSString              *meetingName;


//详情(审核)
@property (nonatomic , copy) NSString              * vcAuditNo;
@property (nonatomic , copy) NSString              * vcResult;
@property (nonatomic , copy) NSString              * partyName;
@property (nonatomic , copy) NSString              * vcStepNo;
@property (nonatomic , copy) NSString              * vcSex;
@property (nonatomic , copy) NSString              * vcStepName;
@property (nonatomic , copy) NSString              * dtApplication;
@property (nonatomic, assign) NSInteger              nStatus; //0未处理 1 审核通过 2审核未通过



//推送
//@property (nonatomic , copy) NSString              * vcType;
@property (nonatomic , copy) NSString              * nMsgId;

@end

NS_ASSUME_NONNULL_END
