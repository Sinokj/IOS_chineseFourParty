//
//  ALMsgSubVC.m
//  CloudPartyDangJian
//
//  Created by 艾林 on 2018/12/13.
//  Copyright © 2018 TWO. All rights reserved.
//

#import "ALMsgSubVC.h"
#import "ALMsgCell.h"
#import "ALMsgApplyDetailVC.h"
#import "PartyArticleDetailVC.h"

@interface ALMsgSubVC ()

@end

@implementation ALMsgSubVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //绘制页面
    [self drawSubView];
    
    //加载我的消息列表
    [self requestForMyMessage];
}

- (void)drawSubView{
    [self.view addSubview:self.tableView];
}


- (void)requestForMyMessage{
    NSInteger type = 0;
    if ([self.yp_tabItemTitle isEqualToString:@"待处理"]) {
        type = 0;
    }else{
        type = 1;
    }
    
    NSString *url = [NSString stringWithFormat:@"%@vcStatus=%ld",GetMessagerList, type];
    [DataRequest GetWithURL:url :^(id reponserObject) {
        NSArray *tempArray = [ALMyMsgModel mj_objectArrayWithKeyValuesArray:reponserObject[@"object"]];
        self.modelArray = tempArray;
        
//        NSMutableArray *unHandle = [NSMutableArray array];
//        NSMutableArray *handled = [NSMutableArray array];
//        for (int i = 0; i < tempArray.count; i ++ ) {
//            ALMyMsgModel *model = tempArray[i];
//            if ([model.vcStates isEqualToString:@"0"]) {//待处理
//                [unHandle addObject:model];
//            }else if([model.vcStates isEqualToString:@"1"]){//已处理
//                [handled addObject:model];
//            }
//        }
//        ALMsgSubVC *subVC = (ALMsgSubVC *)self.viewControllers.firstObject;
//        subVC.modelArray = unHandle;
//
//        ALMsgSubVC *subVC2 = (ALMsgSubVC *)self.viewControllers.lastObject;
//        subVC2.modelArray = handled;
        
        [self.tableView reloadData];
    }];
}


#pragma mark - UITableViewDelegate, UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ALMsgCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ALMsgCell" forIndexPath:indexPath];
    cell.model = self.modelArray[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 160 * kWScale;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ALMyMsgModel *model = self.modelArray[indexPath.row];
    if ([model.vcType isEqualToString:@"审核通知"]) {
        ALMsgApplyDetailVC *detailVC = [ALMsgApplyDetailVC new];
        detailVC.nId = model.nID;
        detailVC.type = model.vcType;
        [self.navigationController pushViewController:detailVC animated:YES];
    }else if ([model.vcType isEqualToString:@"请假通知"]) {
        ALMsgApplyDetailVC *detailVC = [ALMsgApplyDetailVC new];
        detailVC.nId = model.originId;
        detailVC.type = model.vcType;
        [self.navigationController pushViewController:detailVC animated:YES];
    }else if ([model.vcType isEqualToString:@"会议通知"]) {
        PartyArticleDetailVC *detailVC = [PartyArticleDetailVC new];
        detailVC.nId = model.originId;
        detailVC.type = model.vcType;
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

#pragma mark - lazy
- (UITableView *)tableView{
    if (_tableView == nil) {
        _tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight - kTopHeight - 44 - kBottomSafeAreaHeight) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.backgroundColor = [UIColor whiteColor];
//        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_tableView registerClass:[ALMsgCell class] forCellReuseIdentifier:@"ALMsgCell"];
        
//        MJWeakSelf;
//        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//            self.page = 1;
//            [weakSelf requestForMsgList];
//        }];
//
//        _tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
//            self.page ++;
//            [weakSelf requestForMsgList];
//        }];
    }
    
    return _tableView;
}



@end
