//
//  SegmentControlPackage.m
//  MedicineMall
//
//  Created by MyBook on 2017/9/4.
//  Copyright © 2017年 FeiYangHe. All rights reserved.
//

#import "SegmentControlPackage.h"

#define stateViewY 1
@interface SegmentControlPackage ()<UICollectionViewDelegate>

@property (nonatomic,strong) NSArray *titleArr;
@property (nonatomic,strong) NSArray *iconArr;
@property (nonatomic,assign) SegmentedControlType SCType;   //类型
@property (nonatomic,strong) UIScrollView *scrollView;      //底部滚动视图
@property (nonatomic,assign) NSInteger btnCount;            //按钮的数量
@property (nonatomic,strong) NSMutableArray *titleArray;    //存放按钮的数组
@property (nonatomic,strong) UIView *stateView;             //按钮下面的伴随状态按钮
@property (nonatomic,assign) BOOL isSetWID;                 //是否是默认宽度

@end


@implementation SegmentControlPackage


- (instancetype)initwithTitleArr:(NSArray *)titleArr iconArr:(NSArray *)iconArr SCType:(SegmentedControlType)SCType{
    if(self == [super init]){
        self.titleArr = titleArr;
        self.iconArr = iconArr;
        self.SCType = SCType;
        [self initValue];
        [self uiConfig];
        
        [self setupTheParts];
    }
    return self;
}

#pragma mark - 先进行一些设置（根据不同的项目要求进行更改）
- (void)setupTheParts {
    self.backgroundColor = RGB(239, 239, 244);
    self.titleColor = [UIColor darkGrayColor];
    self.selectTitleColor = [UIColor redColor];
    self.selectBtnWID = 70.0;
    self.selectBtnSpace = 5;//按钮间的间距
    self.SCType_Underline_HEI = 2;
    self.titleFont = [UIFont fontWithName:@"STHeitiSC-Light" size:15.0];
}

- (void)initValue{
    self.clipsToBounds = YES;
    _edgeInsetsStyle = ButtonEdgeInsetsStyleTop;
    _animateDuration = 0.01;
    _selectBtnWID = 0;
    _selectBtnSpace = 0;
    _selectIndex = 0;//默认选中第一个
    _SCType_Underline_HEI = 2;
    _btnCount = self.titleArr.count!=0 ? self.titleArr.count : self.iconArr.count;
    _titleColor = [UIColor blackColor];
    _selectTitleColor = RGB(253, 104, 154);
    _ellipseBackColor = [UIColor colorWithRed:0.000 green:0.000 blue:0.000 alpha:0.2];
    _titleBtnBackColor = [UIColor clearColor];
    _cornerRadius = 2;
    _edgeInsetsSpace = 0;
    _titleFont = [UIFont fontWithName:@"STHeitiSC-Light" size:15.0];
}

- (void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    _selectBtnWID = _isSetWID == NO ? (frame.size.width - (_btnCount - 1) * _selectBtnSpace)/_btnCount : _selectBtnWID;
    [self uiConfig];
}

- (void)setSelectBtnWID:(CGFloat)selectBtnWID{
    _isSetWID = YES;
    _selectBtnWID = selectBtnWID;
    [self uiConfig];
}

- (void)setSelectBtnSpace:(CGFloat)selectBtnSpace{
    _selectBtnSpace = selectBtnSpace;
    _selectBtnWID = _isSetWID == NO ? (self.frame.size.width - (_btnCount - 1) * _selectBtnSpace)/_btnCount : _selectBtnWID;
    [self uiConfig];
}

- (void)uiConfig{
    if(_btnCount==0) return;//如果没有值 直接返回
    
    if(self.scrollView == nil){
        self.scrollView = [[UIScrollView alloc]init];
        self.scrollView.showsVerticalScrollIndicator = NO;
        self.scrollView.showsHorizontalScrollIndicator = NO;
        self.scrollView.delegate = self;
    }
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }
    self.scrollView.frame = self.bounds;
    self.scrollView.contentSize = CGSizeMake(_selectBtnWID * _btnCount + _selectBtnSpace * (_btnCount - 1), 0);
    [self addSubview:self.scrollView];
    
    //清空数组
    [self.titleArray removeAllObjects];
    
    NSMutableArray *templarr = [NSMutableArray array];
    for (int i= 0; i<_btnCount; i++) {
        UIButton *titleBtn = [[UIButton alloc]init];
        titleBtn.backgroundColor = _titleBtnBackColor;
//        titleBtn.frame = CGRectMake((_selectBtnWID + _selectBtnSpace) * i, 0, _selectBtnWID, self.frame.size.height);
//        DLog(@"---%@\n",@([self.titleArr[i] widthForFont:_titleFont]));
//        titleBtn.frame = CGRectMake(_selectBtnSpace + (_selectBtnSpace + [self.titleArr[i] widthForFont:_titleFont]) * i, 0, [self.titleArr[i] widthForFont:_titleFont], self.frame.size.height);
        if(self.titleArr.count>0){
            [titleBtn setTitle:_titleArr[i] forState:UIControlStateNormal];
        }
        if(self.iconArr.count>0){
            [titleBtn setImage:[UIImage imageNamed:_iconArr[i]] forState:UIControlStateNormal];
        }
        
        if(self.titleArr.count>0 && self.iconArr.count>0){
            [self layoutButtonWithEdgeInsetsStyle:_edgeInsetsStyle imageTitleSpace:_edgeInsetsSpace button:titleBtn];
        }
        
        titleBtn.titleLabel.numberOfLines = 0;
        titleBtn.titleLabel.adjustsFontSizeToFitWidth = YES;
        titleBtn.titleLabel.textAlignment = NSTextAlignmentCenter;
        titleBtn.titleLabel.font = _titleFont;
        if ([_titleArr[i] length] > 4) {
            titleBtn.titleLabel.font = FontSystem(13);
        }else {
            titleBtn.titleLabel.font = _titleFont;
        }
        
        [titleBtn addTarget:self action:@selector(titleBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [titleBtn setTitleColor:_titleColor forState:UIControlStateNormal];
        [titleBtn setTitleColor:_selectTitleColor forState:UIControlStateSelected];
        
//        [titleBtn sizeToFit];
        [templarr addObject:titleBtn];
        [self.scrollView addSubview:titleBtn];
        [self.titleArray addObject:titleBtn];
        if(_selectIndex == i){
            titleBtn.titleLabel.font = _selectBtnFont;
            titleBtn.selected = YES;
        }
    }
    
    [templarr mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:15 leadSpacing:0 tailSpacing:0];
//    @weakify(self);
    for(int i=0;i< templarr.count;i++) {
        UIButton *btn = templarr[i];
        [btn mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.mas_equalTo(self.frame.size.height);
            make.width.mas_equalTo([self.titleArr[i] widthForFont:_titleFont] + 30);
          
            DLog(@"--%@\n",self.titleArr[i]);
//            make.width.mas_equalTo(100);
//            DLog(@"%@ ---- %f\n",weak_self.titleArr[i],[weak_self.titleArr[i] widthForFont:weak_self.titleFont]);
//            make.width.mas_equalTo([weak_self.titleArr[i] widthForFont:weak_self.titleFont]);
        }];
//        [btn sizeToFit];
//        [btn setNeedsLayout];
//        [btn layoutIfNeeded];
    }
  
    [self setSCTypeUI];
}

#pragma mark - 根据SCType设置相关的风格
- (void)setSCTypeUI{
    UIButton *selectBtn = _titleArray[_selectIndex];
    if(self.SCType == SCType_Underline){
        if(self.stateView==nil){
            self.stateView = [[UIView alloc]init];
        }
        self.stateView.frame = CGRectMake(selectBtn.frame.origin.x, self.frame.size.height - _SCType_Underline_HEI, _selectBtnWID, _SCType_Underline_HEI);
        self.stateView.backgroundColor = _selectTitleColor;
        [self.scrollView addSubview:self.stateView];
    }else if(self.SCType == SCType_Dot){
        if(self.stateView==nil){
            self.stateView = [[UIView alloc]init];
        }
        self.stateView.frame = CGRectMake(selectBtn.frame.origin.x + (_selectBtnWID - _SCType_Underline_HEI)/2, self.frame.size.height - _SCType_Underline_HEI, _SCType_Underline_HEI, _SCType_Underline_HEI);
        self.stateView.backgroundColor = _selectTitleColor;
        self.stateView.layer.masksToBounds = YES;
        self.stateView.layer.cornerRadius = _SCType_Underline_HEI/2;
        [self.scrollView addSubview:self.stateView];
    }else if(self.SCType == SCType_Ellipse){
        if(self.stateView==nil){
            self.stateView = [[UIView alloc]init];
        }
        self.stateView.frame = CGRectMake(selectBtn.frame.origin.x, stateViewY, _selectBtnWID, self.frame.size.height -2*stateViewY);
        self.stateView.backgroundColor = _ellipseBackColor;
        self.stateView.layer.masksToBounds = YES;
        self.stateView.layer.cornerRadius = _cornerRadius;
        [self.scrollView insertSubview:self.stateView belowSubview:selectBtn];
    }else if (self.SCType == SCType_SelectChange){
        
    }else if (self.SCType == SCType_BorderStyle){
        UIButton *firstBtn = [self.titleArray firstObject];
        firstBtn.layer.cornerRadius = self.frame.size.height/2;
        firstBtn.layer.borderWidth = 0.5;
        firstBtn.layer.borderColor = _selectTitleColor.CGColor;
    }else if (self.SCType == SCType_None){
        
    }
}

#pragma mark - 头部按钮点击事件
- (void)titleBtnClick:(UIButton *)button{
    [self refreshUIWithSelectBtn:button];
}

- (void)refreshUIWithSelectBtn:(UIButton *)button{
    for (UIButton *titleBtn in self.titleArray) {
        titleBtn.selected = NO;
        titleBtn.titleLabel.font = _titleFont;
        titleBtn.backgroundColor = _titleBtnBackColor;
        titleBtn.layer.cornerRadius = 0.0;
        titleBtn.layer.borderWidth = 0.0;
        titleBtn.layer.borderColor = [UIColor clearColor].CGColor;
    }
    button.selected = YES;
    if(_selectBtnFont!=nil){
        button.titleLabel.font = _selectBtnFont;
    }
    _selectIndex = [self.titleArray indexOfObject:button];
    if(self.selectType){
        self.selectType(_selectIndex,button.currentTitle);
    }
    
    CGFloat offsetx = (button.frame.origin.x + _selectBtnWID + _selectBtnSpace) - self.scrollView.frame.size.width + _selectBtnWID;
    CGFloat offsetMax = self.scrollView.contentSize.width - self.frame.size.width+50;
    if (offsetx < 0) {
        offsetx = 0;
    }else if (offsetx > offsetMax){
        offsetx = offsetMax;
    }
    CGPoint offset = CGPointMake(offsetx, self.scrollView.contentOffset.y);
    [self.scrollView setContentOffset:offset animated:YES];
    
    if(self.SCType == SCType_Underline){
        //下划线
        [UIView animateWithDuration:_animateDuration animations:^{
            self.stateView.frame = CGRectMake(button.frame.origin.x, self.frame.size.height - _SCType_Underline_HEI, _selectBtnWID, _SCType_Underline_HEI);
        }];
    }else if(self.SCType == SCType_Dot){
        [UIView animateWithDuration:_animateDuration animations:^{
            self.stateView.frame = CGRectMake(button.frame.origin.x + (_selectBtnWID - _SCType_Underline_HEI)/2, self.frame.size.height - _SCType_Underline_HEI, _SCType_Underline_HEI, _SCType_Underline_HEI);
        }];
    }else if(self.SCType == SCType_Ellipse){
        [UIView animateWithDuration:_animateDuration animations:^{
            self.stateView.frame = CGRectMake(button.frame.origin.x, stateViewY, _selectBtnWID, self.frame.size.height - 2*stateViewY);
        }];
    }else if (self.SCType == SCType_SelectChange){
        
    }else if (self.SCType == SCType_BorderStyle){
        [UIView animateWithDuration:_animateDuration animations:^{
            button.layer.cornerRadius = self.frame.size.height/2;
            button.layer.borderWidth = 0.5;
            button.layer.borderColor = _selectTitleColor.CGColor;
        }];
    }else if (self.SCType == SCType_None){
        
    }
}

- (void)setTitleColor:(UIColor *)titleColor{
    _titleColor = titleColor;
    for (UIButton *button in self.titleArray) {
        [button setTitleColor:titleColor forState:UIControlStateNormal];
    }
}

- (void)setSelectTitleColor:(UIColor *)selectTitleColor{
    _selectTitleColor = selectTitleColor;
    for (UIButton *button in self.titleArray) {
        [button setTitleColor:selectTitleColor forState:UIControlStateSelected];
    }
}

- (void)setTitleFont:(UIFont *)titleFont{
    _titleFont = titleFont;
    for (UIButton *button in self.titleArray) {
        button.titleLabel.font = titleFont;
    }
}

- (void)setSelectBtnFont:(UIFont *)selectBtnFont{
    _selectBtnFont = selectBtnFont;
    UIButton *selectBtn = self.titleArray[_selectIndex];
    selectBtn.titleLabel.font = _selectBtnFont;
}

- (void)setAnimateDuration:(CGFloat)animateDuration{
    _animateDuration = animateDuration;
}

- (void)setBorderWidth:(CGFloat)borderWidth{
    if(borderWidth < 0) return;
    _borderWidth = borderWidth;
    self.selectBtnSpace = borderWidth;
    self.backgroundColor = _titleColor;
    self.layer.borderWidth = borderWidth;
    self.layer.borderColor = _titleColor.CGColor;
    self.layer.cornerRadius = borderWidth;
}

- (void)setTitleBtnBackColor:(UIColor *)titleBtnBackColor{
    _titleBtnBackColor = titleBtnBackColor;
    UIButton *selectBtn = self.titleArray[_selectIndex];
    for (UIButton *button in self.titleArray) {
        button.backgroundColor = titleBtnBackColor;
        if(selectBtn == button){
            button.backgroundColor = _titleColor;
        }
    }
}

- (void)setSCType_Underline_HEI:(CGFloat)SCType_Underline_HEI{
    _SCType_Underline_HEI = SCType_Underline_HEI;
    UIButton *selectBtn = (UIButton *)self.scrollView.subviews[_selectIndex];
    if(self.SCType == SCType_Underline){
        self.stateView.frame = CGRectMake(selectBtn.frame.origin.x, self.frame.size.height - _SCType_Underline_HEI, _selectBtnWID, _SCType_Underline_HEI);
    }else if(self.SCType == SCType_Dot){
        self.stateView.frame = CGRectMake(selectBtn.frame.origin.x + (_selectBtnWID - _SCType_Underline_HEI)/2, self.frame.size.height - _SCType_Underline_HEI, _SCType_Underline_HEI, _SCType_Underline_HEI);
        self.stateView.layer.cornerRadius = _SCType_Underline_HEI/2;
    }else if(self.SCType == SCType_Ellipse){
        self.stateView.layer.cornerRadius = _cornerRadius;
    }
}

- (void)setEllipseBackColor:(UIColor *)ellipseBackColor{
    _ellipseBackColor = ellipseBackColor;
    if(self.SCType == SCType_Ellipse){
        self.stateView.backgroundColor = _ellipseBackColor;
    }
}

- (NSMutableArray *)titleArray{
    if(_titleArray==nil){
        _titleArray = [[NSMutableArray alloc]init];
    }
    return _titleArray;
}

- (void)setCornerRadius:(CGFloat)cornerRadius{
    _cornerRadius = cornerRadius;
    if(self.SCType == SCType_Ellipse){
        self.stateView.layer.cornerRadius = _cornerRadius;
    }
}

- (void)setEdgeInsetsStyle:(ButtonEdgeInsetsStyle)edgeInsetsStyle{
    _edgeInsetsStyle = edgeInsetsStyle;
    for (UIButton *button in self.titleArray) {
        if(self.titleArr.count>0 && self.iconArr.count>0){
            [self layoutButtonWithEdgeInsetsStyle:_edgeInsetsStyle imageTitleSpace:_edgeInsetsSpace button:button];
        }
    }
}

- (void)setEdgeInsetsSpace:(CGFloat)edgeInsetsSpace{
    _edgeInsetsSpace = edgeInsetsSpace;
    for (UIButton *button in self.titleArray) {
        if(self.titleArr.count>0 && self.iconArr.count>0){
            [self layoutButtonWithEdgeInsetsStyle:_edgeInsetsStyle imageTitleSpace:_edgeInsetsSpace button:button];
        }
    }
}

- (void)setImageEdgeInsets:(UIEdgeInsets)imageEdgeInsets{
    _imageEdgeInsets = imageEdgeInsets;
    for (UIButton *button in self.titleArray) {
        if(self.titleArr.count>0 && self.iconArr.count>0){
            [self layoutButtonWithEdgeInsetsStyle:_edgeInsetsStyle imageTitleSpace:_edgeInsetsSpace button:button];
        }
    }
}

- (void)setLabelEdgeInsets:(UIEdgeInsets)labelEdgeInsets{
    _labelEdgeInsets = labelEdgeInsets;
    for (UIButton *button in self.titleArray) {
        if(self.titleArr.count>0 && self.iconArr.count>0){
            [self layoutButtonWithEdgeInsetsStyle:_edgeInsetsStyle imageTitleSpace:_edgeInsetsSpace button:button];
        }
    }
}

- (void)layoutButtonWithEdgeInsetsStyle:(ButtonEdgeInsetsStyle)style imageTitleSpace:(CGFloat)space button:(UIButton *)button{
    //得到imageView和titleLabel的宽、高
    CGFloat imageWith = button.imageView.frame.size.width;
    CGFloat imageHeight = button.imageView.frame.size.height;
    
    CGFloat labelWidth = 0.0;
    CGFloat labelHeight = 0.0;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 8.0) {
        //由于iOS8中titleLabel的size为0，用下面的这种设置
        labelWidth = button.titleLabel.intrinsicContentSize.width;
        labelHeight = button.titleLabel.intrinsicContentSize.height;
    } else {
        labelWidth = button.titleLabel.frame.size.width;
        labelHeight = button.titleLabel.frame.size.height;
    }
    
    //声明全局的imageEdgeInsets和labelEdgeInsets
    UIEdgeInsets imageEdgeInsets = UIEdgeInsetsZero;
    UIEdgeInsets labelEdgeInsets = UIEdgeInsetsZero;
    
    //根据style和space得到imageEdgeInsets和labelEdgeInsets的值
    //top, left, bottom, right
    switch (style) {
        case ButtonEdgeInsetsStyleTop:
        {
            //这里面要是值不合适 可以自己手动调
            imageEdgeInsets = UIEdgeInsetsMake(-labelHeight-space/2.0, 0, 0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith, -imageHeight-space/2.0, 0);
        }
            break;
        case ButtonEdgeInsetsStyleLeft:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, -space/2.0, 0, space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, space/2.0, 0, -space/2.0);
        }
            break;
        case ButtonEdgeInsetsStyleBottom:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, 0, -labelHeight-space/2.0, -labelWidth);
            labelEdgeInsets = UIEdgeInsetsMake(-imageHeight-space/2.0, -imageWith, 0, 0);
        }
            break;
        case ButtonEdgeInsetsStyleRight:
        {
            imageEdgeInsets = UIEdgeInsetsMake(0, labelWidth+space/2.0, 0, -labelWidth-space/2.0);
            labelEdgeInsets = UIEdgeInsetsMake(0, -imageWith-space/2.0, 0, imageWith+space/2.0);
        }
            break;
        case ButtonEdgeInsetsStyleNone:{
            imageEdgeInsets = _imageEdgeInsets;
            labelEdgeInsets = _labelEdgeInsets;
        }
            break;
        default:
            break;
    }
    button.titleEdgeInsets = labelEdgeInsets;
    button.imageEdgeInsets = imageEdgeInsets;
}

- (void)setSelectIndex:(NSInteger)selectIndex{
    if(selectIndex >self.titleArray.count || selectIndex<0) return;
    _selectIndex = selectIndex;
    UIButton *button = self.titleArray[selectIndex];
    [self refreshUIWithSelectBtn:button];
}

#pragma mark - UICollectionViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(scrollView == self.scrollView){
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, 0)];
    }
}



@end
